#!/usr/bin/env python
import numpy as np
from numpy import linspace, zeros, pi, exp, sin, sqrt
from matplotlib.pyplot import plot, show
import nose.tools as nt
import sys

#values:
mu_water = 0.0001	# drag coeff water	
mu_air = 8.9*0.001	# drag coeff air
q_water = 1.0*1000	# water density
q_air = 1.2		# air density
g = 9.81		# m/s^2

#drag coefficients
Cd_sphere = 0.45
Cd_semisphere = 0.42
Cd_cube = 1.05
Cd_sylinder = 0.82
Cd_rocket = 0.75
Cd_man = 1.0
Cd_plate = 1.3
Cd_droplet = 0.04

class Vertical_motion_problem:
    """
    a class for problems regarding the vertical motion of a body 
    in a fluid subjected to gravity.
    """
    def __init__(self, d, q, mu, Cd, A, q_body):
	self.d = float(d)
	self.V = 1.3*pi*((d/2.0)**3)
	self.mu = float(mu)
	self.m = q_body*self.V
	self.q_body = q_body
	self.c = q*d/float(mu)
	self.q = float(q)
	self.Cd = Cd; self.A = A; self.q = q

    def b(self, dt, n, R):
	return g*((self.q/self.q_body) -1)

    def a1(self, dt, n):
	return 3*pi*self.d*self.mu/self.m
	
    def a2(self, dt, n):
	return 0.5*self.Cd*self.q*self.A/self.m
    
    def a(self, dt, n, Re):
	if Re < 1:
	    return self.a1(dt, n)
	else:
	    return self.a2(dt, n)

    def f(self, v, dt, n):
	R = self.Re(v)
	b = self.b(dt, n, R)
	a = self.a(dt, n, R)
	if R < 1:
	    return ((1-0.5*dt*a)*v + dt*b) / (1+0.5*a*dt)
	else:
	    return (v+dt*b) / (1+dt*a*abs(v))
	    
    def Re(self, v):
	return self.c*abs(v)

class Test_linear_solution(Vertical_motion_problem):
    """
    class for testing the program by feeding it a problem with a 
    linear solution.
    """
    def __init__(self, d, q, mu, Cd, A, q_body, c, I, mod = 'qd'):
	Vertical_motion_problem.__init__(self, d, q, mu, Cd, A, q_body)
	self.c = c
	self.I = I
	if mod == 'ld':
	    self.Re = lambda v: 0.5
	elif mod == 'qd':	
	    self.Re = lambda v: 1

    def exact_solution(self, dt, n): 
	    return self.c*dt*n + self.I
    def a1(self, dt, n):
	    return self.a2(dt, n) 
    def a2(self, dt, n): 
	    return  (n*dt)**2 + 15 + exp(n*dt)
    def b(self, dt, n, R): 
	if R >= 1:	
	    return  self.c + self.a2(dt, n)* \
	    self.exact_solution(dt, n)* \
	    self.exact_solution(dt, n+1)
	else:
	    return self.c + self.a2(dt, n)*self.exact_solution(dt, n+0.5)

class Test_constant_solution(Vertical_motion_problem):
    """
    class for testing the program by feeding it a problem with a 
    constant solution.
    """
    def __init__(self, d, q, mu, Cd, A, q_body, u_const, mod = 'qd'):
	Vertical_motion_problem.__init__(self, d, q, mu, Cd, A, q_body)
	self.u_const = u_const
    	self.mod = mod
	if mod == 'qd':
	    self.Re = lambda v: 1
	elif mod == 'ld':
	    self.Re = lambda v: 0.5

    def exact_solution(self, dt, n): 
	    return self.u_const
    def a1(self, dt, n): 
	    return self.a2(dt, n)
    def a2(self, dt, n): 
	    return sin(n*dt) + exp(3*n*dt)
    def b(self, dt, n, R):
	if R >= 1:
	    return  self.a2(dt, n)*(self.exact_solution(dt, n)**2)
	else: 
	    return self.a2(dt, n)*self.exact_solution(dt, n)

class Ode_solver:
    def __init__(self, problem, I=0, dt=0.001, T=5):
	self.problem = problem
	self.dt = float(dt); self.I=I; self.T=T;

    def solve(self):
	Nt = int(round(self.T/self.dt))
	T = Nt*self.dt
	self.u = zeros(Nt+1)
	self.t = linspace(0,T,Nt+1)
	self.u[0]=self.I
	
	for n in range(0, Nt):
	    self.u[n+1] = self.problem.f(self.u[n], self.dt, n)
	return self.t, self.u

def main():
    """
    this is a primitive function to start the computations.

    NB: I would have made the program able to work on different input 
    were it stated explicitly as a requirement, or if I had more time.
    """
    r = 0.11;  d = 2*r
    q_body = 0.43/((4*pi*r**3)/3.0) 
    dt = 0.0001; I = 0; T = 0.4

    P = Vertical_motion_problem(d, q_water, mu_water, Cd_sphere, pi*r*r, q_body)
    S = Ode_solver(P, I, dt, T)
    t, u = S.solve()
    plot(t, u)
    show()

def test_linear_solution():
    """
    testing the program with a problem that has a linear (in t) solution.
    checking with quadratic and linear drag.
    """
    c = 2.10; 
    I = 0.0; dt = 0.1; T = 5 
    r = 0.11; d = r*2
    
    for mod in {'qd', 'ld'}:
	P = Test_linear_solution(d, q_water, mu_water, Cd_sphere, pi*r*r, \
	    0.43/((4*pi*r**3)/3.0), c, I, mod)
	S = Ode_solver(P, I, dt, T)
	t, u = S.solve()
	u_e = P.exact_solution(dt+zeros(len(t)), range(len(t)))
	diff = abs(u_e - u).max()
	nt.assert_almost_equal(diff, 0, places=13)

    
def test_constant_solution():
    """
    testing the program with a problem that has a constant solution.
    checking with quadratic and linear drag
    """
    dt = 0.1; T = 4; r = 0.11; d = 2*r
    I = u_const = 1.4

    for mod in {'ld', 'qd'}:
	P = Test_constant_solution(d, q_water, mu_water, Cd_sphere, pi*r*r,\
	    0.43/((4*pi*r**3)/3.0), u_const, mod)
	S = Ode_solver(P, I, dt, T)
	t, u = S.solve()
	u_e = P.exact_solution(dt+zeros(len(t)), range(len(t)))
	diff = abs(u_e - u).max()
	nt.assert_almost_equal(diff, 0, places=13)
    
def test_terminal_velocity():
    """
    comparing the velocity after one minute with the terminal 
    velocity predicted by the formula vT = sqrt(a/b) for upwards motion 
    in the quadratic drag formula.
    """
    dt = 0.1; T = 60; r = 0.11; d = 2*r 
    q_body = 0.43/((4*pi*r**3)/3.0); I = 0.0
    P = Vertical_motion_problem(d, q_water, mu_water, Cd_sphere, pi*r*r, q_body)
    S = Ode_solver(P, I, dt, T)
    
    t, u = S.solve()
    vT = sqrt(P.b(dt,dt*int(round(T/dt)), 1)/P.a2(dt,dt*int(round(T/dt))))
    diff = abs(u[-1] - vT)
    nt.assert_almost_equal(diff, 0, places = 13)

if __name__=='__main__':
    main()


