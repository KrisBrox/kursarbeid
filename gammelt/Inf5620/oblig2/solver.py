#!/usr/bin/env python
from numpy import zeros, linspace, newaxis, sin, cos, exp, ones,pi,sqrt
import nose.tools as nt
import time, sys, os,glob
from scitools.std import surf,savefig,movie

def next_scalar(u, u_1, u_2, f, x, y, t, n, dx,dy, dt, \
		q, b, V=None,fst=False):
    
    Ix = range(0,u.shape[0]); Iy=range(0,u.shape[1])
    dt2=dt**2
    Cx2 = dt2/dx**2;Cy2 = dt2/dy**2
    if fst: 
	Cx2 = 0.5*Cx2; Cy2 = 0.5*Cy2; dt2 = 0.5*dt2
	D1 = 1; D2 = 0
    else:
	D1 = 2; D2 = 1
    for i in Ix[1:-1]:
	for j in Iy[1:-1]:
	    uxx = q(x[i]+dx,y[j]) * ( u_1[i+1,j] - u_1[i,j] ) \
		    - q(x[i]-dx,y[j]) * ( u_1[i,j] - u_1[i-1,j] )
	    uyy = q(x[i],y[j]+dy) * ( u_1[i,j+1] - u_1[i,j] ) \
		    - q(x[i],y[j]-dy) * ( u_1[i,j] - u_1[i,j-1] )
	    u[i,j] = (D1 * u_1[i,j] - D2 * (1-0.5*dt*b)*u_2[i,j] + \
		    dt2*(uxx+uyy+f(x[i],y[j],t[n])))/(1+D2*0.5*dt*b)     
	    if fst: 
		u[i,j] += dt*V(x[i], y[j])
    # Fix ghost points according to Boundary condition
    j = Iy[0] 
    for i in Ix: u[i,j] = u[i,j+2]
    i = Ix[0]
    for j in Iy: u[i,j] = u[i+2,j]
    j = Iy[-1]
    for i in Ix: u[i,j] = u[i,j-2]
    i = Ix[-1]
    for j in Iy: u[i,j] = u[i-2,j]
    return u

def next_vec(u, u_1, u_2, Cx2, Cy2, dt, f_c, V, Qx, Qy,b, fst):
    """
    """
    dt2 = dt**2
    if fst:
	Cx2 = 0.5*Cx2; Cy2 = 0.5*Cy2; dt2 = 0.5*dt2
	D1 = 1; D2 = 0
    else:
	D1 = 2; D2 = 1
    
    uxx = Qx[1:,1:-1] * ( u_1[2:,1:-1] - u_1[1:-1,1:-1] ) - \
	Qx[:-1,1:-1] * ( u_1[1:-1,1:-1] - u_1[:-2,1:-1] )
    
    uyy = Qy[1:-1,1:] * ( u_1[1:-1,2:] - u_1[1:-1,1:-1] ) - \
	Qy[1:-1,:-1] * ( u_1[1:-1,1:-1] - u_1[1:-1,:-2] )

    D2u = Cx2*uxx + Cy2*uyy

    if fst:
	u[1:-1,1:-1] = ( u_1[1:-1,1:-1] - dt*V[1:-1,1:-1]*(1-0.5*dt*b) \
		    + D2u + dt2*f_c[1:-1,1:-1] )

    else:
	u[1:-1, 1:-1] = (D1*u_1[1:-1,1:-1]-D2*(1-0.5*dt*b)*u_2[1:-1,1:-1]+\
		     D2u + dt2*f_c[1:-1,1:-1] )/float(1+D2*0.5*dt*b)
    
    #if fst: u[1:-1,1:-1] += dt*V[1:-1,1:-1] * (1-0.5*dt*b)
    
    j = i = 0
    u[:,j] = u[:,j+2]; u[i,:] = u[i+2,:]

    j = u.shape[1]-1; i = u.shape[0]-1
    u[:,j] = u[:,j-2]; u[i,:] = u[i-2,:]

    return u

def solve(Nx, Ny, Lx, Ly, T, dt, C, I, V, f,b, action=None, version='vec'):
    if V == 0 or V == None: V = lambda x,y : 0
    if f == 0 or f == None: f = lambda x,y,t:0
    # initialise variables and initial conditions
    x = linspace(0, Lx, Nx+1)
    y = linspace(0, Ly, Ny+1)
    dx = x[1]-x[0]; dy = y[1]-y[0]
    x = linspace(-dx, Lx+dx, Nx+3)
    y = linspace(-dy, Ly+dy, Ny+3)
    x[0] = x[2]; y[0] = y[2]; x[-1] = x[-3]; y[-1] = y[-3]
    Nt = int(round(T/float(dt)))
    t = linspace(0, dt*Nt, Nt+1)

    u = zeros((x.shape[0], y.shape[0]), dtype=float)
    u_1 = zeros(u.shape, dtype=float)
    u_2 = zeros(u.shape, dtype=float)
    V_c = zeros(u.shape, dtype=float)
    f_c = zeros(u.shape, dtype=float)
    Q_c = zeros((u.shape[0],u.shape[1]),dtype=float)
    Q_c1 = zeros((u.shape[0]-1,u.shape[1]),dtype=float)
    Q_c2 = zeros((u.shape[0],u.shape[1]-1),dtype=float)
    Cx2 = (dt/dx)**2; Cy2 = (dt/dy)**2     
    xv = x[:,newaxis]; yv = y[newaxis,:]
    t0 = time.clock()

    # Initial condition, precomputing for speed
    if version == 'vec':
	u_1[:,:] = I(xv, yv)
	V_c[:,:] = V(xv, yv) 
	f_c[:,:] = f(xv, yv, 0)
	Q_c[:,:] = C(xv, yv)
	Q_c1[:,:] = (Q_c[1:,:]+Q_c[:-1,:])/2.0
	Q_c2[:,:] = (Q_c[:,1:]+Q_c[:,:-1])/2.0
    elif version == 'scalar':
	Ix = range(0,u.shape[0]); Iy = range(0,u.shape[1])
	for i in Ix:
	    for j in Iy:
		u_1[i,j] = I(x[i],y[i])
    
    if action != None:
	action(u_1, x, xv, y, yv, t, 0)	

    # Do something special for the first step
    if version == 'vec':
	u = next_vec(u, u_1, u_2, Cx2, Cy2, dt, \
	   f_c, V_c, Q_c1,Q_c2,b, fst=True)
    else:
	n = 0
	u = next_scalar(u, u_1,u_2,f,x,y,t,n,dx,dy,dt,C,b,V,True)

    if action != None:
	action(u, x, xv, y, yv, t, 1)	

    # All subsequent steps
    for i in range(1,Nt):
	u_2[:,:] = u_1; u_1[:,:] = u
	if version == 'vec':
	    f_c[:,:] = f(xv,yv, t[i])
	    u = next_vec(u, u_1, u_2, Cx2, Cy2,dt, \
		    f_c, None, Q_c1,Q_c2,b, fst=False)
	else:
	    u = next_scalar(u, u_1,u_2,f,x,y,t,n,dx,dy,dt,C,b,V,False)
	if action != None:  
	    action(u, x, xv, y, yv, t, i+1)
    return u, time.clock()-t0

def test_cube():
    """
    manufactured solution with a cubic polynomial.
    I've not fitted a correct source term. The polynomial may
    also be wrongly fitted to the boundary conditions.
    """
    exact = lambda x,y,t : x**2*y**2*(2*x-3*Lx)*(2*y-3*Ly)*(1+0.5*t)
    I = lambda x,y : exact(x, y, 0)  
    V = lambda x,y : 0.5*exact(x, y, 0)
    Nx = 30; Ny = 30; Lx = 1; Ly = 1
    T = 2; dt = 0.01; b = 0.2
    C = lambda x, y : 1.2 
    f = lambda x,y,t : 2*(1+0.5*t)*(y*(Ly-y)+x*(Lx-x)) 
    
    def assert_error(u, x, xv, y, yv, t, n):
	u_e = exact(xv, yv, t[n])
	s = u[1:-1,1:-1]; t = u_e[1:-1,1:-1]
	diff = abs(t-s).max()
	nt.assert_almost_equal(diff, 0, places=12)
    version = 'vec'	
    u, t = solve(Nx, Ny, Lx, Ly, T,dt, C, I, V, f,b, assert_error)

def test_const():
    """ check if the program reproduces a constant solution exactly.
    """
    exact = lambda x,y,t : 3.14
    I = lambda x,y : exact(x, y, 0) 
    V = lambda x,y : 0
    Nx = 20; Ny = 22; Lx = 1.7; Ly = 1.3
    T = 2.3; dt = 0.005; b = 0.2
    C = lambda x , y : exp(-x**2-y**2)
    f = lambda x,y,t : 0
    
    def assert_error(u, x, xv, y, yv, t, n):
	u_e = exact(xv, yv, t[n])
	s = u[1:-1,1:-1] 
	diff = abs(s-u_e).max()
	nt.assert_equal(diff, 0)
    for version in ('vec', 'scalar'):
	u, t = solve(Nx, Ny, Lx, Ly, T,dt, C, I, V, f,b, assert_error, version)

def test_plugwave():
    """
    Test, for both directions, if a plug wave as described in the 
    assignement text is correctly passed through the model.
    I do not test this with the scalar version due to time constraints.
    """
    def V(x, y): return 0
    Q = lambda x, y : 1
    Nx = Ny = 20; Lx = Ly = 1
    dtx = 1/float(Nx); dty = 1/float(Ny)
    b = 0;
    Tx = dtx*2*Nx; Ty = 2*dty*Ny
    f = lambda x, y, t : 0
    def Ix(x,y):
	r = zeros((x.shape[0], y.shape[1]))
	r[:,1:2] = 2*ones((r.shape[0],1))
	exact = r
	return r
    def Iy(x,y):
	r = zeros((x.shape[0], y.shape[1]))
	r[1:2,:] = 2*ones((1,r.shape[1]))
	exact = r
	return r

    def assx(u, x, xv, y, yv, t, n):
	print u
	raw_input()
	if t[n] == Tx:
	    exact = zeros((xv.shape[0],yv.shape[1]),dtype=float)
	    exact[:,:] = Ix(xv,yv)
	    diff = abs(exact-u).max()
	    nt.assert_equal(diff,0)

    def assy(u, x, xv, y, yv, t, n):
	if t[n] == Ty:
	    exact = zeros((xv.shape[0],yv.shape[1]),dtype=float)
	    exact[:,:] = Iy(xv,yv)
	    diff = abs(exact-u).max()
	    nt.assert_equal(diff,0)
 
    version = 'vec'
    for T, I, ass,dt in ((Tx, Ix, assx,dtx), (Ty, Iy,assy,dty)):
	u,t = solve(Nx,Ny,Lx,Ly,T,dt,Q,I,V,f,b,ass, version)

def mov(Nx=40, Ny=40,Lx=pi,Ly=pi,T=5,dt=0.005,V=0,f=0,b=0.8, \
	fname='gauss',save_plot = True):
    """
    Runs simulations and visualizes results. If called with all default 
    parameters it will play a movie of a tsunami moving over a box 
    subsea hill.
    """
    fname = fname+'.mpeg'
    Ba = 1; Bs = Lx/4.;Bmx = Lx/4.0;Bmy = Ly/2.0;B0=0;
    I0 = 1.4; Ia = 2 
    I = lambda x,y :I0 + Ia*exp(-(x-Lx)**2)
    def plot_u(u,x,xv,y,yv,t,n):
	if n == 1:
            time.sleep(1)
	if n % 10 == 0:   # We don't need to see every frame
	    surf(u[1:-1,1:-1],axis=[0,u.shape[0],0,u.shape[1],I0-Ba/2,I0+Ia*1.5])
	    time.sleep(0) # pause between frames
	    if save_plot:
		filename = 'tmp_%04d.png' % n
		savefig(filename)  # time consuming!

    def Bcos(x,y):
	tmp = zeros((x.shape[0],y.shape[1]),dtype=float)
	x = x[:,0]; y = y[0,:]
	for i in range(tmp.shape[0]):
	    for j in range(tmp.shape[1]):
		if sqrt((x[i]-Bmx)**2 + (y[j]-Bmy)**2) < Bs:
		    tmp[i,j] = B0+Ba*cos(pi*(x[i]-Bmx)/2.*Bs)* \
				 cos(pi*(y[j]-Bmy)/2.*Bs)
	return  10*(I0-tmp)
    
    def Bgauss(x,y):
	tmp = B0 + Ba*exp(-((x-Bmx)/float(Bs))**2-((y-Bmy)/float(Bs))**2)
	return  10*(I0-tmp)

    def Bbox(x,y):
	x = x[:,0]; y = y[0,:]
	tmp = B0*ones((len(x),len(y)),dtype=float)
	for i in range(len(x)):
	    for j in range(len(y)):
		if x[i] < Bmx+Bs and x[i] > Bmx-Bs and \
				y[j] < Bmy+Bs and y[j] > Bmy-Bs:
		    tmp[i,j] += Ba
	return 10*(I0-tmp)

    if fname == 'gauss.mpeg' : Q = Bgauss
    elif fname == 'cos.mpeg' : Q = Bcos
    elif fname == 'box.mpeg' : Q = Bbox
    else: Q = Bgauss

    u, t = solve(Nx, Ny, Lx, Ly, T, dt, Q, I, V, f,b, \
		action=plot_u, version='vec')
    if save_plot:
	movie('tmp*.png',output_file=fname)
	for filename in glob.glob('tmp*.png'):
	    os.remove(filename)

if __name__=='__main__':
    for hill in ('gauss','cos','box'):
    	mov(fname=hill,save_plot=False)
