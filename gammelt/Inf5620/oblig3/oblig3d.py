#!/usr/bin/env python
from dolfin import *
import numpy as np
from sys import argv
def nonlindiffpicard(alpha, dt, T, f, bc, g, V,I,u_exact, tol=1.0E-05, itmax=1):
    """ Solves the nonlinear diffusion equation 
    g*ut = nabla*alpha(u)nabla*u + f(x,t)
    """

    # Define function space, trial and test functions
    u = TrialFunction(V)
    v = TestFunction(V)
    
    # Define initial condition and picard approximation
    u_1 = interpolate(I, V)
    u_ = interpolate(Constant(0.0), V)

    # Define integrands
    a = g*u*v*dx + dt*alpha(u_)*inner(nabla_grad(u), nabla_grad(v))*dx
    L = (u_1+dt*f)*v*dx

    # Variables and unkowns
    u = Function(V)
    t = 0; it = 0; eps = 1.0
     
    # Exact solution (for verification)
    u_exact = Expression('exp(-t*pi*pi)*cos(pi*x[0])', t = 0)
    
    # Solve for each time step
    while t <= T:
	t += dt
	u_ = u_1
	u_exact.t = t   # Update t in exact solution
	while eps > tol and it < itmax: 
	    problem = LinearVariationalProblem(a, L, u, bc)
	    solver = LinearVariationalSolver(problem)
	    solver.solve()
	
	    diff = u.vector().array() - u_.vector().array()
	    eps = np.linalg.norm(diff, ord=np.Inf)
	    u_.assign(u)
	    it += 1
	eps = 1.0; it = 0
	u_1.assign(u)
	
	# check difference u_e-u
	u_e = interpolate(u_exact, V)
	e = u_e.vector().array() - u_1.vector().array()
	E = np.sqrt(np.sum(e**2)/u.vector().array().size)
	if t > 0.045 and t < 0.055:
	    print 'E, t=%.3f: %-10.3f' % (t, E/dt)

if __name__ == '__main__':

    set_log_level(WARNING)
    Nx = Ny = 10
    if len(argv) > 1: Nx = Ny = int(argv[1])
    mesh = UnitSquareMesh(Nx, Ny)
    V = FunctionSpace(mesh, 'Lagrange',1)
    bc = None	
    f = Constant(0.0)
    alpha = lambda u : 1
    dt = (1/float(Nx))**2; 
    T = 0.5 
    g = 1
    itmax = 1; tol = 1.0E-05
    I = Expression('cos(pi*x[0])')
    u_exact = Expression('exp(-t*pi*pi)*cos(pi*x[0])', t = 0)
    nonlindiffpicard(alpha, dt, T, f, bc, g, V,I,u_exact, tol, itmax)


