#!/usr/bin/env python
from dolfin import *
import numpy as np

def nonlindiffpicard(alpha, dt, T, f, bc, g, V,I,tol=1.0E-05, itmax=1):
    """ Solves the nonlinear diffusion equation 
    g*ut = nabla*alpha(u)nabla*u + f(x,t)
    """

    # Define function space, trial and test functions
    u = TrialFunction(V)
    v = TestFunction(V)
    
    # Define initial condition and picard approximation
    u_1 = interpolate(I, V)
    u_ = interpolate(Constant(0.0), V)

    # Define integrands
    a = g*u*v*dx + dt*alpha(u_)*inner(nabla_grad(u), nabla_grad(v))*dx
    L = (u_1+dt*f)*v*dx

    # Variables and unkowns
    u = Function(V)
    t = dt; it = 0; eps = 1.0
    plot(u_1, interactive=True) 
    # Solve for each time step
    while t <= T:
	u_ = u_1
	while eps > tol and it < itmax: 
	    problem = LinearVariationalProblem(a, L, u, bc)
	    solver = LinearVariationalSolver(problem)
	    solver.solve()
	
	    diff = u.vector().array() - u_.vector().array()
	    eps = np.linalg.norm(diff, ord=np.Inf)
	    u_.assign(u)
	    it += 1
	t += dt
	eps = 1.0; it = 0
	u_1.assign(u)
	plot(u)
	#interactive()

if __name__ == '__main__':
    set_log_level(WARNING)
    mesh = UnitSquareMesh(100, 100)
    V = FunctionSpace(mesh, 'Lagrange',1)
    bc = None
    f = Constant(0.0)
    alpha = lambda u : (1+u)**2
    dt = 0.1; T = 2; g = 1
    itmax = 1; tol = 1.0E-05
    
    I = Expression('cos(pi*x[0])')
    nonlindiffpicard(alpha, dt, T, f, bc, g, V,I, tol, itmax)


