#!/usr/bin/env python
from dolfin import *
import numpy as np
def nonlindiffpicard(alpha, dt, T, f, bc, g, V,I, tol=1.0E-05, itmax=1):
    """ Solves the nonlinear diffusion equation 
    g*ut = nabla*alpha(u)nabla*u + f(x,t)
    """

    # Define function space, trial and test functions
    u = TrialFunction(V)
    v = TestFunction(V)
    
    # Define initial condition and picard approximation
    u_1 = interpolate(I, V)
    u_ = interpolate(Constant(0.0), V)

    # Define integrands
    a = g*u*v*dx + dt*alpha(u_)*inner(nabla_grad(u), nabla_grad(v))*dx
    L = (u_1+dt*f)*v*dx

    # Variables and unkowns
    u = Function(V)
    t = 0; it = 0; eps = 1.0
    
    # Solve for each time step
    while t <= T:
	t += dt
	u_ = u_1
	while eps > tol and it < itmax: 
	    problem = LinearVariationalProblem(a, L, u, bc)
	    solver = LinearVariationalSolver(problem)
	    solver.solve()
	
	    diff = u.vector().array() - u_.vector().array()
	    eps = np.linalg.norm(diff, ord=np.Inf)
	    u_.assign(u)
	    it += 1
	    #if it == itmax: 
	    #	print 'epsilon = %.3f after %d Picard iterations'%(eps, itmax)
	eps = 1.0; it = 0
	u_1.assign(u)
	#print max(u_1.vector().array())
	#plot(u, interactive=True)
	plot(u)
if __name__ == '__main__':
    set_log_level(WARNING)
    I = Expression('exp(-1*(x[0]*x[0]+x[1]*x[1])/(2*s*s))',s = 1)
    Nx = Ny = 20
    mesh = UnitSquareMesh(Nx,Ny)
    V = FunctionSpace(mesh, 'Lagrange',1)
    bc = None	    
    f = Constant(0.0)
    beta = 5
    alpha = lambda u : 1+beta*u*u
    dt = 0.01 
    T = 0.5 
    g = 1
    itmax = 40; tol = 1.0E-05
    nonlindiffpicard(alpha, dt, T, f, bc, g, V,I, tol, itmax)


