#!/usr/bin/env python
from dolfin import *
import numpy as np
def nonlindiffpicard(alpha, dt, T, f, bc, g, V, omega, tol=1.0E-05, itmax=1):
    """ Solves the nonlinear diffusion equation 
    g*ut = nabla*alpha(u)nabla*u + f(x,t)
    """

    I = Expression('cos(pi*x[0])-0*x[1]')

    # Define function space, trial and test functions
    u = TrialFunction(V)
    v = TestFunction(V)
    
    # Define initial condition and picard approximation
    u_1 = interpolate(I, V)
    u_ = interpolate(Constant(0.0), V)

    # Define integrands
    a = g*u*v*dx + dt*alpha(u_)*inner(nabla_grad(u), nabla_grad(v))*dx
    L = (g*u_1+dt*f)*v*dx

    # Variables and unkowns
    u = Function(V)
    t = 0; it = 0; eps = 1.0
     
    # Solve for each time step
    while t < T:
	t += dt
	u_ = u_1
	u_exact.t = t   # Update t in exact solution
	f.t = t		# Update t in source term
	while eps > tol and it < itmax: 
	    problem = LinearVariationalProblem(a, L, u, bc)
	    solver = LinearVariationalSolver(problem)
	    solver.solve()
	
	    diff = u.vector().array() - u_.vector().array()
	    eps = np.linalg.norm(diff, ord=np.Inf)
	    u_.assign(u)
	    it += 1
	    #if it == itmax:
	    #	print 'epsilon = %.4f after %d iterations' % (eps, itmax)
	eps = 1.0; it = 0
	u_1.assign(u)
	
	# check difference u_e-u
	u_e = interpolate(u_exact, V)
	e = u_e.vector().array() - u.vector().array()
	E = np.sqrt(np.sum(e**2)/u.vector().array().size)
	print 'E, t=%.3f: %-10.3f' % (t, E)

if __name__ == '__main__':

    set_log_level(WARNING)
    omega = 0.5
    Nx = 100; g = 1
    mesh = UnitIntervalMesh(Nx)
    u_exact = Expression('t*x[0]*x[0]*(0.5-x[0]/3.0)', t=0)
    f = Expression('-g*x[0]*x[0]*x[0]/3 + g*x[0]*x[0]/2 + 8*t*t*t*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]/9 - 28*t*t*t*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]/9 +7*t*t*t*x[0]*x[0]*x[0]*x[0]*x[0]/2 - 5*t*t*t*3*x[0]*x[0]*x[0]*x[0]/4 + 2*t*x[0] - t',t=0,g=g)
    V = FunctionSpace(mesh, 'Lagrange',1)
    bc = None	    
    alpha = lambda u : 1+u**2
    dt = 0.01; 
    T = 1.0 
    g = 1
    itmax = 10; tol = 1.0E-05
    nonlindiffpicard(alpha, dt, T, f, bc, g, V,omega, tol, itmax)


