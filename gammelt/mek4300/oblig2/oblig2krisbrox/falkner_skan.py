#!/usr/bin/env python
from dolfin import *
set_log_active(False)

def falkner_skan(L = 6, n = 50, beta = 1, init=(0,0)):
    beta = Constant(beta)
    mesh = IntervalMesh(n,0,L)
    V = FunctionSpace(mesh, 'CG', 1)
    VV = V*V
    dfh = TrialFunction(VV)
    vf, vh = TestFunctions(VV)
    fh = interpolate(Constant(init), VV)
    f, h = split(fh)

    F = -dot(h.dx(0), vh.dx(0))*dx + f*h.dx(0)*vh*dx \
	    + beta*(-h*h*vh*dx + vh*dx) + h*vf*dx - f.dx(0)*vf*dx

    def right_boundary(x, on_boundary):
	return on_boundary and abs(x[0]-L) < DOLFIN_EPS
    def left_boundary(x, on_boundary):
	return on_boundary and abs(x[0]) < DOLFIN_EPS	
    
    bcf = DirichletBC(VV.sub(0), Constant(0), left_boundary )
    bch0 = DirichletBC(VV.sub(1), Constant(0), left_boundary)
    bchL = DirichletBC(VV.sub(1), Constant(1), right_boundary)
    bcs = [bcf,bch0,bchL]

    J = derivative(F, fh, dfh)
    solve(F==0, fh , bcs, J=J)
    f_N, h_N = split(fh)
    
    hh = Function(V)
    assign(hh, fh.sub(1))

    dhdy=Function(V)
    dhdy.assign(project(h_N.dx(0), V))

    #plot(hh.dx(0),interactive=True)
    return hh.vector().array(), dhdy.vector().array()
    
if __name__ == '__main__':
    # A
    f_1=[]; f_2=[] 
    for beta in (1, 0.3,0,-0.1,-0.18, -0.19884):
	if beta<0:
	    init=(1,1)
	else:
	    init= (0,0)
	h_ , h__ = falkner_skan(beta = beta, init=init)
	f_1.append((h_,beta)); f_2.append((h__, beta))
    	
    
    from scitools.std import plot, hold, hardcopy
    for i in range(len(f_1)):
	plot(f_1[i][0], legend='beta=%f'%f_1[i][1])
	hold('on')
	hardcopy('falkner_skan_f1.png')		
    hold('off')
    for i in range(len(f_2)):
	plot(f_2[i][0],legend='beta=%f'%f_2[i][1])
	hold('on')
	hardcopy('falkner_skan_f2.png')
    hold('off')
    
    # B
    for init in ((0,0),(1,1)):
	plot(falkner_skan(beta=-0.1, init = init)[0],\
		legend='init=%s'%repr(init))
	hold('on')
	hardcopy('falkner_skan_both.png')


