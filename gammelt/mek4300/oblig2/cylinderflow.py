#!/usr/bin/env python
from dolfin import *
set_log_active(False)

def cylinder(x, on_bound):
    y = (x[0]-0.2)*(x[0]-0.2)+(x[1]-0.2)*(x[1]-0.2) \
	    <= 0.05*0.05 + DOLFIN_EPS
    return on_bound and y    
def sides(x, on_bound):
    y = near(x[1],0) or near(x[1], 0.41)
    return y and on_bound
def inlet(x, on_bound):
    return near(x[0],0) and on_bound
def outlet(x, on_bound):
    return near(x[0], 2.2) and on_bound

H = 0.41; H2 = H**2
xs, ys, r = (0.2, 0.2, 0.05); x0, y0 = (0, 0)
x1, y1 = (2.2, 0.41); 

def ipcs_cylinder(degree=1, nu=1e-03, dt = 0.01, T = 10, beta = 1, n=50, \
	    inlet_speed=Constant((0,0)), Um=0.3, steady=True):
    """ Simulates 2D fluid flow around a cylinder, as per 21.4.5 in the fenics book. 
    """
    mesh = Mesh(Rectangle(0,0,2.2,0.41) - Circle(0.2,0.2,0.05,5*n), n)
    V = VectorFunctionSpace(mesh, 'CG', 1+degree)
    Q = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V); v = TestFunction(V)
    p = TrialFunction(Q); q = TestFunction(Q)
    #tentative
    u1 = Function(V); p1 = Function(Q)
    #prev
    u0 = interpolate(Constant((0,0)), V)
    p0 = interpolate(Constant(0), Q)
    #prevprev
    u00 = interpolate(Constant((0,0)), V)


    bcI = DirichletBC(V, inlet_speed, inlet)
    bcS = DirichletBC(V, Constant((0,0)), sides)
    bcC = DirichletBC(V, Constant((0,0)), cylinder)
    bcu = [bcI, bcS, bcC]
    bcp = DirichletBC(Q, Constant(0), outlet)


    epsilon = lambda u : 0.5*(nabla_grad(u)+nabla_grad(u).T)
    sigma = lambda u, p, nu : 2*nu*epsilon(u)-p*Identity(u.cell().d)
    n = FacetNormal(mesh)

    U = 0.5*(u0+u)
    u_ab = 1.5*u0 - 0.5*u00

    F1 = (1./dt)*inner(u-u0,v)*dx \
        + inner(grad(U)*u_ab, v)*dx \
        + inner(sigma(U, p0, nu),epsilon(v))*dx \
        + inner(p0*n, v)*ds \
        - beta*nu*inner(grad(U).T*n, v)*ds \

    a1 = lhs(F1); L1 = rhs(F1)
    a2 = inner(grad(p), grad(q))*dx
    L2 = inner(grad(p0),grad(q))*dx - (1./dt)*div(u1)*q*dx
    a3 = inner(u,v)*dx
    L3 = inner(u1, v)*dx - dt*inner(grad(p1-p0), v)*dx

    t = 0; k = 0;
    plot_dummy = Function(V)
    u_ = Function(V)
   
    cylinder_ = AutoSubDomain(cylinder)
    mf = FacetFunction('size_t',mesh)
    mf.set_all(0)
    cylinder_.mark(mf,1)
    Cdmax = 0; Clmax=0;   
    deltaP = 0

    while t <= T:
        solve(a1==L1, u1, bcu)
        solve(a2==L2, p1, bcp)
        solve(a3==L3, u_, bcu)
        t += dt
        u0.assign(u_)
        plot_dummy.assign(u_)
        plot(plot_dummy, title='t=%.3f'%(t-dt))
   
	if  not steady:
	    # Forces:
	    tau = -p1*Identity(p1.cell().d)+(grad(u_)+grad(u_).T)
	    n = FacetNormal(mesh)
	    R = VectorFunctionSpace(mesh, 'R', 0)
	    c = TestFunction(R)
	    Fd,Fl = assemble(dot(dot(tau, n), c)*ds(1), \
		    exterior_facet_domains=mf, mesh = mesh)
    
	    Cd = 2*Fd/(Um**2*0.1)
	    Cl = 2*Fl/(Um**2*0.1)

	    if abs(Cd)>abs(Cdmax): Cdmax = Cd
	    if abs(Cl)>abs(Clmax): Clmax = Cl

	    #if t >= 1.5 and t <= 2.0 and k % 5 == 0:
	    #	print p1(0.15,0.20)-p1(0.25, 0.2)

	    k += 1

    if not steady:
	print 'Cdmax=%.4f'%Cdmax
	print 'Clmax=%.4f'%Clmax
    
    if steady:
	# Forces:
	tau = -p1*Identity(p1.cell().d)+(grad(u_)+grad(u_).T)
	n = FacetNormal(mesh)
	R = VectorFunctionSpace(mesh, 'R', 0)
	c = TestFunction(R)
	Fd,Fl = assemble(dot(dot(tau, n), c)*ds(1), \
		exterior_facet_domains=mf, mesh = mesh)
    
	Cd = 2*Fd/(Um**2*0.1)
	Cl = 2*Fl/(Um**2*0.1)
	
	# Resirculation zone:
	x0=0.25; x1=0.25; d = 0.002; L = 0
	while x1<2.2:
	    if u_(x1,0.2)[0] <= 0:
		x1+=d
	    else: L = x1-x0; break
	    

	print 'length of resirculation zone=%f'%L
	print 'Cd=%.3f, Cl=%3f'%(Cd, Cl)
	print 'Delta P=%.4f'%(p1(0.15,0.2)-p1(0.25,0.2))
    
	
	

if __name__=='__main__':
    
    steady = False
    
    UmS = 0.3; UmU = 1.5
    if steady: Um = UmS
    else: Um = UmU

    inlet_speed = Expression(('4*Um*x[1]*(H-x[1])/H2','0'), 
		Um=Um,H=H,H2=H2)
    nu = 1e-03
    
    ipcs_cylinder(T = 2, degree = 0, dt = 0.005, \
	    nu=nu, inlet_speed=inlet_speed, Um=Um, steady = steady)
    
    
    
