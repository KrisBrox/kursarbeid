#!/usr/bin/env python
from dolfin import *
from numpy import log
set_log_active(False)

def turb_plane_channel_flow( v_star=0.05, A = float(26), n=20000):
    nu = Constant(float(5e-5))
    kappa = Constant(0.41)
    v_star=Constant(v_star)
    A = Constant(A)
    dpdx = Constant(0.05*0.05)

    mesh = IntervalMesh(n,-1,1)
    V = FunctionSpace(mesh, 'CG', 1)
    v = TestFunction(V)
    du = TrialFunction(V)
    u = interpolate(Constant(0), V)

    y_plus = Expression('(1-std::abs(x[0]))*v_star/nu', \
	    v_star=v_star, nu=nu) 
    l = Expression('kappa*(1-std::abs(x[0]))*(1-exp(-y_plus/A))',\
	    kappa=kappa, y_plus=y_plus, A = A)

    l_i = interpolate(l, V)
    def boundary(x, on_boundary):
	return on_boundary
    bc = DirichletBC(V, Constant(0), boundary)
    
    F = -nu*u.dx(0)*v.dx(0)*dx + dpdx*v*dx \
		- l_i*l_i*abs(u.dx(0))*u.dx(0)*v.dx(0)*dx   

    J = derivative(F, u, du)
    solve(F==0, u, bc, J=J, \
	    solver_parameters={ 'newton_solver':{'maximum_iterations':20}})
    #plot(u,interactive=True)
    vt = Function(V)
    vt.assign(project(u.dx(0),V))
    
    print 'nu_t at the middle of the channel=%.4f'% \
		(abs(vt(0))*l_i(0)*l_i(0))

    u_plus_analytical = interpolate(y_plus, V)
    u_plus_analytical_vec = u_plus_analytical.vector().array()
    upav = u_plus_analytical_vec
    y_plus_vec = interpolate(y_plus,V).vector().array()
    for i in range(len(u_plus_analytical_vec)):
	if upav[i] >= 5 and upav[i] <= 30:
	    upav[i] = 0
	elif upav[i]> 30:
	    upav[i] = kappa**-1*log(upav[i])+5.5

    u_plus_num_vec = u.vector().array()*20
    import scitools.std as sc
    
    sc.semilogx(y_plus_vec, u_plus_analytical_vec,'b', \
		y_plus_vec, u_plus_num_vec, 'r', \
		hardcopy='verifysolution.png')
    
    return u.vector().array()

if __name__=='__main__':
   turb_plane_channel_flow()










