#!/usr/bin/env python
from dolfin import *
set_log_active(False)

def rotatingcylinder(degree, a, creeping = True, dt = 0.01, T = 0.1, \
			    Re = 1e+3):
    x0, y0 = (0.,-1.)
    x1, y1 = (6., 1.)
    xs, ys, r = (2., 0., 0.25)
    mesh = Mesh(Rectangle(x0, y0, x1, y1) - Circle(xs, ys, r, 300), 40)

    def cyl(x, on_boundary):
	return sqrt((x[0]-2)*(x[0]-2)+x[1]*x[1]) < 0.25+1e-10 \
		    and on_boundary
    def left(x, on_boundary):
	return x[0] < 1e-10 and on_boundary
    def sides(x, on_boundary):
	y = False
	if x[1]>=1-DOLFIN_EPS: y = True
	elif x[1]<= -1+DOLFIN_EPS: y = True
	return y and on_boundary

    V = VectorFunctionSpace(mesh, 'CG', degree+1)
    Q = FunctionSpace(mesh, 'CG', degree)
    VQ = V*Q

    cylspeed = Expression(('-a*x[1]','a*(x[0]-2)'), a = a)
    leftspeed = Expression(('1-x[0]*x[0]','0'))
    bcC = DirichletBC(VQ.sub(0), cylspeed, cyl)
    bcL = DirichletBC(VQ.sub(0), leftspeed, left)
    bcS = DirichletBC(VQ.sub(0), Constant((0,0)), sides)

    Cylinder = AutoSubDomain(cyl)
    mf = FacetFunction('size_t', mesh)
    mf.set_all(0)
    Cylinder.mark(mf,1)

    u, p = TrialFunctions(VQ)
    v, q = TestFunctions(VQ)

    mu = Constant(1.0)
    rho = Constant(1e+3)
    Re = Constant(100)

    F = Re**-1*inner(nabla_grad(u),nabla_grad(v))*dx - inner(p,div(v))*dx-\
		inner(q, div(u))*dx
    up_ = Function(VQ)    
    bcs = [bcC, bcL, bcS]
    solve(lhs(F)==rhs(F), up_, bcs)
    u_, p_ = split(up_)

    up_1 = Function(VQ)
    u_1, p_1 = split(up_1)
    
    up_1.assign(up_)

    #plot(u_, interactive = True)
    plotter = Function(V)
    if not creeping:
	F = F + inner(dot(u_1,nabla_grad(u)),v)*dx + \
		dt**-1*inner((u-u_1),v)*dx
	
	t = 0
	while t < T:
	    solve(lhs(F)==rhs(F), up_, bcs)
	    t += dt
	    cylspeed.a = a*sin(pi*t)
	    up_1.assign(up_)
	    
	    #plotting
	    u_, p_ = up_.split()
	    plotter.assign(u_)
	    plot(plotter)
	    	     
    #beregne krefter:
    tau = -p_*Identity(p_.cell().d)+mu*(grad(u_)+grad(u_).T)
    n = FacetNormal(mesh)
    R = VectorFunctionSpace(mesh, 'R', 0)
    c = TestFunction(R)
    forces = assemble(dot(dot(tau, n), c)*ds(1), \
	    exterior_facet_domains=mf, mesh = mesh)
    
    print forces[0], forces[1]
    
degree = 1; a = 4; dt = 0.01; creeping = False
rotatingcylinder(degree, a, creeping = creeping, dt = dt, T = 1)
