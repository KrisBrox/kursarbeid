#!/usr/bin/env python
from dolfin import *
from numpy import cosh, cos
from time import sleep
set_log_active(False)

dpdx = Constant(-0.01)
mu = Constant(0.01)
a = 1; b = 0.5

class u_exact(Expression):
    def eval(self, value, x):
	value[0] = (-dpdx/(2*mu))*a*a*b*b* \
		(1-x[0]*x[0]/(a*a)-x[1]*x[1]/(b*b))/(a*a+b*b)

def main(h, degree = 1):
    mesh = EllipseMesh(Point(0.0,0.0), [1.0,0.5], h)
    V = FunctionSpace(mesh, 'CG', degree)
    u = TrialFunction(V)
    v = TestFunction(V)
    F = inner(grad(u), grad(v))*dx + 1/mu*dpdx*v*dx
    bc = DirichletBC(V, Constant(0), DomainBoundary())
    u_ = Function(V)
    solve(lhs(F) == rhs(F), u_, bcs=bc)
    u_e = interpolate(u_exact(), V)
    bc.apply(u_e.vector())
    u_error = errornorm(u_e, u_, degree_rise=0)
    #plot(u_e-u_, interactive=True)
    return u_error, mesh.hmin()

e = []; h = []
for size in [0.4, 0.2, 0.1, 0.05, 0.025]:
    ei, hi = main(size, degree = 1)
    e.append(ei); h.append(hi)

from math import log as ln
for i in range(1, len(e)):
    r = ln(e[i]/e[i-1])/ln(h[i]/h[i-1])
    print 'h=%2.2E E=%2.2E r = %.2f'%(h[i], e[i], r)

