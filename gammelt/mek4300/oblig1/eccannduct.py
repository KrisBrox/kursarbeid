#!/usr/bin/env python
from dolfin import *
from numpy import cosh, cos
from time import sleep
set_log_active(False)

dpdx = Constant(-0.01)
mu = Constant(0.01)
a = 1; b = 0.3; c = 0.5

qe_code = '''
class U : public Expression
{
  public:

    double a, b, c, mu, dpdx;

  void eval(Array<double>& values, const Array<double>& x) const
    {
      double q = 0.;
      double factor0 = (-DOLFIN_PI*dpdx)/(8.*mu);
      double F = (pow(a,2)-pow(b,2)+pow(c,2))/(2.*c);
      double M = sqrt(pow(F,2)-pow(a,2));
      double alpha = 0.5*log((F+M)/(F-M));
      double beta = 0.5*log((F-c+M)/(F-c-M));
      double factor1 = 8.*pow(c,2)*pow(M,2);
      double q_temp = 0;
      q += pow(a,4)-pow(b,4)-4*pow(c,2)*pow(M,2)/(beta-alpha);
      for (int i=1; i<150; i=i+1)
        q_temp += i*exp(-i*(beta+alpha))/sinh(i*beta-i*alpha);
      q_temp *= factor1;
      q -=q_temp;
      values[0] = q*factor0;      
    }
};'''

q_c = Expression(qe_code)
q_c.a = float(a); q_c.b = float(b); q_c.c = float(c)
q_c.mu = float(mu(0)); q_c.dpdx = float(dpdx(0))

def main(mesh,degree=1):
    q_c = Expression(qe_code)
    q_c.a = float(a); q_c.b = float(b); q_c.c = float(c)
    q_c.mu = float(mu(0)); q_c.dpdx = float(dpdx(0))

    V = FunctionSpace(mesh, 'CG', degree)
    u = TrialFunction(V)
    v = TestFunction(V)
    F = inner(grad(u), grad(v))*dx + 1/mu*dpdx*v*dx
    bc = DirichletBC(V, Constant(0), DomainBoundary())
    u_ = Function(V)
    solve(lhs(F) == rhs(F), u_, bcs=bc)
    Q = assemble(u_*dx, mesh = mesh)
    print 'exact=%.4f, numerical=%.4f'%(q_c(0), Q) 
    
    return mesh.hmin(), abs(q_c(0)-Q)

if __name__=='__main__':
    degree = 1 
    e = []; h = []
    
    mesh = Mesh('eccannL.xml')
    hi,ei = main(mesh, degree=degree)
    e.append(ei); h.append(hi)
    mesh = Mesh('eccannS.xml')
    hi,ei = main(mesh, degree=degree)
    e.append(ei); h.append(hi)
    mesh = Mesh('eccannXS.xml')
    hi,ei = main(mesh, degree=degree)
    e.append(ei); h.append(hi)

    from math import log as ln
    for i in range(1, len(e)):
	r = ln(e[i]/e[i-1])/ln(h[i]/h[i-1])
	print 'h=%2.2E E=%2.2E r = %.2f'%(h[i], e[i], r)

    
    


