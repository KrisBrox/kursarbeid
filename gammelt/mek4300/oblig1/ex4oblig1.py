#!/usr/bin/env python
from dolfin import *
from numpy import zeros
from scitools.std import linspace
set_log_active(False)

def stokesflowstep(lid_speed = (1,0),N = 50,degree = 1, small = True):
    if small:
	L = 1; W = 0.5; s = 0.1
	mesh  = Mesh('smallstep_dolf.xml')
    else:
	L = 1; W = 0.4; s = 0.2
	mesh  = Mesh('largestep_dolf.xml')
    
    V = VectorFunctionSpace(mesh, 'CG', degree+1)
    Q = FunctionSpace(mesh, 'CG', degree)
    VQ = V * Q

    u, p = TrialFunctions(VQ)
    v, q = TestFunctions(VQ)
   
    # Dividing the boundary
    def top_boundary(x, on_boundary):
	return abs(x[1]-W) < 1E-10 and on_boundary
    def low_boundary(x, on_boundary):
	y = False
	if near(x[0],L/2.) and x[1] < s + DOLFIN_EPS:
	    y = True
	if abs(x[1]) < 1E-10:
	    y = True
	if abs(x[1]-s) < 1E-10 and abs(x[0]) < 0.5 + 1E-10:
	    y = True
	return y and on_boundary
    def left(x,on_boundary):
	return x[0] < 1e-12 and on_boundary
    def right(x,on_boundary):
	return abs(x[0]-1) < 1e-12 and on_boundary
    
    Left = AutoSubDomain(left)
    Right = AutoSubDomain(right)
    Bot = AutoSubDomain(low_boundary)
    Top = AutoSubDomain(top_boundary)
    mf = FacetFunction('size_t', mesh)
    mf.set_all(0)
    Left.mark(mf,1)
    Right.mark(mf,2)
    Bot.mark(mf,3)
    Top.mark(mf,4)


    bc0 = DirichletBC(VQ.sub(0), Constant(lid_speed), top_boundary)
    bc1 = DirichletBC(VQ.sub(0), Constant((0,0)), low_boundary)
    
    mu = 100
    F = mu*(inner(nabla_grad(u),nabla_grad(v)) - inner(p,div(v)))*dx \
		- inner(q,div(u))*dx 

    up = Function(VQ)
    solve(lhs(F)==rhs(F), up, [bc0,bc1])     
    u_ , p_ = up.split()

    # Computing the streamfunction:
    psi = TrialFunction(Q)
    psi_v = TestFunction(Q)
    n = FacetNormal(mesh)
    F = -inner(nabla_grad(psi),nabla_grad(psi_v))*dx \
	+ psi_v*curl(u_)*dx \
	+ inner(-u_[1]*n[0]+u_[0]*n[1],psi_v)*ds
  
    psi_ = Function(Q)
    solve(lhs(F) == rhs(F), psi_, None)
    normalize(psi_.vector())
    if lid_speed == (-1,0):
	psi_.vector()[:] = psi_.vector()*-1
    psi_.vector()[:] -= psi_.vector().min()
    
    file = File('u.pvd') 
    file << u_
    file = File('psi.pvd')
    file << psi_

    plot(psi_, interactive=True)
    
    # Compute velocity flux in and out of domain
    flow_left = assemble(u_[0]*ds(1),exterior_facet_domains=mf, \
		mesh=mesh)
    flow_right = assemble(u_[0]*ds(2),exterior_facet_domains=mf, \
		mesh=mesh)
    print 'The sentence "mass is being conserved" is: %s'\
	    %str(near(flow_left, flow_right))
    print 'Flow on left boundary is %.3f' % flow_left
    print 'Flow on right boundary is %.3f' % flow_right

    # Compute normal stress on bottom wall
    I = Identity(p_.cell().d)
    tau = -p_*I + mu*(grad(u_) + grad(u_).T)
    stress_low_boundary = assemble(dot(dot(n,tau),n)*ds(3),\
	    exterior_facet_domains=mf, mesh=mesh)
    print 'The normal stress on the lower boundary is %.4f'\
	    %stress_low_boundary

    # Find the minumum of the streamfunction
    # Finds the wrong value at the correct point, wonder why?
    x0 = linspace(0.5,L, 1000)
    y0 = linspace(0,L/2., 1000) 
    mini = 1
    loc = (0,0)
    for x in x0:
	for y in y0:
	    if x < L/2. and y < s:
		continue
	    else:
		m = psi_(x,y)
		if m<mini:
		    mini = m
		    loc = (x,y)
    print loc, mini

if __name__=='__main__':
    stokesflowstep(lid_speed=(-1,0))

