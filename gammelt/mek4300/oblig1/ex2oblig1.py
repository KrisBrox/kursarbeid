#!/usr/bin/env python
from dolfin import *
import numpy as np
set_log_active(False)
""" Solves the equation F'''+FF''+1-(F')**2=0,
with boundary conditions F(0)=F'(0)=0 and F'(inf)=1
"""

def newtonplanarflow():
    # Using Newton iterations
    L = 4
    mesh = IntervalMesh(50,0,L)
    V = FunctionSpace(mesh,'CG',1)
    VV = V*V
    dfh = TrialFunction(VV)
    vf, vh = TestFunctions(VV)
    fh = interpolate(Constant((1,1)), VV)
    f, h = split(fh)

    F = -dot(h.dx(0), vh.dx(0))*dx + f*h.dx(0)*vh*dx \
	    - h*h*vh*dx + vh*dx + h*vf*dx - f.dx(0)*vf*dx

    def right_boundary(x, on_boundary):
	return on_boundary and abs(x[0]-L) < DOLFIN_EPS
    def left_boundary(x, on_boundary):
	return on_boundary and abs(x[0]) < DOLFIN_EPS	
    
    bcf = DirichletBC(VV.sub(0), Constant(0), left_boundary )
    bch0 = DirichletBC(VV.sub(1), Constant(0), left_boundary)
    bchL = DirichletBC(VV.sub(1), Constant(1), right_boundary)
    bcs = [bcf,bch0,bchL]

    J = derivative(F, fh, dfh)
    solve(F==0, fh , bcs, J=J)
    f_N, h_N = split(fh)

    plotnewton=plot(f_N)
    plotnewton.write_png('ex2newton')

    # Using Picard iterations
    fh = TrialFunction(VV)
    f, h = split(fh)
    fh_ = interpolate(Constant((1,1)), VV)
    f_, h_ = split(fh_)

    F = -dot(h.dx(0), vh.dx(0))*dx + f_*h.dx(0)*vh*dx \
	    - h_*h*vh*dx + vh*dx + h*vf*dx - f.dx(0)*vf*dx

    max_picard = 100; tol = 1e-10
    fh_1 = Function(VV)
    
    for i in range(max_picard):
	solve(lhs(F) == rhs(F), fh_1, bcs)
	diff = fh_1.vector().array()-fh_.vector().array()
	if np.linalg.norm(diff, ord=np.inf) < tol:
	    break
	fh_.assign(fh_1)
    f_, h_ = split(fh_)

    plotpicard=plot(f_)
    plotpicard.write_png('ex2picard')

newtonplanarflow()

