#!/usr/bin/env python
from dolfin import *
set_log_active(False)

def cavityflow(N = 50,degree = 1):
    mesh  = UnitSquareMesh(N,N)
    V = VectorFunctionSpace(mesh, 'CG', degree+1)
    Q = FunctionSpace(mesh, 'CG', degree)
    VQ = V * Q

    u, p = TrialFunctions(VQ)
    v, q = TestFunctions(VQ)
    def top_boundary(x, on_boundary):
	return abs(x[1]-1) < 1e-10 and on_boundary
    def other_boundary(x,on_boundary):
	return on_boundary and not top_boundary(x, on_boundary)
    bc0 = DirichletBC(VQ.sub(0), Constant((1,0)),top_boundary)
    bc1 = DirichletBC(VQ.sub(0),Constant((0,0)), other_boundary)

    mu = 100
    F = mu*(inner(nabla_grad(u),nabla_grad(v)) - inner(p,div(v)))*dx \
		- inner(q,div(u))*dx

    up = Function(VQ)
    solve(lhs(F)==rhs(F), up, [bc0, bc1])
     
    u_ , p_ = up.split()
    
    #plot(u_, interactive = True)
    return u_
    
def cavitystreamfunc(u, N = 50, degree = 1): 
    mesh = UnitSquareMesh(N,N)
    V = FunctionSpace(mesh, 'CG', degree)

    psi = TrialFunction(V)
    psi_v = TestFunction(V)
    def top_boundary(x, on_boundary):
	return abs(x[1]-1) < 1e-10 and on_boundary
    def other_boundary(x,on_boundary):
	return on_boundary and not top_boundary(x, on_boundary)
    bc0 = DirichletBC(V, Constant(0), top_boundary)
    bc1 = DirichletBC(V, Constant(0), other_boundary)
    bc = [bc0,bc1]

    n = FacetNormal(mesh)
    wz = u[0].dx(1) - u[1].dx(0)
    F = inner(nabla_grad(psi),nabla_grad(psi_v))*dx + psi_v*wz*dx \
		- psi_v*inner(grad(psi),n)*ds
  
    psi = Function(V)
    solve(lhs(F)==rhs(F), psi, bc)
	
    #plot(psi, interactive=True) 
    
    return psi

if __name__=='__main__':
    N = 50
    u = cavityflow(N = N, degree = 1)
    psi = cavitystreamfunc(u, N = N, degree = 1)
    
    # Bad way of finding the minimum
    import scitools.std as st
    x = st.linspace(0,1,100)
    y = st.linspace(0,1,100)
    mini = 0
    for i in x:
	for j in y:
	    if psi(i,j) < mini:
		mini = psi(i,j)
		pos = (i,j)
    print "position of the center of the vortex: (%.2f,%.2f)" %pos
    print 'value of psi at minimum: %.5f, error=%.5f'% \
		(mini,min(psi.vector()-mini))

    
