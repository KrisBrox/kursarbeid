#!/usr/bin/env python
from scitools.std import *

x3 = 0.0; x2 = -0.538469310105683;x1 = -0.906179845938664
x4 = -x2; x5 = -x1; 
w0 = 0.568888888888889; w1 = 0.478628670499366; w2 = 0.236926885056189
x = [x1, x2, x3, x4, x5]; w = [w2, w1, w0, w1, w2]

def compute(f):
    ret = 0
    for i in range(5):
	ret += f(x[i])*w[i]
    return ret 

if __name__ == '__main__':
    f = lambda x : x**4*sin(pi*x)**2
    print compute(f)
