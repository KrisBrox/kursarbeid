#!/usr/bin/env python
from scitools.std import zeros, ones, tan, pi
from numpy import *
from scitools.std import *

def evaluaterational(p,x):
    return (p[0]+p[1]*x+x**2)/(p[2]+p[3]*x+p[4]*x**2)
def evaluatepolynomial(p,x):
    ans = 0
    for i in range(len(p)):
	ans += p[i]*x**i
    return ans

x = array([pi/180., pi/90., pi/60., pi/45., pi/36.])
f = [1/tan(y) for y in x]
f = asarray(f)

A = zeros((5,3), dtype=float)
for i in (0,1,2):
    for j in range(len(x)):
	A[j,i] = x[j]**i

B = zeros((5,3),dtype=float)
for i in range(3):
    B[:,i] = f*A[:,i]

M = zeros((5,5), dtype=float)
for i in range(2):
    M[:,i] = A[:,i]
    
for i in range(3):
    M[:,i+2] = -B[:,i]

p = linalg.solve(M,-A[:,2])
#print ['%.4f'%evaluaterational(p,y) for y in x]
print evaluaterational(p,5*pi/360.)

for i in range(5):
    M[:,i] = x**i
q = linalg.solve(M,f)
#print ['%.4f'%evaluatepolynomial(q,y) for y in x]
print evaluatepolynomial(q,5*pi/360.)

