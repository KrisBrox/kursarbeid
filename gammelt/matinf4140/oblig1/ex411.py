#!/usr/bin/env python
from scitools.std import pi, linspace, zeros, cos
import scipy
from scitools.std import *
from numpy.random import rand

cheb = lambda i,n : cos((pi*(2*i-1))/(2.*n))

def vandermonde(x): 
    ret = zeros((len(x),len(x)))
    for i in range(len(x)):
	ret[i,:] = x**i 
    ret = scipy.matrix(ret)
    return ret

def evaluate(p,x):
    px = 0; 
    for i in range(len(p)):
	px += p[i,0]*x**i
    return px
    
def interpolate(n, f,fro,to,a = True, points = 'eq'):
    if points == 'equidistant': x = linspace(fro,to,n)
    elif points == 'chebychev': 
	x = zeros(n)
	for i in xrange(1,n+1):
	    x[i-1] = i
	x = cheb(x,n)
    V = vandermonde(x)
    if a: f_ = f(x)
    else: f_ = rand(n)
    f_ = scipy.matrix(f_)
    return V.T**-1*f_.T

def a(f):
    fro = -1; to = 1
    for n in (8,16):
	for points in ('equidistant','chebychev'):
	    p = interpolate(n,f,fro,to,True,points)
	    x = linspace(-1,1,100)
	
	    semilogy(x,abs(evaluate(p,x)-f(x)), \
		title = 'plot of |f(x)-p(x)|, '+points+ ' n=%d'%n, \
		hardcopy = points + '%d.png'%n	)

def b(f):
    fro = -1; to = 1
    for n in (8,16):
	p = interpolate(n,f,fro,to,False,'equidistant') 
	x = linspace(-1,1,1000)
	plot(x, abs(evaluate(p,x)), \
	    title='plot of |p(x)| on [-1,1], random f (on unit interval), n=%d'%n, \
	    hardcopy='randomf%dpoint.png'%n )


if __name__ == '__main__':
    f = lambda x : (x+3)**-1
    a(f)
    b(f)
