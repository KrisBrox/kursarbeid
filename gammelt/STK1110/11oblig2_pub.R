# R-kommandoer som kan v�re nyttige, oblig 2. Husk at ved � skrive
# help() f�r du detaljert beskrivelse av angitte kommando, 
# f.eks vil help(mean) forklare hvoran du finner gjennomsnitt av en
# tallrekke.
#
# Du kan ogs� kanskje ha nytte av R-kodene som er lagt
# ut sammen med under "Forelesninger" p� kurshjemmesiden.
#

# Oppgave 2
## les inn data
krtemp<-read.table("http://www.uio.no/studier/emner/matnat/math/STK1110/h11/undervisningsmateriale/kroppstemp.txt",header=F,row.names=NULL)         # object "data frame", often called "data matrix".
# Kommandoen krtemp viser innoldet
colnames(krtemp)<-c("Menn","Kvinner")
#N� gir kommandoen krtemp$Menn innholdet i kolonnen Menn,
#length(krtemp$Menn) gir lengnen p� kolonnen
#a)
#boxplot()
#b)
#qqnorm()
#c)
# f�rst bruk av formlene direkte
# her kan du f� bruk for :
# empirisk gjennomsnitt mean(),
# empirisk varians var(), svarer til s^2 i D&B
# sqrt()
# qt() angir fraktiler/kritiske verdier for t-fordelingen
# pt() svaret til kumulativ fordelingsfunksjonen i t-fordelingen
# R-prosedyren for t-tester er t.test()

# Oppgave 3
# leser inn data
snake<-read.table("http://www.uio.no/studier/emner/matnat/math/STK1110/h11/undervisningsmateriale/snake.txt",header=F,row.names=NULL)         # object "data frame", often called "data matrix".
colnames(snake)<-c("Sn�innhold","Vannstand")
# a)
# f�rst direkte ved formlene
# her kan du f� bruk for kommandoene
# sum() summen av tallene i en rekke tall
# De fire  f�lgende kommandoene vil tilpasse en modell, "mod",
# til dataene plotte x ot y og tegne inn en rett linje
# med konstantledd a og stigningskoeffesien b:
#mod<-lm(Vannstand~ Sn�innhold, data=snake)
#summary(mod)
#plot(x,y,xlab="Sn�innhold",ylab="Vannstand")
#abline(a,b)
# mod$coeff[1] gir det estimerte konstantledd i mod
# mod$coeff[2] gir det estimerte stigningsforhold i mod
# mod$res gir residualene i mod, og mod$res*mod$res er en vektor med
# kvadratet av residualene
# mod$fitted gir de tilpassede verdiene  i mod
# 




