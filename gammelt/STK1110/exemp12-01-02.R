# PC skjermer, eksempel 12.1
viddsp<-read.table("http://www.uio.no/studier/emner/matnat/math/STK1110/h11/undervisningsmateriale/exmp12-01.txt",header=T,row.names=NULL)         # object "data frame", often called "data matrix".
plot(viddsp$x,viddsp$y,xlab="palwidth",ylab="OSA")
#
#Skogsd�d, eksempel 12.2
forestgrth<-read.table("http://www.uio.no/studier/emner/matnat/math/STK1110/h11/undervisningsmateriale/exmp12-02.txt",header=T,row.names=NULL)         # object "data frame", often called "data matrix".
par(mfrow=c(2,1))
plot(forestgrth$x,forestgrth$y,xlab="soilPH",ylab="%dieback")
plot(c(0,6),c(0,15),type="n",xlab="soilPH",ylab="%dieback")
points(forestgrth$x,forestgrth$y)
par(mfrow=c(1,1)) # resetter grafisk vindu
#

