static char *itoa_put (char *buf, int n)
{
  if (n > 9) buf = itoa_put(buf, n/10);
  *buf = (n%10)+'0';
  return buf+1;
}

char *my_itoa (int v)
{
  char res[20], *resp = res;

  if (v < 0) {
    *resp = '-';  ++resp;  v = -v;
  }
  resp = itoa_put(resp, v);  
  *resp = 0; /* Husk 0-byte til slutt! */
  return strdup(res);
}
