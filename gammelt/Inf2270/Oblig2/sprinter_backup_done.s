	.globl sprinter, storage1, storage2, offset, ten, num, boolean
	# C-signature: void sprinter(char* str, char* inp, ...);
	# Registers:	%eax:	address of str
	#		%ecx:	length of the produced string
	#		%edx:	address of inp
	.data
	storage1:	.long   0
	storage2:	.long	0
	offset:		.long	0
	num:		.long	0
	boolean:	.long	0
	ten:		.long	10

sprinter:
	pushl	%ebp		    #Standard
	movl	%esp, %ebp	    #funksjonsstart

	movl	12(%ebp), %edx	    # address of inp 
	movl	8(%ebp), %ecx	    # address of str
	xorl	%ebx, %ebx
	xorl    %eax, %eax

loop:				    # while inp[index] != 0
	cmpb	$0x25, 0(%edx)	    # if (inp[i] == '%')
	jne	cpy		    # inp[i] != '%'
	movl	$0, offset
	movl	$0, %eax
	movl	$10, ten
	jmp	found

found:
	incl	%edx
	cmpb	$0x25, (%edx)	    # if (inp[i] == '%')
	je	cpy	
	
	cmpb	$0x73, (%edx)	    # if (inp[i] == 's')
	je	str		    #	

	cmpb	$0x64, (%edx)	    # if (inp[i] == 'c')
	je	number

	cmpb	$0x63, (%edx)	    # if (inp[i] == 'd')
	je	char

	cmpb	$0x78 , (%edx)	    # if (inp[i] == 'x')
	je	hex

	cmpb	$48, (%edx)	    # if (48 <= inp[i] >= 57)
	jge	maybe_offsetting
	
	jmp	loop

maybe_offsetting:
	cmpb	$57, (%edx)
	jle	calc_offset

char:
	incl	%edx		    # skip the '%c'
	addl	$0x04, %ebp	    # increase the stack pointer
	movb	12(%ebp), %bl	    # get the argument char
	cmpl	$0, %eax	    # need to add spaces?
	jne	add_spaces_char

char_cont:
	movb	%bl, (%ecx)	    # put it where it belongs
	incl	%ecx		    # increase the pointer to the output
	xorl	%ebx, %ebx	    # %ebx = 0
	xorl	%eax, %eax	    # %eax = 0
	jmp	loop		    # countinue reading input

add_spaces_char:		    
	cmpl	$1, %eax	    # reserve room for the last char
	je	char_cont	    # 
	movb	$32, (%ecx)	    # add a ' '
	decl	%eax		    # one less space to go
	incl	%ecx		    # increment the output pointer
	jmp	add_spaces_char	    # continue

number:
	incl	%edx		    # increment i
	movl	%edx, storage1	    # save the input pointer
	addl	$4, %ebp	    # increase stack pointer
	movl	%eax, offset	    # save the width argument
	movl	12(%ebp), %eax	    # %eax = arg
	xorl	%edx, %edx	    # %edx gets ready
	movl	$0, num		    # no cyphers yet
	movl	$0, boolean
	cmpl	$0, %eax	    # %eax < 0 ?
	jl	negative	    # fix that
	cmpl	$0, %eax	    # %eax == 0 ?
	je	zero		    
	jmp	my_itoa		    

zero:				    
	pushl	$48		    # push a '0'
	incl	num		    # num = 1
	jmp	append_nums	    # go to appending

negative:			    # add a '-'
	cmpl	$10, ten	    # check if arg is dec or hex
	jne	its_a_neg_hex	    # it's a hex!
	movl	$1, boolean	    # remember the '-'
	decl	offset		    # one less space
	negl	%eax		    # eax *= -1
	jmp	my_itoa
	
my_itoa:			    # converting a number to char*
	xorl	%edx, %edx	    # %edx = 0
	cmpl	$0, %eax	    # number/ten == 0 ?
	je	append_nums	    # put the in the numbers
	idivl	ten		    # %eax/=10, %edx= %eax%10
	addl	$48, %edx	    # convert to ascii (dec)
	cmpl	$58, %edx	    # convert to ascii (hex)
	jge	it_is_hex

itoa_cont:
	pushl	%edx		    # put eax%10 on the stack
	incl	num		    # number of cyphers++
	jmp	my_itoa		    # continue
	
append_nums:
	movl	offset, %eax	    # %eax = remaining spaces
	cmpl	%eax, num	    # add more spaces?
	jl	add_space_num	    # 
	cmpl	$0, boolean	    # need to add a '-'?
	jne	add_one_line_num    #
	decl	offset		    # dont add more spaces!
	cmpl	$0x00, num	    # all cyphers added?
	je	number_end	    # 
	decl	num		    # one less to go
	popl	%ebx		    # take a cypher from the stack
	movl	%ebx, (%ecx)	    # and add it to the output
	incl	%ecx		    # increment the output pointer
	jmp	append_nums	    # continue

add_one_line_num:		    # adds a '-'
	movb	$45, (%ecx)	    # set str[i] = '-'
	incl	%ecx		    # increment the output pointer
	movl	$0, boolean	    # remember you have added a '-'
	jmp	append_nums

add_space_num:			    # adds a ' '
	movb	$32, (%ecx)	    # set str[i] = ' '
	incl	%ecx		    # increment the output pointer
	decl	offset		    # remember you have added a ' '
	jmp	append_nums

number_end:
	movl	$0, %eax	    # %eax = 0
	xorl	%ebx, %ebx	    # %ebx = 0
	movl	storage1, %edx	    # retrieve input pointer
	jmp	loop		    # continue going through input

hex:
	movb	$16, ten	    # fix the divider to get hexvals
	jmp	number		    # from number and not decimals

it_is_hex:
	addl	$39, %edx	    # correct the ascii value
	jmp	itoa_cont	    # to get hex

its_a_neg_hex:			    # no need to add a '-' to
	jmp	my_itoa		    # get negative hex

str:
	incl	%edx		    # increase the input pointer
	pushl	%edx		    # stash it in memory
	addl	$4, %ebp	    # increment the stack pointer
	movl	12(%ebp), %edx	    # %edx = next arg
	movl	%eax, offset	    # offset = minimum width
	cmpw	$0, offset	    # if offset >= 0
	jge	get_str_length	    # get the length of the string
	jmp	str_loop	    #

str_loop:
	cmpb	$0x0, (%edx)	    # reached end of arg?
	je	str_end		    # done
	movb	(%edx), %bl	    # %bl = next char
	incl	%edx		    # increment arg pointer 
	movb	%bl, (%ecx)	    # put the char where it belongs
	incl	%ecx		    # increment the output pointer
	jmp	str_loop	    # continue
	
get_str_length:			    # do the same as in str_loop
	cmpb	$0x0, (%edx)	    # only instead of copying
	je	calc_offset_str	    # count the number of chars.
	incl	%edx
	decl	offset
	jmp	get_str_length

calc_offset_str:		    
	movl	12(%ebp), %edx
	movl	$0, %ebx
	jmp	add_spaces_str

add_spaces_str:
	cmpl	$0x0, offset	    # put as many spaces as 
	jle	str_loop	    # required before the argument 
	movb	$32, (%ecx)	    # string.
	incl	%ecx		    # increment the output pointer
	decl	offset		    # one less space to go
	jmp	add_spaces_str

str_end:
	movl	$0, offset	    # offset = 0
	movl	$0, %eax	    # %eax = 0
	popl	%edx		    # retrieve the input pointer
	jmp	loop		    # continue

cpy:				    # copies a char from input to output
	movb	(%edx), %bl	    # get the char from input
	incl	%edx		    # increment input pointer
	movb	%bl, (%ecx)	    # place the char in the right spot
	incl	%ecx		    # increment the output pointer
	cmpb	$0,%bl		    # was the char a 0 ?
	je	exit		    # then we're done
	jmp	loop		    # if not we continue

exit:				    
	subl	8(%esp), %ecx	    # how long is the produced 
	decl	%ecx		    # string?
	movl	%ecx, %eax	    # put that number in %eax
	popl	%ebp		    # restore the stack pointer
	ret			    # return

calc_offset:
	movb	(%edx), %bl	    # move the value to %bl
	subb	$48, %bl	    # convert from ascii to decimal
	mulb	ten		    # multiply the old width val with ten
	addl	%ebx, %eax	    # add the value of the last cypher 
	jmp	found    
