
/**
* Binary heap implemented with an array, for use with the {@link Graph}-class.
* Nodes are sorted by number of predecessors.
*/
public class Queue {
    private Vertex[] tasks; 
    private int lastIndex;

    /**
    * Constructor, builds a heap with the argument array.
    * @param arr A vertex array.
    */
    Queue (Vertex[] arr) {
	tasks = new Vertex[arr.length+1];
	for (int i = 1; i < tasks.length; i++) {
	    tasks[i] = arr[i-1];
	}
	lastIndex = tasks.length -1;
	minHeapify();
    }

    /**
    * Constructor, makes an empty heap of the specified size.
    * @param initialSize The number of nodes this heap can contain.
    */
    Queue (int initialSize) {
	tasks = new Vertex[initialSize+1];
	lastIndex = 0;
    }

    /**
    * Method for determining if the heap is empty.
    */
    public boolean isEmpty() {
	if (lastIndex == 0) 
	    return true;
	return false;
    }

    /** 
    * Insert a Node into the Heap. Fails if the heap is full.
    * @param v The node to be inserted.
    * @return success ? true : false
    */
    public boolean insert(Vertex v) {
	if (lastIndex == tasks.length - 1)
	    return false;
	else {
	    lastIndex++;
	    tasks[lastIndex] = v;
	    percUp(lastIndex);
	    return true;
	}
    }

    /**
    * Sort the heap by number of predecessors.
    */
    private void minHeapify() {
	for (int i = lastIndex; i > 1; i--) {
	    percUp(i);
	}
    }

    /**
    * Delete the minimum node of the heap (root) and return it.
    * @return The previous root.
    */
    public Vertex deleteMin() {
	Vertex tmp = tasks[1];
	tasks[1] = tasks[lastIndex];
	percDown(1); 
	lastIndex--;
	return tmp;	
    }
    
    /**
    * Find the minimum node (=root) of the heap.
    * @return Root.
    */	
    public Vertex findMin() {
	return tasks[1];
    }

    /**
    * Not in use.
    */
    public void decreaseKey(Vertex v) {
	for (int i = 1; i < lastIndex; i++) {
	    if (tasks[i] == v) {
		if (tasks[i].cntPred > 0) tasks[i].cntPred--; 
	    }
	}
    }

    /**
    * Move the node at position i to its proper position.
    * @param i The index of the node to be moved.
    */
    private void percDown(int i) {
	Vertex tmp; boolean least;
	while (true) {
	    if (i*2 > lastIndex)  
		return;
	    if (!(i*2+1 > lastIndex) && tasks[i*2+1].cntPred 
		< tasks[i*2].cntPred) { least = false;} else least = true; 
    	    if (least && tasks[i].cntPred >= tasks[i*2].cntPred) {
		tmp = tasks[i*2]; tasks[i*2] = tasks[i]; tasks[i] = tmp;
		i = i*2;
	    }  else if (!least && tasks[i].cntPred >= tasks[i*2 + 1].cntPred) {
		tmp = tasks[i*2 +1]; tasks[i*2 + 1] = tasks[i]; tasks[i] = tmp;
		i = i*2+1;
	    }
	}
    }

    /**
    * Move the node at position i to its proper place.
    * @param i the position of the node to be moved.
    */
    private void percUp(int i) {
	Vertex tmp;
	while (true) {
	    if ((int) i/2 == 0)  
		return;
	    if (tasks[i].cntPred <= tasks[(int) i/2].cntPred) {
		tmp = tasks[(int) i/2];
		tasks[(int) i/2] = tasks[i];
		tasks[i] = tmp; i = (int) i/2;
	    } else { return; }
	}
    }
    
    /**
    * Print the queue in order from least to most predecessors.
    */ 
    public void print() {
	for (int i = 1; i< tasks.length; i++) {
	    System.out.print(deleteMin());
	}
    }
}
