import java.util.ArrayList;

/**
* A class for vertices, for use with {@link Graph} class.
*/
public class Vertex {
    ArrayList<Vertex> outEdges = new ArrayList<Vertex>(); 
    ArrayList<Vertex> pred = new ArrayList<Vertex>();
    int cntPred;
    String id;
    Vertex(String id) {
	this.id = id;
    }
}
