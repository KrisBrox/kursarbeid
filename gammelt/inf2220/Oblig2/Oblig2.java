import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;

/**
* This is the second obligatory assignement of INF2220 at the University of Oslo
* <p>
* All code written by Kristian Brox.
*/ 
public class Oblig2 {
    static Project<Task> project;
    static String fileName;

    /**
    * @param args Must specify the .txt file 
    * containing the tasks of the project to be planned.
    */
    public static void main(String[] args) {
	project = new Project<Task>();
	fileName = args[0];
	readFile(fileName);
	if (project.findLoop()) {
	    System.out.print("\nFound circular dependencies! this program is exiting.\n\n");
	} else {
	    shell();
	}	
    }

    /**
    * Print the menu, explaining the user's options.
    */
    private static void displayMenu() {
	System.out.print("\n*** Project Manager ***\n\nThere are four choices:\n\tPress 1 to get the answer to task two of the assignement.\n\tPress 2 to get the answer to task three.\n\tPress q to quit.\n\tPress anything else to display this menu again.\n");
    }

    /**
    * Runs the (incredibly simple) user interface for the program.  
    */
    private static void shell() {
	// User Interface.
	Scanner sc = new Scanner(System.in);
	String s;
	displayMenu();
	while (true) {
	    System.out.print("\n>> ");
	    s = sc.nextLine();
	    if (s.equals("1")) {
		System.out.print(sim());
	    } else if (s.equals("2")) {
		listCritical();
	    } else if (s.equals("q")){
		System.out.print("Until we meet again!\n\n");
		break;
	    } else {
		displayMenu();
	    }
	}
    }
    
    /** 
    * Reads the specified file and builds a project out of the tasks listed 
    * in the file.
    *<p>
    * @param filename The name of a properly formatted .txt 
    * task list file. 
    */
    private static void readFile(String filename) {
	File file = new File(filename);
	try {
	    Scanner sc = new Scanner(file);
	    while (sc.hasNextLine()) {
		String[] line = sc.nextLine().split("\\s+");
		if (line.length > 1) {
		    Task t = new Task(line[0]);
		    if (!project.containsKey(t.id)){
			project.addVertex(t, t.id);
			t.setVars(line[1], Integer.parseInt(line[2]), 
				    Integer.parseInt(line[3]));
			int i = 4;
			while (!line[i].equals("0")) {
			    Task n = new Task(line[i]);
			    if (!project.containsKey(n.id)){
				project.addVertex(n, n.id);
				n.addNeighbor(t); t.cntPred++;
			    } else {
				n = (Task) project.get(n.id);
				n.addNeighbor(t); t.cntPred++;
			    }
			    i++;
			}
		    } else {
			t = (Task) project.get(t.id);
			t.setVars(line[1], Integer.parseInt(line[2]), 
				    Integer.parseInt(line[3]));
			int i = 4;
			while (!line[i].equals("0")) {
			    Task n = new Task(line[i]);
			    if (!project.containsKey(n.id)){
				project.addVertex(n, n.id);
				n.addNeighbor(t); t.cntPred++;
			    } else {
				n = (Task) project.get(n.id);
				n.addNeighbor(t); t.cntPred++;
			    }
			     i++;
			}
		    }
		}
	    }
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	}
    }
    

    /**
    * Returns a String that is a list of times when any task is either 
    * starting or stopping, along with lists of which tasks are starting 
    * or stopping, in accordance with the specification of task two of the 
    * assignement. The formatting of the string is nearly identical to that
    * of the example output in the assignement text.
    * @return The starting and stopping time of each task, and the earliest 
    * possible completion time.
    */
    static String sim() {
	String output = "";
	ArrayList<Task> work = new ArrayList<Task>(); 
	int time  = 0; Task t, tmp; int numTasks = 0;
	boolean wroteTime = false; int staff = 0;
	Queue q = new Queue((Task[]) project.toArray(new Task[0]));
	do {
	    t = (Task) q.deleteMin();
	    t.start = 0; t.stop = t.time;
	    work.add(t); numTasks++; 
	} while (q.findMin().cntPred == 0);
	while (numTasks > 0) {
	    wroteTime = false;
 	    for (int i = 0; i < work.size(); i++) {
		t = work.get(i);
		if (time == t.stop) {
		    if (!wroteTime) { 
			output += "time "+time+":\n";
			wroteTime = true;	
		    }
		    output += "\tFinished: "+t.id+"\n";
		    staff -= t.staff;
		    numTasks--;
		    for (Vertex u : t.outEdges) {
			tmp = (Task) u;
			if (--tmp.cntPred == 0) { 
			work.add(tmp);
			tmp.start = time; tmp.stop = time+tmp.time;
			numTasks++;
			}
		    }
		}
		if (time == t.start) {
		    if (!wroteTime) {
			output += "time "+time+":\n";
			wroteTime = true;
		    }
		    staff += t.staff;
		    output += "\tStarting: "+t.id+"\n";  
		}
	    }
	    time++;
	    if (wroteTime) 
		output += "\tCurrent staff:" + staff+"\n";
	}
	time--;
	output += "*** Shortest possible project execution is "+time+" ***\n";
	project.clear();
	readFile(fileName);
	return output;
    }
    
    /**
    * Prints most information about tasks of the project, ordered by id.
    * <p>
    * Calls {@link time} to determine earlist and latest start of each task.
    */
    static void listCritical() {
	ArrayList<Vertex> stuff = project.topologicalSort();
	Task tmp = new Task(Integer.toString(stuff.size()+1));
	for (int j = 0; j<stuff.size(); j++) {
	    if (stuff.get(j).outEdges.size() == 0)
		tmp.pred.add((Task) stuff.get(j));	
	}
	time(tmp);
	for (int i = 1; i<= project.size(); i++) {
	    String s = Integer.toString(i);
	    System.out.print(project.get(s));   
	}
    }
    
    /**
    * Recursively determines the earliest and latest time that a task can be 
    * started.
    */
    static int time(Task t) {
	int most = 0;
	if (t.pred.size() == 0) { 
	    return t.time;
	}
	for (int i = 0; i < t.pred.size(); i++) {
	    Task v = (Task) t.pred.get(i);
	    int tmp = time(v);
	    v.start = tmp - v.time;
	    if (most < tmp) 
		most = tmp; 
	}   
	for (int i = 0; i < t.pred.size(); i++) {
	    Task v = (Task) t.pred.get(i);
	    v.latestStart = most-v.time;
	    v.slack = v.latestStart - v.start;
	}
	return most + t.time;
    }
}

/** 
* For all intents and purposes identical to it's parent class, this class 
* is not necessery, and i apologize for not having removed it. 
*/
final class Project<Task> extends Graph<Vertex> {
    Project() {

    }
}

/**
* 
*/
class Task extends Vertex {
    int staff, time;
    String name;
    int start, stop, latestStart = 0, slack = 0;   

    /**
    * Constructor.
    
    * @param id The id of the Task. 
    */
    Task(String id) {
	super(id);
    }

    /**
    * Adds an edge from this to t, and adds this to t's list of predecessors.

    * @param t the neighbor of this task.
    */
    public void addNeighbor(Task t) {
	outEdges.add(t);
	t.pred.add(this);
    }

    /**
    * @return The relevant information about this task.
    */
    public String toString() {
	String ans = "";
	for (int i = 0; i < outEdges.size(); i++) {
	    ans += outEdges.get(i).id+", ";
	}
	return "Id: "+id+", Name: "+name+", Time: "+time+" Staff: "+staff+
		    " Latest start: "+latestStart+" Slack: "+slack+
		    "\nSuccessors: "+ans+"\n";
    }

    /**
    * Updates the information about this task.
    */
    public void setVars(String name, int time, int staff) {
	this.name = name; this.time = time; this.staff = staff;
    }
}
