public class HashMap<E> {
    private int size = 0;
    @SuppressWarnings({"unchecked"})
    private E[] table = (E[]) new Object[11];
    HashMap() { }

    private void put2(E e, E[] items) {
	int hashCode = e.hashCode() % items.length;
	if (items[hashCode] != null) {
	    int i = 1;
	    while (true) {
		if (items[(hashCode+i*i)%items.length] != null) {
		    i++;
		    continue;
		}
		items[(hashCode+i*i)%items.length] = e;
		break;
	    }
	} else 
	    items[e.hashCode()%items.length] = e;
    }

    public void put(E e, String key) {
	size++;
	if (size > table.length/2)
	    reHash();
	put2(e, table);
    } 

    public E get(String key) {
	if (key == null) return null;
	int hashCode = 0;
        for (int i = 0; i < key.length(); i++) {
	    hashCode += 37*key.charAt(i);
	}
	int i = 0;
	while (true) {
	    E tmp = table[(hashCode+i*i)%table.length]; 
	    if (tmp.hashCode() == hashCode) {
		return tmp;
	    } else {
		i++;
		continue;
	    }
	}
    }

    private void reHash() {
	@SuppressWarnings({"unchecked"})
	E[] tmp = (E[]) new Object[table.length*2 + 1];
	for (int i = 0; i < table.length; i++) {
	    if (table[i] != null)
		put2(table[i], tmp);
	}
	table = tmp;
    } 

    public int size() {
	return size;
    }
}
