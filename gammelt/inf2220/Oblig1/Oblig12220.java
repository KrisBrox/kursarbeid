import java.io.*;
import java.util.Scanner;
import java.math.*;
import java.util.Random;

// by Kristian Brox

class Oblig12220 {
    public static void main(String[] args) {
	EtternavnTre etternavnTre = new EtternavnTre(args[0]);
	etternavnTre.ordreLokke();
    }
}

class EtternavnTre {
    public BinTreNode root;
    int numUniLast = 0, temp1 = 0, temp2 = 0;
    String mostCommon;
    BinTreNode flest1[] = new BinTreNode[20];
    BinTreNode flest2[] = new BinTreNode[20];
    BinTreNode nonUniques[] = new BinTreNode[10];
    CCTre ccTre = null;

    EtternavnTre(String filename) {
	// Remove the comments on the line directly below this one to get a bigger, randomly generated tree. adding a number of entries above a certain amount will cause the program to crash if running certain commands (exception reported at line 355). I am not currently sure why this happens. 
	//while (temp1 < 100000) { temp1++; newEntry(); }
	addList(filename);
    }

    private void addList(String filename) {
	// Read the contact list file specified when running the program, and 
	// adds each entry to the contact tree.
	try {
	    FileReader input = new FileReader(filename);
	    BufferedReader bRead = new BufferedReader(input);
	    String line[] = bRead.readLine().split("\t");
	    String lestLinje;
	    while(line != null) {
		lestLinje = bRead.readLine();
		if (lestLinje != null && line.length == 4){
		    BinTreNode nde = new BinTreNode(line[0], line[1], line[2], line[3], null, null);
		    insert(nde, root);
		    line = lestLinje.split("\t");
		} else {
		    line = null;
		}
	    }
	    bRead.close();

	} catch(IOException fnfe) {
	    System.out.println(fnfe.getMessage());
	}
    }

    private void printMenu() {
	System.out.print("\nWhat do you want to do?\n1 = Get the number of unique last names in the list.\n2 = Find the most common last name(s) in the list.\n3 = Search, by last name.\n4 = Print all the people who share both first and last names, but not country code.\n5 = Print the country code tree in preorder, printing each of the phone number trees in inorder.\n6 = Change an entry's last name\n7 = Print all last names in the tree, in lexical order.\n8 = Incite revolution in a city.\n0 = Print the menu again.\n9 = Quit\nNext command");
    }

    public void ordreLokke() {
	boolean i = true;
	Scanner sc = new Scanner(System.in);
	String kommando = null;
	int choice = 0;
	BinTreNode[] arr;
	printMenu();
	while (i) {
	    System.out.print(" : ");
	    kommando = sc.nextLine();
	    if (kommando == null) { 
		continue; 
	    } else if (kommando.equals("0")) { 
		printMenu();
	    } else if (kommando.equals("1")) {
		uniqueNames(root); 
		System.out.println("\nThe number of unique names in the list is: "+numUniLast);
		numUniLast = 0; 
	    } else if (kommando.equals("2")) {
		System.out.println("\nThe people with the most common last name(s) are:\n"); 
		mostCommonName();
		temp1 = 0; temp2 = 0; clearArr(flest1); clearArr(flest2);
	    } else if (kommando.equals("3")) {
		System.out.print("Type any string to search, by last name: ");
		kommando = sc.nextLine();
		peopleWithLast(root, kommando);
	    } else if (kommando.equals("5")){
		ccTre = createCCTree();
		ccTre.printIt(ccTre.root);
	    } else if (kommando.equals("4")){
		nonUniqueNames(root);
		printArr(nonUniques, false);
		clearArr(nonUniques);
	    } else if (kommando.equals("6")) {
		System.out.println("Type first and last name, seperated by space: ");
		kommando = sc.nextLine();
		arr = findPerson(kommando);
		if (arr == null) {continue; }
		for (int j = 0; j < arr.length; j++) {
		    if (arr[j] != null) {
			System.out.print(j+": "); arr[j].printInfo(true);
		    }
		}
		if (numArrItems(arr) == 0) {System.out.println("Not Found!"); continue; }
		if (numArrItems(arr) > 1) { System.out.print("State which person got a name change (type the number to the left of the entry): ");
		    choice = Integer.parseInt(sc.nextLine());
		    if (choice > arr.length) { continue; }
		}
		System.out.print("Type new last name: ");
		kommando = sc.nextLine(); 
		if (kommando.length() > 0) {
		    removeItem(null, root, arr[choice], false); 
		    arr[choice].last = kommando;
		    arr[choice].left = null; arr[choice].right = null;
		    insert(arr[choice], root);
		}
	    } else if (kommando.equals("7")) {
		printIt(root);
	    } else if (kommando.equals("8")) {
		System.out.print("Independence  day! type the country code of the country losing a city: ");
		kommando = sc.nextLine();
		System.out.print("Type the cipher that all the phone numbers of the city begin with: ");
		independenceDay(kommando, sc.nextLine());
		System.out.println("Viva la Revolución!");
	    } else if (kommando.equals("9")) {
		i = false;
	    }
	}
        sc.close();
    }
    
    public void insert(BinTreNode ny, BinTreNode place){
	// adds a node to the phone book tree.
	if (root == null){
	    root = ny;
	} else {
	    if (ny.last.compareTo(place.last) < 0) {
		if (place.left == null){
		    place.left = ny;
		} else {
		    insert(ny, place.left);
		}	    
	    } else if (ny.last.compareTo(place.last) > 0){
		if (place.right == null) {
		    place.right = ny;
		} else {
		    insert(ny, place.right);
		}
	    } else {
		ny.left = place.left;
		place.left = ny;
	    }
	}
    }

    public void uniqueNames(BinTreNode b){
	// Finds the number of unique last names in the list.
	BinTreNode n;
	BinIterator it = new BinIterator(b);
	while (it.hasNext()) {
	    numUniLast++;
	    n = it.next();
	    if (it.look() != null && it.look().last.equals(n.last)){ numUniLast--; }
	}
    }

    public void newEntry() {
	// Method for putting a randomly generated entry into the contact tree.
	 Random r = new Random();
	 String token = Long.toString(Math.abs(r.nextLong()), 36);
	 String tmp = "";
	 tmp += token.charAt(3); tmp +=  token.charAt(2); 
	 String first = tmp + token.charAt(4);
	 BinTreNode b = new BinTreNode(tmp,first,Long.toString(Math.abs(r.nextLong()%99)), tmp ,null,null);
	 insert(b,root);
    }

    public void mostCommonName() {
	// finds the most common name in the phone book, and lists the people sharing that
	// name. if more than one name is the most common, all the most common ones are listed.
	// Note also that all the methods manipulating arrays in this program exist to help this method. 
	// The reason i chose an array-implementation is that i had some trouble with the ArrayList-class.

	BinIterator it = new BinIterator(root);
	BinTreNode b = null;
	temp1 = 0; temp2 = 0;
	while (it.hasNext()) {
	    b = it.next();
	    temp1++;
	    flest1 = addToArr(flest1, b);
	    if (it.look() == null || !it.look().last.equals(b.last)) {
		if (temp1 > temp2) {
		    mostCommon = b.last;
		    temp2 = temp1;
		    flest2 = arrCpy(flest1);
		} else if (temp1 == temp2) {
		    mostCommon += ", "+b.last;
		    for (int i = 0; i < flest1.length && flest1[i] != null; i++) {
			flest2 = addToArr(flest2, flest1[i]);
		    }
		}
		temp1 = 0; clearArr(flest1);
	    }
	}

	for (int i = 0; i < flest2.length && flest2[i] != null; i++) {
	    flest2[i].printInfo(true);
	}
    }

    public void clearArr(BinTreNode[] arr) {
	// Replaces every node in the array with a null-pointer.
	for (int i = 0; i < arr.length; i++) {
	    arr[i] = null;
	}
    }

    public void nonUniqueNames(BinTreNode b) {
	// Finds all entries who share both first and last name, and prints them.
	if (b.left != null){
	    findMatch(b.left, b);
	    nonUniqueNames(b.left);
	}
	if (b.right != null){
	    nonUniqueNames(b.right);
	}
    }

    public BinTreNode[] addToArr(BinTreNode[] arr, BinTreNode b) {
	// Doubles the array size if you try to add more elements when the array is full. 
	// Returns a pointer to the array.
	if (arr[arr.length-1] != null){
	    BinTreNode[] newArr = new BinTreNode[arr.length*2];
	    for (int i = 0; i < arr.length; i++) {
		newArr[i] = arr[i];
	    }
	    arr = newArr;
	}
	if (!arrContains(arr, b)) {
	    for (int i = 0; i < arr.length; i++) {
		if (null == arr[i]) {
		    arr[i] = b;
		    return arr;
		}
	    }
	} return arr;
    }

    public BinTreNode[] arrCpy(BinTreNode[] keep) {
	// Returns a copy of the array passed as an argument.
	BinTreNode[] del = new BinTreNode[keep.length];
	for (int i = 0; i < keep.length && i < del.length; i++){
	    del[i] = keep[i];
	}
	return del;
    }

    public void printArr(BinTreNode[] arr, boolean nr) {
	// prints the elements != null of the array arr.
	for (int i = 0; i < arr.length; i++) {
	    if (arr[i] != null && nr) {arr[i].printInfo(true);}
	    else if (arr[i] != null) { arr[i].printInfo(false); }
	}
    }

    public void peopleWithLast(BinTreNode b, String ln) {
	// lists every person in the phone book with last name starting with 
	// the searh term ln.
	if (ln.length() <= b.last.length()){
	    for (int i = 0; i < ln.length() && i < b.last.length(); i++) {
		if (ln.charAt(i) != b.last.charAt(i)){
		    break;
		} else if (i == ln.length()-1) {
		    b.printInfo(true);
		}
	    }
	}
	if (b.left != null){
	    peopleWithLast(b.left, ln);
	}
	if (b.right != null){
	    peopleWithLast(b.right, ln);
	}
    }
    
    private CCTre createCCTree() {
	// Builds a country code tree from the given iterator.
	BinIterator it = new BinIterator(root);
	CCTre ccTre = new CCTre(it);
	return ccTre;
    }

    public void peopleWithLast(BinTreNode b, String ln, BinTreNode[] arr) {
	if (b.last.equals(ln)) {
	    arr = addToArr(arr, b);
	    if (b.left == null || !b.left.last.equals(ln)) {
		flest1 = arr;
	    }
	}
	if (b.left != null && ln.compareTo(b.left.last) <= 0){
	    peopleWithLast(b.left, ln, arr);
	}
	if (b.right != null && ln.compareTo(b.right.last) > 0){
	    peopleWithLast(b.right, ln, arr);
	}
    }

    public int numArrItems(BinTreNode[] arr) {
	// Returns the number of non-null items in the given array.
	for (int i = 0; i < arr.length; i++) {
	    if (arr[i] == null) { return i; }
	} return 0;
    }

    public boolean arrContains(BinTreNode[] arr, BinTreNode b) {
	// Returns true if the given node is found in the array.
	for (int i = 0; i < arr.length; i++) {
	    if (arr[i] == b) { return true; } 
	} return false;
    }

    public boolean arrContainsName(BinTreNode[] arr, BinTreNode b) {
	// Returns true if the first and last name of the given node is found in the given array.
	for (int i = 0; i < arr.length; i++) {
	    if (arr[i] != null && arr[i].last.equals(b.last) && arr[i].first.equals(b.first) && arr[i].cc.equals(b.cc)) { 
		return true; 
	    } 
	} return false;
    }

    public void printIt(BinTreNode where) {
	// Method to print the whole contact tree.
	BinIterator it = new BinIterator(where);
	BinTreNode b;
	int f = 0;
	root.printInfo(true);
	while (it.hasNext() && f < 100000) {
	    b = it.next();
	    f++;
	    System.out.print(b.last+" | ");
	    //b.printInfo(true);
	}
    }

    public void findMatch(BinTreNode place, BinTreNode b) {
	// Method to find any and all entries sharing first and last name, but not
	// country code, with the given node.
	if (b.last.compareTo(place.last) == 0 && b.first.compareTo(place.first) == 0 
	    && b.cc.compareTo(place.cc) != 0) {
	    if (!arrContainsName(nonUniques, b)) {
		nonUniques = addToArr(nonUniques, b);
	    }
	    if (!arrContainsName(nonUniques, place)) {
		nonUniques = addToArr(nonUniques, place); 
	    }
	    return;
	}	
	if (place.left != null){
	    findMatch(place.left, b);
	} 
	if (place.right != null) {
	    findMatch(place.right, b);
	}
    }

    public BinTreNode[] findPerson(String name) {
	// Returns an array of everyone in the contact tree with full name name.
	String[] full = name.split(" ");
	if (full.length != 2) { System.out.println("Invalid name!"); return null;}
	String first = full[0]; String last = full[1];
	BinIterator it = new BinIterator(root);
	BinTreNode b;
	BinTreNode[] arr = new BinTreNode[5];
	while (it.hasNext()) {
	    b = it.next();
	    if (b.last.equals(last) && b.first.equals(first)) {
		arr = addToArr(arr, b);
	    }
	}
	return arr;
    }

    public void removeItem(BinTreNode par, BinTreNode n, BinTreNode del, boolean isLeftChild) { 
	// Removes a node from the contact tree.
	if (n.left != null && del.last.compareTo(n.last) <= 0) {
	    if (n.left != null && del.last.compareTo(n.last) < 0) {
	    removeItem(n, n.left, del, true);
	    } else if (n.last.equals(del.last)) {
		if (n != del) {
	    	    removeItem(n, n.left, del, true);
		}
	    }
	} 
	if (n.right != null && del.last.compareTo(n.last) > 0) {
	    removeItem(n, n.right, del, false);
	}
	if (n == del) {
	    if (n.left == null) {
		if (isLeftChild) { par.left = n.right; }
		else { par.right = n.right; }
	    } else if (n.right == null) {
		if (isLeftChild) { par.left = n.left; }
		else { par.right = n.left; }
	    } else {
		if (isLeftChild) { 
		    par.left = n.left; 
		} else if (par != null) { 
		    par.right = n.left; 
		} else {
		    root = n.left;
		}
		insert(n.right, root);
	    }
	}
    }

    public void independenceDay(String cc, String city) {
	// Method to free the people a city from oppression by their evil overlords.
	BinIterator it = new BinIterator(root);
	BinTreNode b;
	while (it.hasNext()) {
	    b = it.next();
	    if (b.cc.equals(cc) && b.nr.charAt(0) == city.charAt(0)) {
		b.cc += city.charAt(0);
	    }
	}
	it = new BinIterator(root);
	ccTre = new CCTre(it);
    }

    private class BinIterator {
	// An iterator for instances of the EtternavnTre class.
	Stk s;
	BinIterator(BinTreNode b) {
	    this.s = new Stk();
	    slideL(b, s);
	}

	public boolean hasNext() {
	    if (s.isEmpty()) {
		return false;
	    }
	    return true;
	}

	public BinTreNode next() {
	    BinTreNode b = s.pop();
	    if (b.right != null) {
		slideL(b.right, s);
	    }
	    return b;
	}

	private void slideL(BinTreNode b, Stk s) {
	    while (b != null) {
		ListNode n = new ListNode(b);
		s.push(n);
		b = b.left;
	    } 
	}

	public BinTreNode look() {
	    return s.peek();
	}
    }

    private class CCTre {
	// A class of trees modeled after the requirements in the assignement text.
	CTreNode root = null;

	CCTre(BinIterator it) {
	    addTree(it);
	}

	private void addTree(BinIterator it) {
	    while (it.hasNext()) {
		BinTreNode b = it.next();
		insert(b, root);
	    }
	}

	public void insert(BinTreNode b, CTreNode where) {
	    // Inserts a node b into the CCTre instance.
	    if (root == null) {
		NrTree t = new NrTree(b.cc, b);
		root = new CTreNode(b.cc, t, null, null);
	    } else if (Integer.parseInt(b.cc) == Integer.parseInt(where.cc)){
		where.t.nrInsert(b, where.t.root);
	    } else if(Integer.parseInt(b.cc) < Integer.parseInt(where.cc)) {
		if (where.left == null) {
		    NrTree t = new NrTree(b.cc, b);
		    where.left = new CTreNode(b.cc, t, null, null);
		} else {
		    insert(b, where.left);
		}
	    } else {
		if (where.right == null) {
		    NrTree t = new NrTree(b.cc, b);
		    where.right = new CTreNode(b.cc, t, null, null);
		} else {
		    insert(b, where.right);
		}
	    }
	}

    
	public void printIt(CTreNode c) {
	    // Prints a CCTre instance.
	    c.printInfo();
	    if (c.left != null) { printIt(c.left); }
	    if (c.right != null) { printIt(c.right); }
	}

	public class NrTree {
	    // A class of trees that contains nodes, sorted by nr.
	    NTreNode root = null;
	    String cc = null;
	    NrTree (String cc, BinTreNode b) {
		this.root = new NTreNode(b.nr, b, null, null);
		this.cc = cc;
	    }

	    public void nrInsert(BinTreNode b, NTreNode where) {
		// Another insert method (I should have made a generic binary tree class..)
		if (b.nr.compareTo(where.nr) <= 0) {
		    if (where.left != null) {
			nrInsert(b, where.left);
		    } else {
			where.left = new NTreNode(b.nr, b, null, null);
		    }
		} else {
		    if (where.right != null) {
			nrInsert(b, where.right);
		    } else {
			where.right = new NTreNode(b.nr, b, null, null);
		    }
		}
	    }

	    public void printIt(NTreNode n) {
		// Recursively prints the NrTree.
		if (n.left != null) { printIt(n.left); }
		n.printInfo();
		if (n.right != null) { printIt(n.right); }
    	    }

	    private class NTreNode {
		// I made the NrTrees contain nodes of this kind, instead of BinTreNoder, to avoid
		// duplication of data.
		BinTreNode item;
		String nr;
		NTreNode left, right;
		NTreNode(String nr, BinTreNode item, NTreNode left, NTreNode right) {
		    this.right = null;
		    this.left = null;
		    this.nr = nr;
		    this.item = item;
		}

		public void printInfo() {
		    System.out.print(nr+" ");
		}
	    }
	}

	private class CTreNode {
	    // Nodes in the country code tree look like this.
	    String cc;
	    CTreNode left, right;
	    NrTree t;
	    CTreNode(String cc, NrTree t, CTreNode left, CTreNode right) {
		this.cc = cc;
		this.t = t;
		this.left = left;
		this.right = right;
	    }
	    public void printInfo() {
		System.out.println("\n"+cc+": ");
		t.printIt(t.root);
	    }
	}
    }
}
class BinTreNode {
    // the nodes in the contact tree look like this.
    String last, first, cc, nr;
    BinTreNode left, right;
    BinTreNode(String last, String first, String cc, String nr, BinTreNode left, BinTreNode right){
	this.last = last;
	this.first = first;
	this.cc = cc;
	this.nr = nr;
	this.left = left;
	this.right = right;
    }
    public void printInfo(boolean i) {
	// prints the info contained in a contact tree. 
	if (i) {
	    System.out.println(last+", "+first+"\t"+cc+"\t"+nr);
	} else {
	    System.out.println(first+"\t"+last+"\t"+cc);
	}
    }
}


class Stk {
    // A stack for use with the classes in this program.
    ListNode top;
    Stk() {
	this.top = null;
    }

    public void push(ListNode n) {
	n.next = top;
	top = n;
    }
    
    public BinTreNode pop() {
	if (top == null) {
	    return null;
	}
	BinTreNode b = top.item;
	top = top.next;
	return b;
    }

    public boolean isEmpty() {
	if (top == null) {
	    return true;
	} return false;
    }

    public BinTreNode peek() {
	if (top != null) {
	    return top.item;
	} else { return null; }
    }
}

class ListNode {
    // I implemented the stack using listnodes that looks like this.
    BinTreNode item;
    ListNode next;
    ListNode(BinTreNode b) {
	this.item = b;
	this.next = null;
    }
}
