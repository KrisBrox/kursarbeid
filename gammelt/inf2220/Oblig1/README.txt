README 
for Oblig 1, inf2220 (Oblig12220.java)

- Compile with "javac Oblig12220.java"
- Run with "java Oblig12220 contact.txt".

- Menu will display, explaining how to choose between the possible actions on the contact list.

- All the requirements for the assignement should be in order, when the default (given) contacts.txt is being used.

- In the constructor for the EtternavnTre class there is a line, currently commented out, that allows you to add any number of random entries to the list. Doing this will, for a reason to me unknown, sometimes result in the program crashing when running certain commands. 

- There is little checking of user input, so a command that is not recognised (e.g. separating the names in the name change procedure with a comma instead of just a space) will likely result in the program crashing. 

- I'd like to comment on you changeing the contact file without stating that you had done so. I find that pretty strange.
