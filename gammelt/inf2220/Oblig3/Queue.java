import java.util.Comparator;

/**
* Binary heap implemented with an array, for use with the {@link Graph}-class.
* Nodes are sorted by number of predecessors.
*/
public class Queue<E> {
    private Object[] items; 
    private int lastIndex;
    private MyComparator c;

    /**
    * Constructor, builds a heap with the argument array.
    * @param arr A vertex array.
    */
    Queue (Object[] arr, MyComparator c) {
	this.items = arr;
	this.c = c;
	lastIndex = items.length -1;
	minHeapify();
    }

    /**
    * Constructor, makes an empty heap of the specified size.
    * @param initialSize The number of nodes this heap can contain.
    */
    Queue (int initialSize, MyComparator c) {
	items = new Object[initialSize];
	this.c = c;
	lastIndex = -1;
    }

    /**
    * Method for determining if the heap is empty.
    */
    public boolean isEmpty() {
	if (lastIndex == -1) 
	    return true;
	return false;
    }

    /** 
    * Insert a Node into the Heap. Fails if the heap is full.
    * @param v The node to be inserted.
    * @return success ? true : false
    */
    public boolean insert(Object v) {
	if (lastIndex == items.length - 1)
	    return false;
	else {
	    lastIndex++;
	    items[lastIndex] = v;
	    percUp(lastIndex);
	    return true;
	}
    }

    /**
    * Sort the heap by number of predecessors.
    */
    private void minHeapify() {
	for (int i = lastIndex; i > 0; i--) {
	    percUp(i);
	}
    }

    /**
    * Delete the minimum node of the heap (root) and return it.
    * @return The previous root.
    */
    @SuppressWarnings("unchecked")	
    public E deleteMin() {
	Object tmp = items[0];
	items[0] = items[lastIndex];
	lastIndex--;
	percDown(0); 
	return (E) tmp;	
    }
    
    /**
    * Find the minimum node (=root) of the heap.
    * @return Root.
    */
    @SuppressWarnings("unchecked")		
    public E findMin() {
	return (E) items[0];
    }

    /**
    * Not in use.
    */
    public void changeKey(Object v, int k) {
	for (int i = 0; i <= lastIndex; i++) {
	    if (items[i] == v) {
		percUp(i);
		return;
	    }
	}
    }

    /**
    * Move the node at position i to its proper position.
    * @param i The index of the node to be moved.
    */
    private void percDown(int i) {
	Object tmp; boolean least;
	while (true){	
	    if ( (i+1)*2 - 1 > lastIndex) { 
		return;
	    }
	    if (((i+1)*2 < lastIndex) && 
		    c.compare(items[(i+1)*2-1], items[(i+1)*2]) > 0 ) { 
		least = false;} else least = true; 
    	    if (least && c.compare(items[i], items[(i+1)*2-1]) >= 0) {  
		tmp = items[(i+1)*2-1];
		items[(1+i)*2-1] = items[i];
		items[i] = tmp;
		i = (i+1)*2-1;
	    }  else if (!least && c.compare(items[i], items[(i+1)*2]) >= 0) {
		tmp = items[(i+1)*2]; 
		items[(i+1)*2] = items[i]; 
		items[i] = tmp;
		i = (i+1)*2;
	    } else {
		return;
	    }
	}
    }

    /**
    * Move the node at position i to its proper place.
    * @param i the position of the node to be moved.
    */
    private void percUp(int i) {
	Object tmp;
	while (true){
	    if (i == 0)  
		return;
	    if (c.compare(items[i], items[((i+1)/2)-1]) <= 0) {
		tmp = items[(i+1)/2-1];
		items[(i+1)/2 -1] = items[i];
		items[i] = tmp; i = (i+1)/2-1;
	    } else {
		return; 
	    }
	}
    }
    
    /**
    * Print the queue in order from least to most predecessors (empties the queue).
    */ 
    public void print() {
	for (int i = 0; i < items.length; i++) {
	    Object o = deleteMin();
	    System.out.print("\n"+c.getKey(o)+o);
	}
    }
}
