import java.util.LinkedList;

public final class Machine extends Vertex {
    private int capacity, numData, oldest = 0, newest = -1;
    private String[] oData;
    private LinkedList<String> lData = new LinkedList<String>();
    
    int time = -1, cost = -1;
    boolean visited = false;
    Machine prev = null;

    Machine(String info) {
	String[] tmp = info.split("\\s+");
	this.id = tmp[0];
	this.capacity = Integer.parseInt(tmp[1]);
	oData = new String[tmp.length-2];
	for (int i = 2; i<tmp.length; i++) {
	    oData[i-2] = tmp[i];
	    numData++;
	} 
    }

    public void addData(String s) {
	if (lData.contains(s)) return;
	if (numData == capacity && lData.isEmpty()) { 
	    return;
	}
	if (numData == capacity) {
	    lData.removeFirst();
	    lData.add(s);
	    return;
	}
	lData.add(s);
	numData++;
    }
    
    public boolean hasOriginalData(String s) {
	for (int i = 0; i < oData.length; i++) {
	    if (s.equals(oData[i])) {
		return true;
	    }
	} 
	return false;		
    }

    public boolean hasData(String s) {
	for (int i = 0; i < oData.length; i++) {
	    if (s.equals(oData[i])) { return true; }
	} 
	if (lData.contains(s)) { return true; }
	return false;
    }

    public String toString() {
	String ans ="\n"+ this.id+":"+this.capacity+" \nOriginal data:";
	for (int i = 0; i < oData.length; i++) {
	    ans += oData[i]+" ";
	}
	ans += "\nLocal data:";
	for (String s : lData) {
	    ans += s+" ";
	}
	return ans; 
    }
    
    public int numEdges() {
	return this.edges.size();
    }
}
