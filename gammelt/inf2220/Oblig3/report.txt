rapport for oblig3 inf2220, av Kristian Brox


Jeg valgte en grafbasert l�sning av oblig nr. 3. Hver maskin i grafen har en liste av kantobjekter. hver kant har to vekter, tid og kostnad. S�king i grafen gj�res med en implementasjon av Dijkstra's algoritme, tatt fra wikipedia. 

Kompleksitet:

Initialiseringen av programmt (innlasting av data fra fil) skjer i to steg, addMachines og addConnections. 
    addMachines er O(n), siden den kun leser hver linje i data.txt �n gang og gj�r konstant arbeid per linje.
    addConnections er O(m), siden den gj�r konstant arbeid per linje i connections.txt.
    Implementasjonen av Dijkstra's algoritme i l�sningen min har en litt verre kompleksitetsanalyse. I et fors�k p� � begrense kompleksiteten ved � unng� line�re s�k for � finne den ubes�kte noden med lavest totalkost valgte jeg � skrive en k� som alle de ubes�kte nodene plasseres i. Trolig skj�t jeg meg selv i foten ved � gjre dette. changeKey er nemlig implementert ved et line�rt s�k gjennom heapen.
    Jeg tror av denne grunn at dijkstr-algoritmen min blir O(log|E|*|V|^2). Dersom jeg hadde latt hver maskin vite sin egen indeks i k�en, ville dette blir reduserttil O(log|E|*|V|). Jeg har her antatt at log|E| er et rimelig estimat for gjennomsnittlig antall kanter per node, en antagelse jeg ikke er i stand til � forsvare. 
    Totalt blir da s�kealgoritmen O(n*log|E|*|V|), hvor n er antall elementer det s�kes etter.

I etterp�klokskapens lys ser det ut til at jeg kunne spart b�de kj�retid og arbeid p� � lage en mindre generell og mer naiv l�sning p� oppgaven. Brorparten av tiden jeg brukte p� oppgaven gikk med til � lage den comparator-baserte k�en.
