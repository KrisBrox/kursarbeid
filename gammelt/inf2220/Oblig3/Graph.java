import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Collection;

/**
* A rather unfinished class for graphs, NB: v2.0.
*/
public class Graph<E extends Vertex> {
    private HashMap<String, E> vertexMap = new HashMap<String, E>();
    private ArrayList<E> vertexList = new ArrayList<E>();
    Graph() { }

    /**
    * Adds a vertex to the graph, with the specified key.
    * @param e The vertex to add.
    * @param key The key of the vertex.
    * @return True if the everything is working properly. If false the graph
    * is corrupted, i.e the vertex will both be and not be contained in 
    * graph.
    */	    
    public boolean addVertex(E e, String key){
	boolean success;
	vertexMap.put(key, e);
	success = vertexList.add(e);
	return success;
    } 

    /**
    * Adds an edge from Vertex with id=key to Vertex with id=id.
    * @param key The id of the Vertex the edge is from.
    * @param id The id of the Vertex the edge is to.
    * @return the value previously associated with {@link key}.
    */
    public void addEdge(String key, String id) {
	Edge tmp = new Edge(key, id);
	vertexMap.get(key).edges.add(tmp);
    } 

    /**
    * Adds a bidirected edge between Vertices with id's idOne and idTwo.
    * @param idOne The id of the first vertex.
    * @param idTwo The id of the second vertex.
    */
    public void addBiEdge(String idOne, String idTwo) {
	vertexMap.get(idOne).edges.add(new Edge(idOne, idTwo)); 
	vertexMap.get(idTwo).edges.add(new Edge(idTwo, idOne));
    }

    /**
    * Adds a weighted bidirected edge between Vertices with id's idOne and idTwo.
    * @param idOne The id of the first vertex.
    * @param idTwo The id of the second vertex.
    * @param weight The weight of the edge.
    */
    public void addBiEdge(String idOne, String idTwo, String weight) {
	vertexMap.get(idOne).edges.add(new Edge(idOne, idTwo, weight)); 
	vertexMap.get(idTwo).edges.add(new Edge(idTwo, idOne, weight));
    }

    /**
    * Adds a double-weighted bidirected edge between Vertices with id's 
    * idOne and idTwo.
    * @param idOne The id of the first vertex.
    * @param idTwo The id of the second vertex.
    * @param weight The first weight parameter of the edge.
    * @param weight2 The second weight parameter of the edge.
    */
    public void addBiEdge(String idOne, String idTwo, String weight, String weight2) {
	vertexMap.get(idOne).edges.add(new Edge(idOne, idTwo, weight, weight2)); 
	vertexMap.get(idTwo).edges.add(new Edge(idTwo, idOne, weight, weight2));
    }
  
    /**
    * Removes all values from the graph.
    */ 
    public void clear() {
	vertexMap.clear();
	vertexList.clear();
    }
    
    /*
    public boolean retainAll(Collection c) { return false; }
    public boolean removeAll(Collection c) { return false; }
    public boolean addAll(Collection c) { return false; }
    public boolean remove(Object o) { return false; } 
    */

    /**
    * Adds a vertex to the graph, using the id of the vertex as its key.
    * @param o An instance of a class that extends vertex
    * @return Same as for {@link addVertex}
    */
    public boolean add(Object o) {
	@SuppressWarnings("unchecked")
	E tmp = (E) o; 
	return addVertex(tmp, tmp.id);
    }
    
    /**
    * Determine if the Graph contains all of the items of a collection.
    * @param c Any Collection.
    * @return True if the graph contains all of the items of the collection c.
    */
    public boolean containsAll(Collection c) {
	return vertexList.containsAll(c);
    }

    
    public boolean isEmpty() {
	return vertexMap.isEmpty();
    }

    public Iterator iterator() {
	return vertexList.iterator();
    }

    public int size() {
	return vertexMap.size();
    }

    public Object[] toArray() {
	return vertexList.toArray();
    }

    public Object[] toArray(Object[] a) {
	return vertexList.toArray(a); 
    }

    /**
    * Print the vertices of the graph.
    */
    public void print() {
	for (E e :vertexMap.values()) {
	    System.out.println(e);
	}
    }
    
    /**
    * Check if an object is contained in the graph.
    * @param o An instance of {@link Vertex} or subclass.
    */
    public boolean contains(Object o) {
	@SuppressWarnings("unchecked")
	E tmp = (E) o;
	return vertexMap.containsValue(tmp); 
    }

    /**
    * Get the vertex specified by key.
    * @param key The key with which the vertex was added to the graph.
    * @return The vertex associated with {@link key}, or null if key is
    * not used.
    */
    public E get(String key) {
	return vertexMap.get(key);
    }

    /**
    * Check if a vertex with id=key is contained in the graph.
    */
    public boolean containsKey(String key) {
	return vertexMap.containsKey(key);
    }
    

    /*
    /**
    * Checks the Graph for loops, printing all vertices affected by the 
    * loop(s) if one or more is found. 
    *<p>
    * NB: relative to the Obligatory assignement this method is a part of,
    * this method prints out all tasks that as a consequence of the loop
    * cannot be started.
    * @return True if at least one loop is found in the graph.
     
    public boolean findLoop() {
	int teller = vertexList.size();
	ArrayList<Vertex> loop = new ArrayList<Vertex>(vertexList);
	Queue q = new Queue(teller);
	for (int i = 0; i < vertexList.size(); i++) {
	    if (vertexList.get(i).cntPred == 0) {
		q.insert(vertexList.get(i));
	    }
	}
	while (!q.isEmpty()) {
	    Vertex tmp = q.deleteMin();
	    loop.remove(tmp);
	    teller--;
	    for (Vertex v : tmp.outEdges) {
		if (--v.cntPred == 0) {
		q.insert(v);
		}
	    }
	}
	for (Vertex v : vertexList) {
	    v.cntPred = v.pred.size();
	}
	if (teller > 0) { 
	    for (int i = 0; i<loop.size(); i++) {
	        System.out.print(loop.get(i));
	    }
	    return true;
	}
	return false;
    }
    */
    
    /*
    /**
    * Copy the vertexList and sort it according to the ordering 
    * specified by the given Comparator.
    * @return The sorted copy.
    *//*
    public ArrayList<Vertex> topologicalSort() {
	Queue q = new Queue( vertexList.toArray());
	ArrayList<Vertex> ans = new ArrayList<Vertex>();
	while (!q.isEmpty()) {
	    ans.add(q.deleteMin());
	}
	return ans;
    }*/
}
