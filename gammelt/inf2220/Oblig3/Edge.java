public class Edge {
    private String id1, id2;
    private int weight = 0, weight2 = 0;
    
    Edge(String id1, String id2) {
	this.id1 = id1;
	this.id2 = id2;
    }

    Edge(String id1, String id2, String weight) {
	this.id1 = id1;
	this.id2 = id2;
	this.weight = Integer.parseInt(weight);
    }

    Edge(String id1, String id2, String weight, String weight2) {
	this.id1 = id1;
	this.id2 = id2;
	this.weight = Integer.parseInt(weight);
	this.weight2 = Integer.parseInt(weight2);
    }


    public String get1() {
	return id1;
    }

    public String get2() {
	return id2;
    }

    public int getCost() {
	return weight2;
    }

    public int getTime() {
	return weight;
    }

    public String toString() {
	return id1+":"+id2+":"+weight+":"+weight2+"\n";
    }

    public String getOther(String id) {
	if (id.equals(id1) || id.equals(id2)) {
	    return id.equals(id1) ? id2 : id1;
	} else {
	    return null;
	}
    }
}
