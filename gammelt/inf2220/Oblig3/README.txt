README for Oblig3 v1.0, inf2220, by Kristian Brox.

It seems to me that the program works like it is supposed to. That is: the source-code compiles, the program runs without crashing when given proper input, and 
the output seems correct i.e it changes when changing the search parameters. 

Searches must be formatted exactly like the example in the assignement text, e.g'1:C:O:57 15 296 83'

