import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Comparator;
import java.util.ArrayList;

public final class Network extends Graph<Machine>{
    String file1, file2;
    private int numMachines = 0;
    Network(String file1, String file2) {
	this.file1 = file1;
	this.file2 = file2;
	addMachines(file1);
	addConnections(file2);
	shell();
    }

    private void shell() {
	Scanner sc = new Scanner(System.in);
	String in;
	System.out.print("\nType a search string, a machine id to view that\nmachine's data, 'm' to see this menu again, or 'q' to exit.\n");
	while (true) {
	    System.out.print("\n>> ");
	    in = sc.nextLine();
	    if (in.equals("q")) {
		return;
	    } else if (in.length() <= 2) {
		System.out.print(get(in)+"\n");
	    } else if (in.length() > 2) {
		System.out.print(search(in)+"\n");
	    }
	}
    }

    private void addMachines(String fileName) {
	File file = new File(fileName);
	try {
	    Scanner sc = new Scanner(file);
	    while (sc.hasNextLine()) {
		numMachines++;
		Machine m = new Machine(sc.nextLine());
		this.addVertex(m, m.id);
	    }
	} catch (FileNotFoundException fnfe) {
	    fnfe.printStackTrace();
	}
    }
    
    private void addConnections(String fileName) {
	File file = new File(fileName);
	String[] info;
	try {
	    Scanner sc = new Scanner(file);
	    while (sc.hasNextLine()) {
		info = sc.nextLine().split("\\s+");
		this.addBiEdge(info[0], info[1], info[2], info[3]); 
	    }
	} catch (FileNotFoundException fnfe) {
	    fnfe.printStackTrace();
	}
    }
    
    public void printEdges() {
	Machine[] m =(Machine[]) this.toArray(new Machine[0]);
	for (int i = 0; i < m.length; i++) {
	    for (Edge e: m[i].edges) {
		System.out.print(e);
	    }
	}
    }

    public String search(String search) {
	String[] data = search.split(" ");
	String[] comm = data[0].split(":");
	if (comm.length != 4 || data.length == 0) {
	    return "Invalid search string, please try again:";
	} 
	data[0] = comm[comm.length-1];
	Machine requester = this.get(comm[0]);
	boolean orig = comm[2].equals("O") ? true : false;

	if (comm[1].equals("T")) {
	    return oneSearch(comm, data, requester, orig, new CompareTime());

	} else if (comm[1].equals("C")) {
	    return oneSearch(comm, data, requester, orig, new CompareCost());
	
	} else if (comm[1].equals("B")) {
	    return oneSearch(comm, data, requester, orig, new CompareBoth());
	}
	return "ERROR";
    }

    public String oneSearch(String[] comm, String[] data, 
		    Machine requester, boolean orig, MyComparator c) {
	String ans = "";
	int longestTime = 0, totalCost = 0;
	ArrayList<Result> res = new ArrayList<Result>();
	for (int i = 0; i < data.length; i++) {
	    res.add(dijkstra(requester, data[i], orig, c));
	    ans += res.get(i).toString();
	}
	for (Result r : res) {
	    if (longestTime < r.time)
		longestTime = r.time;
	    totalCost += r.cost;
	}
	return ans+"Total time: "+longestTime+", total cost: "+totalCost+"\n";
    }

    public Result dijkstra(Machine start, String data, 
		    boolean orig, MyComparator c) {
	ArrayList<Machine> ans = new ArrayList<Machine>();
	Iterator it = this.iterator();
	Queue<Machine> h = new Queue<Machine>(numMachines, c);
	Machine m = null, o, tmp;
	String out = "", param;
	int altTime, altCost, least = 10000;	

	while(it.hasNext()) {
	    m = (Machine) it.next();
	    m.time = 10000;
	    m.cost = 10000;
	    m.visited = false;
	    if (m == start) {
		m.time = 0;
		m.cost = 0;
	    }
	    m.prev = null;
	    h.insert(m);
	}
	
	/*
	Hovedløkke Dijkstra
	*/
	while (!h.isEmpty()) {
	    m = (Machine) h.deleteMin();
	    m.visited = true;
	    if (!orig && m.hasData(data)) {
		ans.add(m);
	    } else if (orig && m.hasOriginalData(data)) {
		ans.add(m);
	    }

	    for (Edge e: m.edges) {
		o = this.get(e.getOther(m.id));
		if (o.visited == false) {
		    altTime = m.time + e.getTime();
		    altCost = m.cost + e.getCost();
		    param = c.getParameter();

		    if (param.equals("time")) {
			if (altTime < o.time) {
			    o.time = altTime;
			    o.prev = m;
			    o.cost = altCost;
			    h.changeKey(o, altTime);
			}
		    } else if (param.equals("cost") 
			    || param.equals("cost:time")) {
			if (altCost < o.cost) {
			    o.cost = altCost;
			    o.prev = m;
			    o.time = altTime;
			    h.changeKey(o, altCost);
			}
		    }
		}
	    }
	}
	
	if (ans.size() == 0) 
	    return new Result(data+ ": The data does not exist!\n", 0,0);	

	for (Machine v : ans) {
	    if (c.getKey(v) < least) {
		least = c.getKey(v);
		m = v;
	    } 
	}

	if (c.getParameter().equals("cost:time")) {
	    for (Machine v:ans) {
		if (v.time < m.time && v.cost == m.cost) {
		    m = v;
		}
	    }
	}

	tmp = m;
	while (true) {
	    out = tmp.id +out;
	    tmp = tmp.prev;
	    if (tmp != null){ 
		out = "->"+out;
	    } else {
		break;
	    }
	}
	out = data+": "+out+" (t="+m.time+", c="+m.cost+")\n";
	start.addData(data);
	return new Result(out, m.cost, m.time);
    }

    class Result {
	String res = "";
	int cost;
	int time;

	Result(String res, int cost, int time) {
	    this.res = res;
	    this.time = time;
	    this.cost = cost;
	}
	
	public String toString() {
	    return res;
	}
    }

    class CompareTime implements MyComparator {	
	public int compare(Object o1, Object o2) {
	    Machine m1 = (Machine) o1;
	    Machine m2 = (Machine) o2;
	    if (m1.time < m2.time) { return -1; }
	    if (m1.time > m2.time) { return 1; }
	    return 0;
	}

	public int getKey(Object o) {
	    return ((Machine) o).time;
	}
    
	public int setKey(Object o, int i) {
	    int tmp = getKey(o);
	    ((Machine) o).time = i;
	    return tmp;
	}
	
	public String getParameter() {
	    return "time";
	}
    }

    class CompareCost implements MyComparator {
	public int compare(Object o1, Object o2) {
	    Machine m1 = (Machine) o1;
	    Machine m2 = (Machine) o2;
	    if (m1.cost < m2.cost) { return -1; }
	    if (m1.cost > m2.cost) { return 1; }
	    return 0;
	}

	public int getKey(Object o) {
	    return ((Machine) o).cost;
	}
    
	public int setKey(Object o, int i) {
	    int tmp = getKey(o);
	    ((Machine) o).cost = i;
	    return tmp;
	}
	
	public String getParameter() {
	    return "cost";
	}
    }

    class CompareBoth implements MyComparator {
	public int compare(Object o1, Object o2) {
	    Machine m1 = (Machine) o1;
	    Machine m2 = (Machine) o2;
	    if (m1.cost < m2.cost) { return -1; }
	    if (m1.cost > m2.cost) { return 1; }
	    return 0;
	}

	public int getKey(Object o) {
	    return ((Machine) o).cost;
	}
    
	public int setKey(Object o, int i) {
	    int tmp = getKey(o);
	    ((Machine) o).cost = i;
	    return tmp;
	}
	
	public String getParameter() {
	    return "cost:time";
	}
    }

}
