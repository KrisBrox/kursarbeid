import java.util.Comparator;

public interface MyComparator extends Comparator<Object> {
    public int getKey(Object o);
    public int setKey(Object o, int i);
    public String getParameter();
}
