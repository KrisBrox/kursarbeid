function [xopt,numit]=newtonbacktrack(f,df,d2f,x0)
  epsilon=10^(-3);
  xopt=x0;
  maxit=100;
  for numit=1:maxit
      d=-d2f(xopt)\df(xopt);
      eta=-df(xopt)'*d;
      if eta/2<epsilon
          break;
      end
      alpha=armijorule(f,df,xopt,d);
      xopt=xopt+alpha*d;
  end