function [xopt,numit]=newtonbacktrack_commented(f,df,d2f,x0)
  epsilon=10^(-3);  % we are satisfied with a point where f'(alfa) is 
                    % within 10^-3 of 0.
  xopt=x0;          % the starting point, our "guess".
  maxit=100;        % max 100 iterations, in case it doesn't converge    
  
  % loop calculating the approximation to the minimum
  for numit=1:maxit
      d=-d2f(xopt)\df(xopt);    % find dk
      eta=-df(xopt)'*d;
      if eta/2<epsilon          % if the approximation is close enough
          break;                % we are done
      end
      alpha=armijorule(f,df,xopt,d);    % choosing the step length 
                                        % according to armijo's rule
      xopt=xopt+alpha*d;
  end