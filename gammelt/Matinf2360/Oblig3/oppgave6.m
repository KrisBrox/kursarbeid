% Oppgave 6

a = 0.5; n = [10, 1000, 100000]; m = 1;

y=zeros(3, 1);

for k=1:3
    x = randmuon(a, m, n(k));
    f = @(a)(-sum(log((1+a*x)/2)));
    df = @(a)(-sum(x./(1+a*x)));
    d2f = @(a)(sum((x./(1+a*x)).^2));
    y(k) = newtonbacktrack(f, df, d2f, 0);
end

plot(y);

% ser at n=100000 gir best (mest p�litelig) resultat.