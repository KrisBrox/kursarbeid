function alpha=armijorule(f,df,x,d)
  beta=0.2; s=0.5; sigma=10^(-3);
  m=0;
  while (f(x)-f(x+beta^m*s*d) < -sigma *beta^m*s *(df(x))'*d)
      m=m+1;
  end
  alpha = beta^m*s;