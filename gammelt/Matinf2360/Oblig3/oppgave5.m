% Oppgave 5

% initialize input values:
a0 = 0;
x = [0.4992, -0.8661, 0.7916, 0.9107, 0.5357, 0.6574, 0.6353, 0.0342, 0.4988, -0.4607];

% Define the input functions:
f = @(a)(-sum(log((1+a*x)/2)));
df = @(a)(-sum(x./(1+a*x)));
d2f = @(a)(sum((x./(1+a*x)).^2));

% run the algorithm:
newtonbacktrack(f, df, d2f, a0)

% result: 0.7230
