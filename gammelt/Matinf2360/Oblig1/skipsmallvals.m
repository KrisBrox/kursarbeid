function newy=skipsmallvals(y,eps)
    num = 0;
    for n=1:length(y)
        if abs(y(n))<eps
            y(n) = 0+0i;
            num = num+1;
        end
    end
    ratio = num/length(y);
    disp(ratio);
    newy=y;
end