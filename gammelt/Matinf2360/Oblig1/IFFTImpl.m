function y = IFFTImpl(x)
N = length(x);
if N == 1 
    y = x(1);
else
    xe = x(1:2:(N-1));
    xo = x(2:2:N);
    ye = FFTImpl(xe);
    yo = FFTImpl(xo);
    D=exp(2*pi*1j*(0:N/2-1)'/N);
    y = [ ye + yo.*D; ye - yo.*D]/sqrt(2);
end

% Returnerer en vektor som er resultatet av � gj�re en invers diskret
% fourier transform p� inputvektor x. Funksjonen er identisk med FFTImpl
% bortsett fra fortegnet i D-matrisen, noe som gj�r at denne funksjonen
% blir den inverse av FFTImpl.