clear all;
close all;
tic;
[S, fs] = wavread('castanets.wav');
ans1 = zeros(14, 1);
for n = 4:14
    x = S(1:2^n, 1);
    start=toc; 
    y = FFTImpl(x);
    stop = toc;
    ans1(n) = stop - start;
end

ans2 = zeros(14, 1);
for n = 4:14
    x = S(1:2^n, 1);
    start = toc;
    y = DFTImpl(x);
    stop = toc;
    ans2(n) = stop - start;
end

plot(ans1)
hold on
plot(ans2,'r'); 

% Her leses wav-filen inn, f�r DFT og FFT beregnes for de f�rste 2^n
% samplene for n=4,5,...,14. Tiden dette tar for hver n lagres i to
% vektorer, en for DFT og en for FFT, og resultatene plottes mot hverandre
% (bl� = FFT, r�d = DFT).
