function y = FFTImpl(x)
N = length(x);
if N == 1 
    y = x(1);
else
    xe = x(1:2:(N-1));
    xo = x(2:2:N);
    ye = FFTImpl(xe);
    yo = FFTImpl(xo);
    D = exp(-2*pi*1j*(0:N/2-1)'/N);
    y = [ ye + yo.*D; ye - yo.*D]/sqrt(2);
end

% Algoritmen tar som argument en vektor x. 
% Denne vektoren deles s� i to, hvor den ene halvdelen er elementene med 
% partallsindeks og den andre er de med oddetallsindeks. FFT'en til hver av
% vektorene beregnes, og resultatvektoren y settes sammen av disse to
% i henhold til teorem 4.4. Algoritmen er rekursiv.

