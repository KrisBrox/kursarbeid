% Oblig 1 Mat-Inf2360

function y=DFTImpl(x)
  N=length(x);
  FN=zeros(N);
  for n=1:N
    FN(n,:)=exp(-2*pi*1i*(n-1)*(0:(N-1))/N)/sqrt(N);
  end
  y=FN*x;
end

% Algoritmen tar inn en vektor x, f.eks en wav-fil. s� opprettes
% en nullmatrise med dimensjon NxN, hvor N er lengden av inputvektor.
% deretter beregnes matrisen FN etter formelen 
% (FN)nk = (1/sqrt(n))exp(-2ink/N).
% returverdien er FN*x, alts� x uttrykt i fourierbasisen FN.

