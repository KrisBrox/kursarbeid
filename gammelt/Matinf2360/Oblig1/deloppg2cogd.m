function y = deloppg2cogd(Hz)
[S, fs] = wavread('castanets.wav');
x = S(1:2^17,1);
x = FFTImpl(x);
step = fs/length(x);
index1 = round(Hz/step);
index2 = length(x)/2 + (length(x)/2 - index1);
for n = index1:index2
    x(n) = 0;
end
y = real(IFFTImpl(x));
plot(y);
end

% Funksjonen finner hvilken indeks n i DFT'en til lydfilen som svarer til
% 10000 Hz, og setter alle verdier med indekser i N/2 � (N/2-n) til 0.
% (DFT-en er symmetrisk om N/2). returverdi er IFFT'en til den resulterende
% vektoren.