[S, fs] = wavread('castanets.wav');
x = S(1:2^17, 1);
x = FFTImpl(x);
plot(abs(x));

% Plotter DFT-verdiene til de f�rste 2^17 samplene fra lydfilen. Det ser ut
% til fra plottet at relativt lave frekvenser dominerer lydbildet.