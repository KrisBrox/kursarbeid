function ek = ek(k)
    ek = quad(@(t)t.^k.*(1-2*abs(t-0.5)),0,1);
end