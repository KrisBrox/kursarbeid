% deloppg1c
deloppg1aog1b
t = linspace(-2,3,1000);
psi00 = (t>=0).*(t<=1).*(1-2*abs(t-0.5));
phi00 = (t>=-1).*(t<=1).*(1-abs(t));
phi01 = (t>=0).*(t<=2).*(1-abs(t-1));
phi0minus1 = (t>=-2).*(t<=0).*(1-abs(t+1));
phi02 = (t>=1).*(t<=3).*(1-abs(t-2));

psihat = psi00 - z1*phi00 - z2*phi01 - z3*phi0minus1 - z4*phi02;
