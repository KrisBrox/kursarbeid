function bk = bk(k)
    bk = quad(@(t)t.^k.*(1-abs(t-1)),0,2);
end