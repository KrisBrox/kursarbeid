function playDWTfilterslowerdifference(m,h0,h1,g0,g1)
  [S fs]=wavread('castanets'); 
  newx=DWTImpl(h0,h1,S(1:2^17,1),m);
  len=length(newx);
  newx(1:(len/2^m))=zeros(len/2^m,1);
  newx=IDWTImpl(g0,g1,newx,m);
  playerobj=audioplayer(newx,fs);
  playblocking(playerobj);