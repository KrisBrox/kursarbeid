function [a0,a1]=changecolumnrows(g0,g1)
  % Keep track of the indices of the biggest even and odd index.
  maxeveng0=length(g0);
  maxoddg0=length(g0);
  if mod(maxeveng0,2)==0 
      maxeveng0=maxeveng0-1; 
  else
      maxoddg0=maxoddg0-1;
  end
  
  maxeveng1=length(g1);
  maxoddg1=length(g1);
  if mod(maxeveng1,2)==0 
      maxeveng1=maxeveng1-1; 
  else
      maxoddg1=maxoddg1-1;
  end;
  
  a0length=max(maxeveng0,maxoddg1);
  a1length=max(maxeveng1,maxoddg0);
  
  a0=zeros(1,a0length);
  a0(1:2:maxeveng0)=g0(1:2:maxeveng0);
  a0(2:2:maxoddg1)=g1(2:2:maxoddg1);
  
  a1=zeros(1,a1length);
  a1(1:2:maxeveng1)=g1(1:2:maxeveng1);
  a1(2:2:maxoddg0)=g0(2:2:maxoddg0);