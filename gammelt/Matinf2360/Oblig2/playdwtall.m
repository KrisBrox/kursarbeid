function playdwtall(h0, g0, h1, g1)

    for m = 1:4
        playDWTfilterslower(m, h0, h1, g0, g1);

    end

    for m = 1:4 
        playDWTfilterslowerdifference(m, h0, h1, g0, g1);
    end
end