function x=IDWTImpl(g0,g1,xnew,m)
  [a0,a1]=changecolumnrows(g0,g1);
  for mres=m:(-1):1
    len=length(xnew)/2^(mres-1);
    
    % Reorganize the coefficients first
    l=xnew(1:(len/2));
    h=xnew((len/2+1):len);
    xnew(1:2:(len-1))=l;
    xnew(2:2:len)=h;
    
    xnew(1:len)=rowsymmmratrans(a0,a1,xnew(1:len));
  end
  x=xnew;
