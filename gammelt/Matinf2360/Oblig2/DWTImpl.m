function xnew=DWTImpl(h0,h1,x,m)
  for mres=1:m
    len=length(x)/2^(mres-1);
    x(1:len)=rowsymmmratrans(h0,h1,x(1:len));

    % Reorganize the coefficients
    l=x(1:2:(len-1));
    h=x(2:2:len);
    x(1:len)=[l h];
  end
  xnew=x;