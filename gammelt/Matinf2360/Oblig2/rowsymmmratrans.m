function y=rowsymmmratrans(a0,a1,x)
  N=length(x);
  y=zeros(length(x),1);
  E0=length(a0)-1;
  E1=length(a1)-1;
  
  % Compute the even samples in y
  n=0;
  while n<E0
      y(n+1)= a0(1)*x(n+1);
      for k=1:n
          y(n+1) = y(n+1) + a0(k+1)*(x(n+k+1)+x(n-k+1));
      end
      for k=(n+1):E0 
          y(n+1) = y(n+1) + a0(k+1)*(x(n+k+1)+x(n-k+N+1));
      end
      n=n+2;
  end
  while n<(N-E0)
      y(n+1)= a0(1)*x(n+1);
      for k=1:E0
          y(n+1) = y(n+1)+ a0(k+1)*(x(n+k+1)+x(n-k+1));
      end
      n=n+2;
  end
  while n<N
    y(n+1) = a0(1)*x(n+1);
    for k=1:(N-1-n)
        y(n+1) = y(n+1) + a0(k+1)*(x(n+k+1)+x(n-k+1));
    end
    for k=(N-1-n+1):E0 
        y(n+1) = y(n+1) + a0(k+1)*(x(n+k-N+1)+x(n-k+1));
    end
    n=n+2;
  end
  
  % Compute the odd samples in y
  n=1;
  while n<E1
    y(n+1)= a1(1)*x(n+1);
    for k=1:n
        y(n+1) = y(n+1) + a1(k+1)*(x(n+k+1)+x(n-k+1));
    end
    for k=(n+1):E1 
        y(n+1) = y(n+1) + a1(k+1)*(x(n+k+1)+x(n-k+N+1));
    end
    n=n+2;
  end
  while n<(N-E1)
      y(n+1)= a1(1)*x(n+1);
      for k=1:E1
          y(n+1) = y(n+1)+ a1(k+1)*(x(n+k+1)+x(n-k+1));
      end
      n=n+2;
  end
  while n<N
    y(n+1) = a1(1)*x(n+1);
    for k=1:(N-1-n)
        y(n+1) = y(n+1) + a1(k+1)*(x(n+k+1)+x(n-k+1));
    end
    for k=(N-1-n+1):E1 
        y(n+1) = y(n+1) + a1(k+1)*(x(n+k-N+1)+x(n-k+1));
    end
    n=n+2;
  end