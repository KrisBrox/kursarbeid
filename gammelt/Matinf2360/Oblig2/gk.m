function gk = gk(k)
    gk = quad(@(t)t.^k.*(1-abs(t+1)),-2,0);
end