function dk = dk(k)
    dk = quad(@(t)t.^k.*(1-abs(t-2)),1,3);
end