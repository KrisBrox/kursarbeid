function ak = ak(k)
    ak = quad(@(t)t.^k.*(1-abs(t)),-1,1);
end