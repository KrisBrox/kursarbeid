#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>

void excercise(void) {
    
    

    int n = 10;
    int sizeR = 10, sizeQ = 10*sizeR, sizeP = 10*sizeQ;    
    char *p = (char*)malloc(sizeof(char)*n*n*n);
    char **q = (char**)malloc(sizeof(char*)*n*n);
    char ***r = (char***)malloc(sizeof(char**)*n);
    unsigned int i = 0, j, k;
    for (i = 0; i < n*n; ++i) {
	q[i] = &p[i*n];
    }
    for (i = 0; i < n; ++i) {
	r[i] = &q[i*n];
    }
    for (i = 0; i < n; ++i) {
	for (j = 0; j<n; ++j) {
	    for (k = 0; k<n; ++k) {
		srand(time());
		r[i][j][k] =(char) rand();
    }}}
    printf("%c\n",r[2][0][4]);    

    free(p);
}

void swap(int *a, int *b) {
    int t=*a; *a=*b; *b=t;
}

void sort(int arr[], int beg, int end) {
    if (end > beg + 1) {
	int piv = arr[beg], l = beg + 1, r = end;
	while (l < r) {
	    if (arr[l] <= piv)
		l++;
	    else
		swap(&arr[l], &arr[--r]);
	 }
	 swap(&arr[--l], &arr[beg]);
    sort(arr, beg, l);
    sort(arr, r, end);
    }
}



int main (int argc, char *argv[]) {
    excercise();
}

