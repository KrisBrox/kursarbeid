#include <stdio.h>
#include <mpi.h>
	
int main(int argc, char **argv) {
	int size, myRank;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	printf("procrank=%d, procnum=%d\n", myRank, size);
    MPI_Finalize();
	return 0;
}
