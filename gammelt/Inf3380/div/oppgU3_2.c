#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>

int excercise(int k) {
    int size, min = INT_MAX, max = 0;
    float avrg = 0;
    /*printf("size of array: ");
    scanf("%d", &size);
    */
    size = k;
    int i = 0;
    int *p = (int*)malloc(sizeof(int)*size);
    for (i = 0; i < size; ++i) {
	p[i] = rand();
	min = (min > p[i]) ? p[i] : min;
	max = (max < p[i]) ? p[i] : max;
	avrg += (double) p[i] / (double) size;
    }

    float ans = avrg;
    printf("average = %f, min = %d, max = %d\n", ans, min, max);
    free(p);
    return min;
}

int main (int argc, char *argv[]) {
    int i = 0;
    int one = 0;
    while (one != 1) {
    one = excercise(10000);
    ++i;
    }
}

