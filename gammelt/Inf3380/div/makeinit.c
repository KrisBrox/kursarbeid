#include <stdio.h>
#include <stdlib.h>

#define rdim 25
#define cdim 25

int main(int argc, char** argv) {
    FILE *file;
    file = fopen("init.txt", "w");
    int i=0, j=0;
    for (i = 0; i<rdim; i++) {
	for (j = 0; j<cdim; j++) {
	    if (i == 12 && j <= 14 && j >= 12) {
		fprintf(file, "%d", 1);
	    } else {
		fprintf(file, "%d", 0);
	    }
	}
	fprintf(file, "\n");
    }  
    fclose(file);
    return 0;
}
