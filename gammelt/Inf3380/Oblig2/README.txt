README for Oblig2 inf3380 by kristian brox

compile with: "mpicc -o prog matrix_binaryformat.c prog.c -fopenmp"

run with: mpirun -np <number of processes> prog <matrix_a.bin> <matrix_b.bin>

result matrix: binary file "matrix_c.bin"

Some comments:
    The program currently compiles and runs without crashing, but I've not
    checked whether it computes the result matrix C correctly.

    For some reason the program crashes at runtime (segfaults) when I add more 
    variables.
    The reason for this is a mystery to me, though I suspect it's caused by
    some flawed code (that I've been unable to locate). 

    Update (after group class 05.14):
    The reason for the problem described above was calling
    MPI_Scatterv (line 58) without making sure that matrix_a[0] was an
    accessible address. Changing argument 1 to "id ? NULL:matrix_a[0]" solved
    the problem.
