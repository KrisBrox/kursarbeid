#ifndef MPI_UTILS_H__
#define MPI_UTILS_H__

#include <stdio.h>  /* fprintf */
#include <stdlib.h> /* exit, EXIT_* */
#include <mpi.h>

/* POSIX SPECIFIC DEBUG */
#ifdef DEBUG_BUILD
# include <sys/types.h> /* pid_t */
# include <unistd.h>    /* getpid, hostname */

# define DEBUG_PID_ATTACH			\
  do {						\
    volatile int i = 0;				\
    char hostname[128];				\
						\
    gethostname(hostname, sizeof(hostname));	\
						\
    printf("PID %d on %s ready for attach.\n",	\
	   getpid(), hostname);			\
    fflush(stdout);				\
						\
    while (!i)					\
      sleep(1);					\
  } while (0)
#else /* NOT DEBUG_BUILD */
# define DEBUG_PID_ATTACH ((void) 0)
#endif /* DEBUG_BUILD */

/* MPI */

/**
 * ROOT_PROCESS
 * Signifies which process rank that should
 * be referred to as the root process.
 */
#define ROOT_PROCESS 0

/**
 * PROCESS_IS_ROOT
 * Checks whether a rank is root
 * process or not.
 *
 * @param  (int) rank to check with
 * @return (int) zero if rank isn't root and non-zero otherwise
 */
#define PROCESS_IS_ROOT(id) \
  ((id) == ROOT_PROCESS)

/* MPI-UTILS */

/**
 * VALID_PROCESS
 * Returns either a valid rank or MPI_PROC_NULL.
 *
 * @param  (int) candidate rank
 * @param  (int) total number of processes
 * @return (int) candidate if it's a valid rank and MPI_PROC_NULL otherwise
 */
#define VALID_PROCESS(id, p) \
  (((id) < 0) || ((id) >= (p)) ? MPI_PROC_NULL : (id))

/**
 * BLOCK_LOW
 * Returns the offset of a local array
 * with regards to block decomposition
 * of a global array.
 *
 * @param  (int) process rank
 * @param  (int) total number of processes
 * @param  (int) size of global array
 * @return (int) offset of local array in global array
 */
#define BLOCK_LOW(id, p, n) \
  ((id)*(n)/(p))

/**
 * BLOCK_HIGH
 * Returns the index immediately after the
 * end of a local array with regards to
 * block decomposition of a global array.
 *
 * @param  (int) process rank
 * @param  (int) total number of processes
 * @param  (int) size of global array
 * @return (int) offset after end of local array
 */
#define BLOCK_HIGH(id, p, n) \
  (BLOCK_LOW((id)+1, (p), (n)))

/**
 * BLOCK_SIZE
 * Returns the size of a local array
 * with regards to block decomposition
 * of a global array.
 *
 * @param  (int) process rank
 * @param  (int) total number of processes
 * @param  (int) size of global array
 * @return (int) size of local array
 */
#define BLOCK_SIZE(id, p, n) \
  ((BLOCK_HIGH((id), (p), (n))) - (BLOCK_LOW((id), (p), (n))))

/**
 * BLOCK_OWNER
 * Returns the rank of the process that
 * handles a certain local array with
 * regards to block decomposition of a
 * global array.
 *
 * @param  (int) index in global array
 * @param  (int) total number of processes
 * @param  (int) size of global array
 * @return (int) rank of process that handles index
 */
#define BLOCK_OWNER(i, p, n) \
  (((p)*((i)+1)-1)/(n))

/**
 * FINALIZE_AND_EXIT
 * Exits immediately after finalizing.
 *
 * @param (int) exit status
 * THIS MACRO DOES NOT RETURN
 */
#define FINALIZE_AND_EXIT(status) \
  do { MPI_Finalize(); exit(status); } while (0)

/**
 * FINALIZE_AND_FAIL
 * Finalizes and exits signaling failure.
 */
#define FINALIZE_AND_FAIL \
  FINALIZE_AND_EXIT(EXIT_FAILURE)

/**
 * FINALIZE_AND_SUCCEED
 * Finalizes and exits signaling success.
 */
#define FINALIZE_AND_SUCCEED \
  FINALIZE_AND_EXIT(EXIT_SUCCESS)

/**
 * root_fprintf
 * Produces output according to the printf
 * family of functions to a certain stream
 * if process rank is root.
 *
 * @param  (int) rank of process
 * @param  (FILE *) output stream
 * @param  (const char *) format string
 * @param  (...) printf-like arguments
 * @return (int) the number of characters printed to stream
 */
#define root_fprintf(id, ...) \
  (PROCESS_IS_ROOT(id) ? fprintf(__VA_ARGS__) : 0)

/**
 * root_printf
 * Produces output according to the printf
 * family of functions to the stdout stream
 * if process rank is root.
 *
 * @param  (int) rank of process
 * @param  (const char *) format string
 * @param  (...) printf-like arguments
 * @return (int) the number of characters printed to stream
 */
#define root_printf(id, ...) \
  root_fprintf((id), stdout, __VA_ARGS__)

/**
 * root_printf
 * Produces output according to the printf
 * family of functions to the stderr stream
 * if process rank is root.
 *
 * @param  (int) rank of process
 * @param  (const char *) format string
 * @param  (...) printf-like arguments
 * @return (int) the number of characters printed to stream
 */
#define root_eprintf(id, ...) \
  root_fprintf((id), stderr, __VA_ARGS__)

#endif /* MPI_UTILS_H__ */
