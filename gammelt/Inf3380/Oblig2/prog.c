#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <string.h>
#include <omp.h>

extern void read_matrix_binaryformat (char* filename, 
		double*** matrix,int* num_rows, int* num_cols);
extern void write_matrix_binaryformat (char* filename, 
		double** matrix, int num_rows, int num_cols);

int main (int argc, char** argv) 
{
    int id, num_procs, num_rows_a, num_cols_a, low, high, sendcnt;
    int num_rows_b, num_cols_b, j, my_count, i;
    double **matrix_a, **matrix_b, **matrix_c;
    double *my_a, *my_c;
    
    /***********************************************************
    * Standard init functions
    ***********************************************************/
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    int *sendcnt_arr = (int*) malloc(num_procs*sizeof(int));
    int *offset_arr = (int*) malloc(num_procs*sizeof(int));

    /***********************************************************
    * Read the matrices from the files (every process has the
    * the whole matrix B, this sets a rather low limit on the 
    * maximum Dimensions of the input) 
    ***********************************************************/
    if (id == 0) {
	read_matrix_binaryformat(argv[1], &matrix_a,
				&num_rows_a, &num_cols_a);
    }
    read_matrix_binaryformat(argv[2], &matrix_b,
				&num_rows_b, &num_cols_b);
    
    MPI_Bcast(&num_cols_a, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&num_rows_a, 1, MPI_INT, 0, MPI_COMM_WORLD);

    /**************************************************************
    * Distribute matrix A
    **************************************************************/
    low = id * num_rows_a / num_procs;
    high = ((id+1) * num_rows_a / num_procs) -1;
    my_count = (high - low +1)*num_cols_a;
    my_a = malloc(my_count*sizeof(double));
    
    for (i = 0; i < num_procs; i++) {
        low = i * num_rows_a / num_procs;
        high = ((i+1) * num_rows_a / num_procs) -1;
        sendcnt = (high-low+1)*num_cols_a;
        sendcnt_arr[i] = sendcnt;
        offset_arr[i] = low*num_cols_a;
    }
    MPI_Scatterv(id ? NULL : matrix_a[0], sendcnt_arr, 
		offset_arr, MPI_DOUBLE, 
	        my_a, my_count, MPI_DOUBLE, 0, MPI_COMM_WORLD); 
    my_count /= num_cols_a;
    my_count *= num_cols_b;
    
    /********************************************************
    * Calculate the product of Ai and B 
    ********************************************************/
    my_c =(double*) malloc(my_count*sizeof(double));
    
    #pragma omp parallel for private(j)
    for (i = 0; i < my_count; i++) {
	my_c[i] = 0;
	for (j = 0; j < num_cols_a; j++) {
	    my_c[i] += my_a[(i%num_cols_b)*num_cols_a+j]*
		matrix_b[j][i%num_cols_b];
	}
    }

    /**********************************************************
    * Collect the pieces of Matrix C
    **********************************************************/
    if (id == 0) {
	for (i = 0; i < num_procs; i++) {
	sendcnt_arr[i] /= num_cols_a;
	sendcnt_arr[i] *= num_cols_b;
	offset_arr[i] /= num_cols_a;
	offset_arr[i] *= num_cols_b;
	}
    }
    if (id == 0) {
	matrix_c = (double**) malloc(num_rows_a*sizeof(double*)); 
	matrix_c[0]=(double*)malloc(num_rows_a*num_cols_b*sizeof(double));
	for (i=0;i<num_rows_a;i++) 
	    matrix_c[i]=&matrix_c[0][0] + i*num_cols_a;
    }

    MPI_Gatherv(my_c, my_count, MPI_DOUBLE,id ? NULL : &matrix_c[0][0],
		  sendcnt_arr, offset_arr, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    /*********************************************************
    * Write Matrix C to file "matrix_c.bin"
    *********************************************************/
    if (id == 0) {
	write_matrix_binaryformat("matrix_c.bin", matrix_c, 
				 num_rows_a, num_cols_b);
    }

    /*********************************************************
    * Some deallocating
    *********************************************************/
    if(id == 0) {
	free(matrix_c[0]);
	free(matrix_c);
	free(matrix_a[0]);
	free(matrix_a);
    }
    free(matrix_b[0]);
    free(matrix_b);
    free(my_a);
    free(my_c);

    MPI_Finalize();
    return 1;
}
