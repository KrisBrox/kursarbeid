#include <stdio.h>
#include <stdlib.h>
#include "image.h"

void read_image(image *u, int m, int n, unsigned char *data) {
    

}

void allocate_image (image *u, int m, int n) {
    u->data = (float**) malloc(m*sizeof(float*));
    u->m = m;
    u->n = n;
}


void deallocate_image (image *u) {
    free(u->data);
}


void iso_diffusion_denoising (image *u, image *u_bar, float kappa) {
    int i, j;
    for (i = 1; i < u->m -1; i++) {
	for (j = 1; j < u->n -1; j++) {
	    u_bar->data[i][j] = u->data[i][j] + 
		kappa*(u->data[i-1][j] + u->data[i][j-1] -
		4*u->data[i][j] + u->data[i][j+1] + u->data[i+1][j]);
	}
    }
}



