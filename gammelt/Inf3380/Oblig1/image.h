#ifndef image_h
#define image_h

typedef struct {
    float** data;
    int m;
    int n;
} image;

void allocate_image(image *image, int m, int n);

//void deallocate_image(image *image);

void iso_diffusion_denoising(image *u, image *u_bar, float kappa);

void import_JPEG_file(const char *filename, unsigned char **image_chars,
		    int *image_height, int *image_width,
		    int *num_components);

void export_jpeg_file(const char *filename, unsigned char *image_char,
		    int image_height, int image_width,
		    int num_components, int quality);

#endif
