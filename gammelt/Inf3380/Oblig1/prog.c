#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include "image.h"
#include <math.h>
#include <string.h>


int main(int argc, char** argv) {
    int id, num_procs, m, n, c, iters;
    int my_m, from_m, to_m, send_from_m, send_to_m; // local number of rows and offset
    float kappa, *my_image_floats, *my_new_image_floats; 
    unsigned char *image_chars, *my_image_chars, *send_buf;
    char *input_filename;
    char *output_filename;
    image *my_image = (image*) malloc(sizeof(image));
    image *my_new_image = (image*) malloc(sizeof(image));
    image *tmp;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    int *sendcnt_arr = (int*) malloc(num_procs*sizeof(int));
    int *offset_arr = (int*) malloc(num_procs*sizeof(int));

    if (argc != 5) {
	printf("You need to specify all of the following kappa, iterations, input filename and output filename, in that order.\n"); 
	MPI_Finalize();
	return 0;
    } 
    kappa = atof(argv[1]);
    iters = atoi(argv[2]);
    input_filename = argv[3];
    output_filename = argv[4];
    
    if (id == 0) {
	import_JPEG_file(input_filename, &image_chars, &m, &n, &c);
    }

    MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    
    /***********************************************************************
    * The following block is each process determining how many rows it will work on.
    ***********************************************************************/
    int size = m;
    if (id == 0) { 
	from_m = floor( size * id / num_procs);
	to_m = floor(((id+1) * size / num_procs));
    } else if (id == num_procs-1) {
	from_m = floor(( size * id / num_procs) -1);
	to_m = floor((size * (id+1) / num_procs) -1);
    } else {
	from_m = floor((size*id / num_procs) -1);
	to_m = floor((size*(id+1) / num_procs));
    }
    my_m = to_m - from_m + 1;
    
    int i, sendcnt;
    
    /***********************************************************************
    * The following block is proc 0 figuring out which process gets which rows of 
    * the picture. 
    ***********************************************************************/
    if (id == 0) {
	for (i = 0; i < num_procs; i++) {
	    if (i == 0) { 
		my_image_chars = malloc(my_m*n*sizeof(unsigned char));
		memcpy(my_image_chars, image_chars, my_m*n*sizeof(unsigned char));
	    } else if (i == num_procs-1) {
		send_from_m = floor(( size * i / num_procs) -1);
		send_to_m = floor((size * (i+1) / num_procs) -1);
		sendcnt = send_to_m - send_from_m + 1;
		sendcnt_arr[i] = sendcnt-1;
		offset_arr[i] = send_from_m + 1;
		send_buf = malloc(sendcnt*n*sizeof(unsigned char));
		memcpy(send_buf, &image_chars[send_from_m*n], sendcnt*n*sizeof(unsigned char));
	    } else {
		send_from_m = floor((size*i / num_procs) -1);
		send_to_m = floor((size*(i+1) / num_procs));
		sendcnt = send_to_m - send_from_m + 1;
		sendcnt_arr[i] = sendcnt-2;
		offset_arr[i] = send_from_m + 1;
		send_buf = malloc(sendcnt*n*sizeof(unsigned char));
		memcpy(send_buf, &image_chars[send_from_m*n], sendcnt*n*sizeof(unsigned char));
	    }
	    if (i != 0) {
		MPI_Send(send_buf, sendcnt*n, MPI_UNSIGNED_CHAR, i, 0, MPI_COMM_WORLD);
		free(send_buf);
	    }
	}
    }

    MPI_Status *s;
    if (id != 0) {
	my_image_chars = (unsigned char*) malloc(my_m*n*sizeof(unsigned char));
	MPI_Recv(my_image_chars, my_m*n, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD, s);
     }

    my_image_floats = (float*) malloc(my_m*n*sizeof(float));
    my_new_image_floats = (float*) malloc(my_m*n*sizeof(float));
    
    int j;    
    for (j = 0; j < my_m*n; j++) {
	my_image_floats[j] = (float) my_image_chars[j];
	my_new_image_floats[j] = (float) my_image_chars[j];
    }

    allocate_image(my_image, my_m, n);
    allocate_image(my_new_image, my_m, n);

    for (j = 0; j < my_m; j++) {
	my_image->data[j] = &my_image_floats[j*n];
	my_new_image->data[j] = &my_new_image_floats[j*n];
    }
    
    /*****************************************************************
    * Doing actual work.
    *****************************************************************/
    for (j = 0; j < iters; j++) {
	iso_diffusion_denoising(my_image, my_new_image, kappa);
	if (id != 0) {
	    MPI_Send(my_new_image->data[1], n, MPI_FLOAT, id-1, 0, MPI_COMM_WORLD); 
	}
	if (id != num_procs -1) {
	    MPI_Recv(my_new_image->data[my_m-1], n, MPI_FLOAT, id+1, 0,
			MPI_COMM_WORLD, s);
	    MPI_Send(my_new_image->data[my_m-2], n, MPI_FLOAT, id+1, 0,
			MPI_COMM_WORLD);
	}
	if (id != 0) {
	    MPI_Recv(my_new_image->data[0], n, MPI_FLOAT, id-1, 0, MPI_COMM_WORLD, s);
	}
	tmp = my_image;
	my_image = my_new_image;
	my_new_image = tmp;
    }
    
    /*****************************************************************
    * Converting the image back to unsigned char. The if-test is there to 
    * determine which of the two float-arrays holds the result.
    *****************************************************************/
    if (iters % 2 == 0 ) {
	for (i = 0; i < my_m*n; i++) {
	    my_image_chars[i] = (unsigned char) my_image_floats[i];
	}
    } else {
	for (i = 0; i < my_m*n; i++) {
	    my_image_chars[i] = (unsigned char) my_new_image_floats[i];
	}
    }

    /***************************************************************
    * Some deallocating.
    ***************************************************************/
    deallocate_image(my_image);
    deallocate_image(my_new_image);
    free(my_image_floats);
    free(my_new_image_floats);
    
    /*****************************************************************
    * Sending the pieces back to proc 0.
    *****************************************************************/
    if (id == num_procs-1) {
        MPI_Send(&my_image_chars[n], (my_m-1)*n, MPI_UNSIGNED_CHAR, 
		    0, 0, MPI_COMM_WORLD);
    } else if (id != 0) {
        MPI_Send(&my_image_chars[n],(my_m-2)*n, MPI_UNSIGNED_CHAR, 
		    0, 0, MPI_COMM_WORLD);
    } else {
	memcpy(image_chars, my_image_chars, (my_m-1)*n*sizeof(unsigned char));
    }
    if (id == 0) {
	for (j = 1; j < num_procs-1; j++) {
	    MPI_Recv(&image_chars[offset_arr[j]*n], (sendcnt_arr[j])*n, 
	    	    MPI_UNSIGNED_CHAR , j, 0, MPI_COMM_WORLD, s);

        }
	MPI_Recv(&image_chars[offset_arr[num_procs-1]*n], (sendcnt_arr[j])*n, 
		    MPI_UNSIGNED_CHAR ,num_procs-1, 0, MPI_COMM_WORLD, s);
    }

    /***************************************************************
    * More deallocating.
    ***************************************************************/
    free(my_image_chars);
    free(sendcnt_arr);
    free(offset_arr);
    
    /***************************************************************
    * Exporting the assembled image.
    ***************************************************************/
    if (id == 0) {
	export_JPEG_file(output_filename, image_chars, m,n,c, 75);
    }


    MPI_Finalize();
    return 0;
}




