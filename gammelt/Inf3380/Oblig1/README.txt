README for Oblig1 INF3380 av Kristian Brox

kompilering:
    mpicc prog.c smooth_image.c -L lib/ -limage -o prog

kj�ring:
    mpirun -np <antall prosesser> prog <kappa> <antall iterasjoner> 'noisy-paprika.jpg' 'paprika.jpg'

Det ser ut til at alt fungerer som det skal. resultat av kj�ring med kappa = 0.02 og 
iterasjoner = 30 ligger i denne mappen. 
