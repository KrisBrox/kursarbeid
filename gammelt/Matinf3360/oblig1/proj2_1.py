#!/usr/bin/env python
from scitools.std import *
import time

def trap(a,b,f,n):
    h = (b-a)/float(n)
    x = linspace(a,b,n+1)
    f = f(x)
    print h, x[1]
    return h*(f[0]+f[-1])/2.+h*sum(f[1:-1])

def c():
    g = lambda x : x**5
    h = lambda x : sqrt(abs(x-0.5))
    a = 0; b = 1
    nl = (10,20,40,80,160)
    for f in ((g,1/6.),(h,sqrt(2)/3)):
	diff = []
	if f[0] == g:
	    print 'approximating $\int_{%d}^{%d}x**5$' % (a,b)
	else:
	    print  'approximating $\int_{%d}^{%d}sqrt{|x-0.5|}$' % (a,b)
	for i in range(len(nl)):
	    num = trap(a,b,f[0],nl[i])
	    diff.append(abs(f[1]-num))
	    print 'difference=%g for n=%d' % (diff[-1],nl[i])
	for i in range(len(diff)-1):
	    alpha = log(diff[i]/diff[i+1])/ \
		    log((nl[(i+1)])/float(nl[(i)]))

	    print 'calculated convergence rate:%g'%alpha	

def f_(f,u_e, n, f_s):
    a = zeros(n+1); 
    b =	zeros(n+1); 
    u = zeros(n+1)
    x = linspace(0,1,2*n+1)
    if f != 1:
	f = f(x)
    else:
	f = ones(2*n+1)
    h = 1/float(n)
    for i in range(n):
	a[i+1] = a[i]+0.25*h*( f[2*i]+f[2*i+2] + 2*f[2*i+1])
	b[i+1] = b[i]+0.25*h*( x[2*i]*f[2*i] + 2*x[2*i+1]* \
	    f[2*i+1] + x[2*i]*f[2*i] )
    
    x = linspace(0,1,n+1)
    u =	x*(a[-1]-b[-1])+b-x*a
    u_e = u_e(x)
    errnorm = max(abs(u_e-u))
    print errnorm
    return errnorm
    #plot(x,u_e,x, u, \
#	title='plot of exact vs. computed u, f=%s, n=%d'%(f_s,n),\
#	hardcopy='comparef=%sn=%d.png'%(f_s,n))
 #   time.sleep(1)

if __name__ == '__main__':
    c()
    e = []
    for n in (10,20,40,80,160,320):
	 e.append(f_(1 , lambda x : 0.5*x*(1-x), n,'1'))

    for i in range(len(e)-1):
	print "ratio of error norms e[i+1]/e[i] = %.3f" % (e[i+1]/e[i])
    e=[]
    for n in (10,20,40,80,160,320):
	e.append(f_(lambda x : x, lambda x : (1/6.)*x*(1-x**2), n,'x'))
    for i in range(len(e)-1):
	print "ratio of error norms e[i+1]/e[i] = %.3f" % (e[i+1]/e[i])

"""
Result for f):

f = 1:
ratio of error norms e[i+1]/e[i] = 1.000
ratio of error norms e[i+1]/e[i] = 1.625
ratio of error norms e[i+1]/e[i] = 5.538
ratio of error norms e[i+1]/e[i] = 0.903
ratio of error norms e[i+1]/e[i] = 4.015

f = x:
ratio of error norms e[i+1]/e[i] = 0.500
ratio of error norms e[i+1]/e[i] = 0.500
ratio of error norms e[i+1]/e[i] = 0.500
ratio of error norms e[i+1]/e[i] = 0.500
ratio of error norms e[i+1]/e[i] = 0.500
"""
