#!/usr/bin/env python
from scitools.std import *
import time

def u_exact(x,t):
    return 3*exp(-pi**2*t)*sin(pi*x)+5*exp(-16*pi*pi*t)*sin(4*pi*x)

def semidiscrete(n,t):
    h = 1/float(n+1)
    mu = zeros(n)
    for j in xrange(n):
	k = j+1
	mu[j] = -4*sin(k*pi*h/2.)**2/h**2
    
    def u_approx4(x, t):
	return 3*exp(mu[0]*t)*sin(pi*x)+5*exp(mu[3]*t)*sin(4*pi*x) 
    def u_approx2(x,t):
	return (5/2.)*sqrt(3)*exp(mu[0]*t)*sin(pi*x)+ \
		(3/2.)*sqrt(3)*exp(mu[1]*t)*sin(2*pi*x) 

    x = linspace(0,1,50)
    if n==2:
	u_a = u_approx2
    else:
	u_a = u_approx4
    text = 'for n = %d' % n  
    plot(x, u_a(x,t), 'r',x,u_exact(x,t),'b',
	    legend=('semidiscrete solution %s'%text,'exact solution'), \
	    hardcopy='plotN=%d.eps'%n)

if __name__=='__main__':
    t = 0.01
    for n in [2,4,6]:
	semidiscrete(n,t)
	time.sleep(3)



