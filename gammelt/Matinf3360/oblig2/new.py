#/usr/bin/env python
from scitools.std import *

def diffusion1d(nx,nt,dt,f):
    dx = 1/float(nx-1)
    dt = 1/float(nt)

    A = zeros((nx,nx),dtype=float)
    for i in range(nx):
	for j in range(nx):
	    if i==j:
		A[i,j] = 2
	    elif i == j-1 or i == j+1
		A[i,j] = -1
    A *= 1/(dx**2)
    

if __name__ == '__main__':
    Nx = 100
    Nt = 100


    diffusion1d(Nx, Nt, f)
