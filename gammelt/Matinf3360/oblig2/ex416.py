#!/usr/bin/env python
from scitools.std import *

def diffusion1d(nx,nt, T,f):
    dx = 1/float(nx-1)
    dt = T/float(nt)
    r = dt/dx**2
    """
    A = zeros((nx,nx),dtype=float)
    for i in range(nx):
	for j in range(nx):
	    if i==j:
		A[i,j] = 2
	    elif i == j-1 or i == j+1:
		A[i,j] = -1
    A *= 1/(dx**2)
    """
    x = linspace(0,1,nx)
    v_ = zeros(nx, dtype=float); 
    v = zeros(nx, dtype=float)
    for i in range(len(v)):
	v_[i] = f(x[i])

    def gamma(k):
	return  2*dx*v_*sin(k*pi*x)

    def a(mu):
	return (1-dt*mu/2.)/(1+dt*mu/2.)

    def mu(k):
	return 4*sin(k*pi*dx/2.)**2/dx**2	

    ret = 0
    for k in range(1,nx+1):
	ret += gamma(k)*a(mu(k))**nt*sin(k*pi*x)
    print r
    return x, ret

if __name__ == '__main__':
    Nx = 500
    Nt = Nx/2.
    T = 1.5
    def f(x):
	if x <= 0.5:
	    return 2*x
	elif x > 0.5:
	    return 2*(1-x)
    x, y = diffusion1d(Nx, Nt, T, f)
    plot(x, y)
