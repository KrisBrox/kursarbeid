def ForwardEuler(f, u0, T, n):
    from numpy import zeros

    t = zeros(n+1)
    u = zeros(n+1)

    u[0] = u0
    t[0] = 0

    dt = T/float(n)

    for k in range(n):
        t[k+1] = t[k] + dt
        u[k+1] = u[k] + dt*f(u[k], t[k])

    return u, t

def f(h, t):
    from numpy import sqrt
    return -(r/R)**2 * (1 + (r/R)**4)**(-0.5) * sqrt(2*g*h)

T = 200; h0 = 1; dt = 10
n = T / dt
r = .01; R = .2; g = 9.81

u, t = ForwardEuler(f, h0, T, n)

from scitools.std import *
plot(t, u)

a = (r/R)**2 * (1 + (r/R)**4)**(-0.5)

x = linspace(0, T)
y = 0.5 * a**2 * g * x**2 - sqrt(2) * a * sqrt(g) * x + 1
hold("on")
plot(x, y)
