from scitools.all import *

x0 = 1
tp = 0
a = 0
b = 0.6
n = 6


def f(t, x) :
    return 1+x*x

def _oppg2d(n, xp, a, b) :
    anst = zeros(n+1)
    ansx = zeros(n+1)
    h = (b-a)/n
    tp = a
    anst[0] = tp
    ansx[0] = xp
    for i in range (1, n+1):
	xpp = (2-h*xp-2*sqrt(1-h**2-2*xp*h))/h
	xp = xpp
	anst[i] = anst[i-1]+h
	ansx[i] = xpp
	
    return anst, ansx	

def _oppg2c(n, xp, a, b):
    anst = zeros(n+1)
    ansx = zeros(n+1)
    h = (b-a)/n
    tp = a
    anst[0] = tp    
    ansx[0] = xp
    for i in range(1,n+1):
	#xpp = xp + h*f(tp, xp)
	xpp = xp + h*f(tp+h/2.0, xp + (h/2)*f(tp, xp))
	tpp = a + (i) * h
	anst[i] = tpp
	ansx[i] = xpp
	xp = xpp
	tp = tpp
	print tpp, xpp
    return anst, ansx

def _oppg2b(n, xp, a, b):
    anst = zeros(n+1)
    ansx = zeros(n+1)
    h = (b-a)/n
    tp = a
    realx = zeros(n+1)
    anst[0] = tp    
    ansx[0] = xp
    for i in range(1,n+1):
	xpp = xp + h*f(tp, xp)
	#xpp = xp + h*f(tp+h/2.0, xp + (h/2)*f(tp, xp))
	tpp = a + (i) * h
	anst[i] = tpp
	ansx[i] = xpp
	xp = xpp
	tp = tpp
	print tpp, xpp
    return anst, ansx

anst, ansx = _oppg2b(n, x0, a, b)

plot(anst, ansx)
hold('on')	
xval = linspace(0,0.6)
plot(xval, tan((pi/4)+xval))
hold('on')
anst, ansx = _oppg2c(n, x0, a,b)
plot(anst, ansx)
hold('on')
anst, ansx = _oppg2d(n,x0,a,b)

plot(anst, ansx, hardcopy='svarOppg2')


