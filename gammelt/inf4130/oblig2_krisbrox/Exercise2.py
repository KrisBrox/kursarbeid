#!/usr/bin/env python
from copy import deepcopy
class State:
    def __init__(self, board, n):
	self.kids = []
	self.n = n
	self.board = board
	self.wrong = self.h()
	for i in range(n):
	    for j in range(n): 
		if board[i][j] == 0:
		    self.empty = (i, j)
    


    def h(self):
	k = -1
	for i in range(self.n):
	    for j in range(self.n):
		if self.board[i][j] != i*self.n+j+1:
		    k += 1
	return k

    def findkids(self):
	for (i, j) in ((-1, 0),(0, -1),(1, 0), (0, 1)):
		try:
		    self.kids.append(State(self.gennewboard(i, j), self.n))
		except:
		    print 'x'
		    pass


    def gennewboard(self, ii, jj):
	i, j = self.empty
	if i+ii < 0 or j+jj < 0: raise indexError;

	return self.swap(i, j, i+ii, j+jj, self.board)

    def swap(self, i, j, ii, jj, board):
	newboard = deepcopy(board)
	newboard[i][j] = newboard[ii][jj]
	newboard[ii][jj] = 0
	return newboard
	
def solve():




t = State([[5, 2, 3],[4,0,6],[7,8,1]], 3)
print t.h()
t.findkids()
print t.board
print '-------------------------------'
for kid in t.kids:
    print kid.board
    print kid.h()


