#!/usr/bin/env python
import sys

class Solver:
    def __init__(self, n, K, Sn, Sp, U, nums):
	"""
	A Class containing all relevant variables, allowing me to 
	not have to pass too many parameters to the method u.
	Note that i have extended the list of ti's with the number 0,
	although i could have written the method to not require that,
	doing so is an easy fix to make the recursive method work.
	Also note that I only fill in the fields of U that actually solve
	the specific SOS-problem, not all possible sums like in ex.1b.
	"""
	self.U = U
	self.nums = nums
	self.nums.insert(0, 0)
	print 'problem: ', n, K, nums
	self.u(n+1,K-Sn)
	table = ''
	for row in self.U:
	    for item in row:
		table += '%3s' % str(item)
	    table += '\n'
	print table

    def u(self, i, j):
	if self.U[i][j]:
	    return 1
	elif i == 1:
	    return 0
	else:
	    if self.u(i-1, j) or self.u(i-1, j-int(self.nums[i-1])):
		self.U[i][j] = 1
	    return self.U[i][j]
	    
def setup(p):
    """
    Sets up U and extracts from the list p all values required to solve the SOS-problem.
    Then makes a Solver to solve the problem.
    """
    n = int(p[0])
    K = int(p[1])
    Sn = Sp = 0
    for ti in p[2:]:
	ti = int(ti)
	if ti < 0:
	    Sn += ti
	else:
	    Sp += ti
    U = [[0 for j in range(Sn, Sp+1)] for i in range(n+1)]
    first = [i for i in range (Sn, Sp+1)]
    U.insert(0, first)
    U[1][-Sn] = 1
    x = Solver(n, K, Sn, Sp, U, p[2:])
try:
    f = open(sys.argv[1], 'r')
except:
    sys.exit('usage: %s <input file>')

probs = []
for line in f:
    probs.append(line[:-2])

for p in probs:
    setup(p.split())


