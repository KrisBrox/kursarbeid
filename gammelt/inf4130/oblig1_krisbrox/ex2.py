#!/usr/bin/env python
import sys
global ret
ret = ''
class Node:
    """
    a simple class for trie nodes.
    """
    def __init__(self, string = "", parent = None):
	self.kids = []
	self.string = string
	self.parent = parent

    def __str__(self):
	return self.string

    def add(self, new):
	self.kids.append(new)

class Trie:
    """
    a somewhat more complex Trie class. 
    """
    def __init__(self):
	self.root = Node()
    
    def insert(self, w):
	"""
	handles one-character words, if w has more than one character,
	it is passed on to recinsert.
	"""
	for kid in self.root.kids:
	    if w[0] in kid.string:	
		return self.recinsert(w[1:], kid)
	new = Node(w[0], self.root)
	self.root.add(new)
	return self.recinsert(w[1:], new)

    def recinsert(self, w, where):
	""" 
	inserts a word in the trie.
	"""
	if len(w) == 0:
	    if where.kids:
		return 0
	    else: 
		return 1
	for kid in where.kids:
	    if kid.string == w[0]:
		return self.recinsert(w[1:], kid)

	new = Node(w[0], where)
	where.add(new)
	return self.recinsert(w[1:], new)

    def lookup(self, w, where):
	"""
	look in the trie and see if w is in it.
	"""
	if len(w) == 0:
	    return 1
	for kid in where.kids:
	    if kid.string == w[0]:
		return self.lookup(w[1:], kid)
	return 0

    def prefix(self, where):
	global ret
	if where.parent and len(where.parent.kids) > 1:
	    ret += '('
	ret += where.string
	for i in range(len(where.kids)):
	    self.prefix(where.kids[i])
	    if len(where.kids) > 1:
		ret += ')'
try:
    inf = open(sys.argv[1], 'r')
    outf = open(sys.argv[2], 'w')
except:
    sys.exit('usage: %s <infile> <outfile>' % sys.argv[0])

N = int(inf.readline())
words = []
x = Trie()

# read from file
for line in inf:
    if N > 0:
	N -= 1
	words.append(line[:-2]) #remove newline character "\r\n"

# construct the trie, output progress to file
for w in words:
    if x.insert(w):
	x.prefix(x.root)
	outf.write( w +' ' + ret+'\n')
	ret = ''
    else:
	outf.write( w + ' PREFIX'+'\n')

# lookup the rest of the words in the file
inf.close()
inf = open(sys.argv[1], 'r')
N = int(inf.readline())
for line in inf:
    if N < 1:
	if x.lookup(line[:-2], x.root):
	    outf.write( line[:-2] + ' YES\n' )
	else: 
	    outf.write( line[:-2]+' NO\n' )
    N -= 1

inf.close()
outf.close()
