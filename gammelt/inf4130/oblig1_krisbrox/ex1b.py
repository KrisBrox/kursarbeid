#!/usr/bin/env python
import sys

def solve(n, K, Sn, Sp, U, nums):
    """
    prints the solution table for an SOS-problem. note that for large values of Sp-Sn
    the table will not be very pretty in the terminal window.
    """
    print 'problem:', n, K, ' '.join(nums)
    U[1][-Sn] = 1
    for i in range(2,len(U)):
	for j in range(len(U[0])):
	    if U[i-1][j]:
		U[i][j] = 1
		U[i][j+int(nums[i-2])] = 1
    table = ''
    for row in U:
	for item in row:
	    table += '%3s' % str(item)
	table += '\n'
    print table

def setup(p):
    """
    Sets up U and extracts from the list p all values required to solve the SOS-problem.
    Then calls solve to solve the problem.
    """
    n = int(p[0])
    K = int(p[1])
    Sn = Sp = 0
    for ti in p[2:]:
	ti = int(ti)
	if ti < 0:
	    Sn += ti
	else:
	    Sp += ti
    U = [[0 for j in range(Sn, Sp+1)] for i in range(n+1)]
    first = [i for i in range (Sn, Sp+1)]
    U.insert(0, first)
    solve(n, K, Sn, Sp, U, p[2:])

try:
    f = open(sys.argv[1], 'r')
except:
    sys.exit('usage: %s <input file>')

probs = []
for line in f:
    probs.append(line[:-2])

for p in probs:
    setup(p.split())


