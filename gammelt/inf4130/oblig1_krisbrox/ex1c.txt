Exercise 1 c)

if we are only interested in a yes/no answer to the SOS-problem, the 
algorithm we are using can be modified to use only one row from the U-array.
in addition we would use an array that keeps track of which cells of U were 
were 1 in the previous iteration. we iterate over U for each ti as before,
but only mark new cells if the one we are standing in was marked in the 
previous function call. 
    This would be roughly equivalent to discarding row k-2 when we begin 
processing row k, or having two rows change roles between each iteration,
one being row n and the other being n-1. although this uses two arrays of 
dimension 1x[Sp-Sn], it is clearly O(Sp-Sn) space.
