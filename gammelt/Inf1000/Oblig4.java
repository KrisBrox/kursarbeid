import easyIO.*;
import java.util.*;

class Oblig4 {
    public static void main(String[] args) {
	Filmregister filmregister = new Filmregister();
	filmregister.ordrelokke();
    }
}

class Person {
    String kode, navn;
    Person(String kode,String navn) {
	this.kode = kode;
	this.navn = navn;
    }
}

class Film {
    String kode, tittel, �r, reg, stjerner, sjangre, serie;
    Film(String kode, String tittel,String �r,String reg,String stjerner,String sjangre,String serie){
	this.kode = kode;
	this.tittel = tittel;
	this.�r = �r;
	this.reg = reg;
	this.stjerner = stjerner;
	this.sjangre = sjangre;
	this.serie = serie;
    }
    Film(String kode, String tittel,String �r,String reg,String stjerner,String sjangre){
	this.kode = kode;
	this.tittel = tittel;
	this.�r = �r;
	this.reg = reg;
	this.stjerner = stjerner;
	this.sjangre = sjangre;
    }
}

class Filmregister {
    In tast = new In();
    Out skjerm = new Out();

    HashMap<String, Person> personer = new HashMap<String, Person>();
    HashMap<String, Film> filmer = new HashMap<String, Film>();
    TiAar[] ti�r = new TiAar[11];

    Filmregister() {
	In fil = new In("persondata.txt");
	fil.inLine();
	while(! fil.endOfFile()){
	    String kode = fil.inWord();
	    String navn = fil.inLine();

	   Person p = new Person(kode, navn);
	   personer.put(kode, p);
	}
	fil.close();

	fil = new In("filmdata.txt");
	fil.inLine();
	while (!fil.endOfFile()) {
	    String linje = fil.inLine();
	    String[] felt = linje.split("\t");
	    if (felt.length==6){
		Film f = new Film(felt[0], felt[1], felt[2], felt[3], felt[4], felt[5]);
		filmer.put(felt[0], f);
	    } else if(felt.length==7) {
		Film f = new Film(felt[0], felt[1], felt[2], felt[3], felt[4], felt[5], felt[6]);
		filmer.put(felt[0], f);
	    }
	}
	fil.close();
    }
    void ordrelokke(){
		String ordre = "";
		char char0 = '?';
		visMeny();

		while (!ordre.equals("q")) {
		    skjerm.out("Kommando ('?' = meny): ");
		    ordre = tast.readLine();

		    int ordreLengde = ordre.trim().length();
		    if (ordreLengde>0) {
				char0 = ordre.charAt(0);
		    }
		    if (ordreLengde==1 && char0 == '?') {
				visMeny();
		    } else if (ordreLengde == 3 && char0 >= 'A' && char0 <= 'Z') {
				finnFilm(ordre);
		    } else if (ordreLengde > 3 && char0 >= 'A' && char0 <= 'Z') {
				visFilmInfo(ordre);
		    } else if (ordreLengde == 1 && char0 == '.') {
				visStatistikk();
		    } else if (ordreLengde == 3 && char0 >= 'a' && char0 <= 'z') {
				finnPerson(ordre);
		    } else if (ordreLengde > 3 && char0 >= 'a' && char0 <= 'z') {
				visPersonInfo(ordre);
		    } else if (ordreLengde == 2 && char0 >= '0' && char0 <= '9' && ordre.charAt(1) == '0') {
				visTi�rInfo(ordre);
			}
 	   }
   }
    void visMeny(){
	skjerm.outln("...");
	skjerm.outln(".      = Vis statistikk");
	skjerm.outln("?      = Vis meny");
	skjerm.outln("AAA1   = Vis info om film");
	skjerm.outln("AAA    = Finn film");
	skjerm.outln("tom1   = Vis info om en person");
	skjerm.outln("tom    = Finn person");
	skjerm.outln("90     = Vis info om et ti�r");
	skjerm.outln("q      = Avslutt");
    }

    void visStatistikk() {
		int[] decades = new int[11];
		Iterator it = filmer.values().iterator();
		while (it.hasNext()) {
			Film f = (Film) it.next();
			int premAr = Integer.parseInt(f.�r);
			if (1899 < premAr && premAr < 1910) {
				decades[0]++;
			} else if (premAr >= 1910 && premAr <= 1919) {
				decades[1]++;
			} else if (premAr >= 1920 && premAr <= 1929) {
				decades[2]++;
			} else if (premAr >= 1930 && premAr <= 1939) {
				decades[3]++;
			} else if (premAr >= 1940 && premAr <= 1949) {
				decades[4]++;
			} else if (premAr >= 1950 && premAr <= 1959) {
				decades[5]++;
			} else if (premAr >= 1960 && premAr <= 1969) {
				decades[6]++;
			} else if (premAr >= 1970 && premAr <= 1979) {
				decades[7]++;
			} else if (premAr >= 1980 && premAr <= 1989) {
				decades[8]++;
			} else if (premAr >= 1990 && premAr <= 1999) {
				decades[9]++;
			} else if (premAr >= 2000 && premAr <= 2009) {
				decades[10]++;
			}
		}
		for (int j=0; j<decades.length; j++) {
			System.out.println("antall filmer produsert mellom "+ (1900+j*10) + " og "+ (1909+j*10)+ ": "+ decades[j]);
		}
		int i = filmer.size();
		System.out.println("Totalt antall filmer = "+ i);
    }

    void visFilmInfo(String ordre1){
		int j = 0;
		Film f = (Film) filmer.get(ordre1);
		String starring = f.stjerner;
		int lengde = starring.trim().length();
		for (int i=0; i<lengde; i++) {
			if (starring.charAt(i) == ';') {
				j++;
			}
		}
		System.out.println("-----------------------------------------");
		System.out.println("Tittel: "+f.tittel);
		System.out.println("Premiere: "+f.�r);
		System.out.println("Regi: "+personer.get(f.reg).navn);
		System.out.println("Viktigste skuespillere: ");
		String[] stars = new String[j+1];
		int k=0;
		for (int i=0; i<stars.length; i++) {
			stars[i] = "";
			while (starring.charAt(k)!= ';' && k<lengde-1){
				stars[i] += starring.charAt(k);
				k++;
			}
			k++;
			
			Person p = personer.get(stars[i]);
			if (!(p == null)) {
			System.out.println(p.navn);
			}
		}
		System.out.println("------------------------------------------");
    }

    void finnFilm(String ordre2) {
		System.out.println();
		Iterator it = filmer.values().iterator();
		while (it.hasNext()) {
			Film f = (Film) it.next();
			if (f.kode.contains(ordre2)) {
				System.out.println("("+f.kode+") "+ f.tittel);
			}
		}
		System.out.println();
    }

    void visPersonInfo(String ordre5) {
		Person p = (Person) personer.get(ordre5);
		System.out.println("Navn: "+p.navn);
		Iterator it = filmer.values().iterator();
		while (it.hasNext()) {
			Film f = (Film) it.next();
			if (f.reg.equals(ordre5) && f.stjerner.contains(ordre5)) {
				System.out.println("(regi og skuespill) ("+f.kode+")"+f.tittel);
			} else if (f.reg.equals(ordre5)) {
				System.out.println("(regi)              ("+f.kode+")"+f.tittel);
			} else if (f.stjerner.contains(ordre5)) {
				System.out.println("(skuespill)         ("+f.kode+")"+f.tittel);
			}
		}
    }

    void finnPerson(String ordre4) {
		Iterator it = personer.values().iterator();
		while (it.hasNext()) {
			Person p = (Person) it.next();
			if (p.kode.contains(ordre4)) {
				System.out.println("("+p.kode+") "+p.navn);
			}
		}
    }

    void visTi�rInfo(String ordre6){
		String s = "";
		System.out.println();
		System.out.println("Viktige filmer dette ti�ret: ");
		System.out.println();
		int antFilmer = 0;
		Regissor flestFilmer = null;
		HashMap<String, Regissor> regs = new HashMap<String, Regissor>();
		Iterator it = filmer.values().iterator();
		while (it.hasNext()) {
			Film f = (Film) it.next();
			if (f.reg.charAt(0) != '-' && f.�r.length() == 4 && f.�r.charAt(2) == ordre6.charAt(0)) {
				if (f.sjangre.length()>1) {
					if (f.sjangre.contains("1")) {
						s += 1;
					} if (f.sjangre.contains("2")) {
						s+=2;
					} if (f.sjangre.contains("3")) {
						s+=3;
					} if (f.sjangre.contains("4")) {
						s+=4;
					} if (f.sjangre.contains("5")) {
						s+=5;
					}
				}
				if (s.length()>1) {
					System.out.println(f.tittel+": ");
					for (int i=0; i<s.length(); i++) {
						switch(s.charAt(i)){
							case '1': System.out.println("- 'Films considered the greatest ever' (Wikipedia). "); break;
							case '2': System.out.println("- Academy award. "); break;
							case '3': System.out.println("- Palme d'Or. "); break;
							case '4': System.out.println("- Sight & Sound. "); break;
							case '5': System.out.println("- AFI's 100 Years... 100 Movies. "); break;
						}
					}
				}
				s = "";
				if (!regs.containsKey(f.reg)) {
					Person p = (Person) personer.get(f.reg);
					Regissor r = new Regissor(f.reg, p.navn, 1);
					regs.put(f.reg, r);
				} else {
					regs.get(f.reg).n++;
				}
			}
		}
		it = regs.values().iterator();
		while (it.hasNext()) {
			Regissor r = (Regissor) it.next();
			if (r.n >= antFilmer) {
				flestFilmer = r;
				antFilmer = r.n;
			}
		}
		System.out.println();
		System.out.print("Regissert flest filmer mellom "+ ordre6+" og "+ ordre6.charAt(0) + 9 + ": ");
		System.out.println(flestFilmer.navn1+" ("+flestFilmer.n+")");
		System.out.println();
		it = filmer.values().iterator();
    }
}

class Regissor {
	int n;
	String kode1;
	String navn1;
	Regissor (String kode, String navn, int m) {
		kode1 = kode;
		navn1 = navn;
		n = m;
	}
}

