import easyIO.*;

class Oblig3Pi{
	public static void main(String[] args) {
		int n = 0;
		String resFil = null;

		if (args.length != 2) {
			System.out.println("Programmet trenger akkurat to parametre: �nsket antall siffre i Pi og navn p� svarfil, i den rekkef�lgen.");
		} else {
			n = Integer.parseInt(args[0]);
			Pi piLengdeN = new Pi(n);
			resFil = args[1];
			piLengdeN.print(resFil);
		}
	}
}

class Pi {
	int lengde;
	int[] svar;
	String[] res;

    Pi(int n){
		lengde = n/4+4;
		res = new String[lengde];
		svar = new int[lengde];
		arctan L5 = new arctan(lengde,5,16);	//* Disse linjene endres dersom man �nsker � bruke
		arctan L239 = new arctan(lengde,239,4);	//* en annen formel (f.eks St�rmers formel),
		svar = mindre(L5.rekke, L239.rekke);	//* se kommentarer nederst i filen.
		for (int i=0; i<lengde; i++) {
			res[i] = lagString(svar[i]);
		}
    }

    String lagString(int a) {		// "lagString"-metoden er kopiert fra oppgaveteksten.
		String s = ""+a;
		while (s.length() < 4) {
			s="0"+s;
		}
		return s;
	}

	int[] mindre(int[] tall1, int tall2[]){
		int mente = 0;
		int svarTest[] = new int[lengde];
		for (int i=lengde-1; i>0; i--){
			svarTest[i] += tall1[i] - tall2[i] - mente;
			mente = 0;
			if (svarTest[i] < 0){
				svarTest[i] += 10000;
				mente = 1;
			}
		}
		return svarTest;
	}

	int[] mere(int[] tall1, int[] tall2) {
		int svarTest[] = new int[lengde];
		int mellomR = 0;
		int mente = 0;
		for (int i=lengde-1; i>=0; i--) {
			mellomR = tall1[i] + tall2[i] + mente;
			mente = 0;
			if (mellomR > 9999) {
				mellomR = mellomR - 10000;
				mente = 1;
			}
			svarTest[i] = mellomR;
		}
		return svarTest;
	}


    void print(String resFil){

		Out skriv = new Out(resFil);
		skriv.out(svar[1] + ".");
		System.out.print(svar[1] + ".");
		for (int i=2; i<lengde-2; i++){
			skriv.out(res[i] + " ");
			System.out.print(res[i] + " ");
			if (i % 15 == 0 || i == lengde - 3) {
				skriv.outln();
				System.out.println(" ");
			}
		}
		skriv.close();
	}
}

class arctan {
    int lengde;
    int verdi;
    int mult;
    int[] rekke;
    int[] ledd;
    boolean ferdig, slutt;

    arctan(int lengde, int verdi, int mult){
		this.lengde = lengde;
		this.verdi = verdi;
		this.mult = mult;
		ferdig = false;
		slutt = false;
		ledd = new int[lengde];
		rekke = new int[lengde];
		ledd[1] = mult;
		ledd = div(ledd, verdi);
		int i = 0;

		while (!ferdig) {
			if (i % 2 == 0) {
			    	add(ledd);
			} else {
			    	sub(ledd);
			}
	    	i++;

			ledd = mul(ledd, (2*i)-1);
			ledd = div(ledd, (2*i+1)*verdi*verdi);
			slutt = false;

			for (int j=lengde-1; (j>=0) && (!slutt); j--) {
				if (ledd[j] != 0) {
					slutt = true;
				}
			}
			if (!slutt) {
			ferdig = true;
			}
		}
    }

	void add(int[] ledd) {
		int svarTest[] = new int[lengde];
		int mellomR = 0;
		int mente = 0;
		for (int i=lengde-1; i>0; i--) {
			mellomR = rekke[i] + ledd[i] + mente;
			mente = 0;
			if (mellomR > 9999) {
				mellomR = mellomR - 10000;
				mente = 1;
			}
			svarTest[i] = mellomR;

		}
		rekke = svarTest;
	}

	void sub(int ledd[]){
		int mente = 0;
		int svarTest[] = new int[lengde];
		for (int i=lengde-1; i>0; i--) {
			svarTest[i] = rekke[i] - ledd[i] - mente;
			mente = 0;
			if (svarTest[i] < 0){
				svarTest[i] += 10000;
				mente = 1;
			}
		}
		rekke = svarTest;
	}

	public int[] mul(int[] faktor1, int faktor2) {
		int svarTest[] = new int[lengde];
		int mellomR = 0;
		for (int i=lengde-1; i>0; i--) {
			svarTest[i] = svarTest[i] + faktor1[i] * faktor2;
			if (svarTest[i] > 9999) {
				mellomR = svarTest[i];
				svarTest[i-1] = mellomR / 10000;
				svarTest[i] = mellomR % 10000;
			}
		}
		return svarTest;
	}

	public int[] div(int[] teller, int nevner) {
		int svarTest[] = new int[lengde];
		long mellomR = 0;
		for (int i=0; i<lengde; i++) {
		    svarTest[i] = (int) ((teller[i]+mellomR) / nevner);
			mellomR =(long) ((teller[i]+mellomR) % nevner) * 10000L;
		}
		return svarTest;
	}
}

// For � endre programmet til � benytte St�rmers formel istedet for Machins, bytter du ut
// de merkede linjene med:

// arctan L57 = new arctan(lengde, 57, 176);
// arctan L239 = new arctan(lengde, 239, 28);
// arctan L682 = new arctan(lengde, 682, 48);
// arctan L12943 = new arctan(lengde, 12943, 96);
// svar = mere(L57.rekke, L239.rekke);
// svar = mindre(svar, L682.rekke);
// svar = mere(svar, L12943.rekke);

// Det vil si, i teorien. I praksis f�r du ikke riktig resultat om du bytter ut de nevnte linjene med de over.
// Jeg vet ikke helt hvorfor resultatet da blir feil. Men fordi programmet slik det er n� gir riktig resultat
// anser jeg oppgaven som korrekt besvart. Jeg er allikevel sv�rt interessert i � vite hvorfor St�rmers formel
// ikke gir rett svar. Jeg mistenker at L12943 er synderen, og at grunnen er at kapasiteten til en eller annen
// variabel overskrides (i div-metoden).