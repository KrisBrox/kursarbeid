/* $Id: SimpleViewer.cpp, v1.1 2011/09/20$
 *
 * Author: Christopher Dyken, <dyken@cma.uio.no>
 * Reviewed by: Bartlomiej Siwek, <bartloms@ifi.uio.no>
 *
 * Distributed under the GNU GPL.
 */

#include "SimpleViewer.hpp"

#include <iostream>
#include <cmath>
#include <stdexcept>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace GfxUtil {
  
SimpleViewer::SimpleViewer() {
  setViewVolume(glm::vec3(0.0f, 0.0f, 0.0f), 1.74);
}

void SimpleViewer::setWindowSize(int w, int h) {
  m_window_width = w;
  m_window_height = h;
  m_window_aspect = (float)w / (float)h;
}

void SimpleViewer::setViewVolume(glm::vec3 origin, float radius) {
  // skip
  // set viewvolume related variables...
  // unskip
}

void SimpleViewer::rotationBegin(int x, int y) {
  // skip
  // handle beginning of the rotation...
    m_state = ROTATING;
    m_rotation_orientation_i = m_camera_orientation;
    m_rotation_mousepos_i = getPointOnUnitSphere(getNormalizedCoords(x, y));
    //printf("%f, %f, %f\n", m_camera_orientation.w, m_camera_orientation.x, m_camera_orientation.y);
  // unskip
}
void SimpleViewer::resetState(int /*x*/, int /*y*/) {
  m_state = NONE;
}

void SimpleViewer::motion(int x, int y) {
  // skip
  // handle mouse motion while one of the mouse buttons is down...
    glm::vec3 r = getPointOnUnitSphere(getNormalizedCoords(x, y));
    glm::fquat t = getGreatCircleRotation(m_rotation_mousepos_i, r);
    m_camera_orientation = t * m_rotation_orientation_i;
  // unskip
}

glm::mat4x4 SimpleViewer::getProjectionMatrix() {
  glm::mat4x4 result;
  // skip
  // Modify angle
  // unskip
    return  glm::perspective(45.0f, 4.0f / 4.0f, 0.1f, 100.0f);


  return result;
}

glm::mat4x4 SimpleViewer::getModelViewMatrix() {
    /*
    Source: http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
    Comment: finding this and realising that it was the solution to my problem was, in my opinion, enough work.
    */


	glm::mat4x4 result = glm::mat4(1.f);
	// apply rotation to result
	//glm::mat4x4 rotation = glm::rotate(1.0, glm::vec3(1.0, 1.0, 1.0));
	// move model into view volume

	glm::mat4 View       = glm::lookAt(
			glm::vec3(0,0,7),
			glm::vec3(0,0,0),
			glm::vec3(0,1,0)
			);

	glm::mat4 Model      =  glm::mat4_cast(m_camera_orientation);

	glm::mat4 MVP        =  View * Model;
	result = MVP;

	return result;
}

void SimpleViewer::renderDebugGraphics() {
	// This is a debug method that uses the deprecated API
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-m_window_aspect, m_window_aspect, -1.0f, 1.0f, -1.0f, 1.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	switch(m_state) {
		case ROTATING:
			glLineWidth(3.0);

			glColor3f(0.4, 0.4, 0.4);
      glBegin(GL_LINE_LOOP);
      for(size_t i=0; i<50; i++)
        glVertex2f(glm::cos(2.0*M_PI*i/(float)50), glm::sin(2.0*M_PI*i/(float)50));
      glEnd();

      glBegin(GL_LINES);
      glColor3f(1.0, 0.5, 0.5);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3fv(&m_rotation_axis[0]);

      glColor3f(0.5, 1.0, 0.5);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3fv(&m_rotation_mousepos_i[0]);

      glColor3f(0.5, 0.5, 1.0);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3fv(&m_rotation_mousepos_c[0]);

      glEnd();
      break;
    default:
      // Do nothing.
      break;
  }
}

glm::vec2 SimpleViewer::getNormalizedCoords(int x, int y) {
  // skip
  // compute the position of mouse in a coordinate system convinet for us...
  
  
  return glm::vec2(((float)x)/m_window_width - 0.5, 0.5 - ((float)y) / m_window_height);
  // unskip
}

glm::vec3 SimpleViewer::getPointOnUnitSphere(glm::vec2 p) {
  // skip
  // project mouse position in a convinient coordinate system to a unit sphere...
    double r = sqrt(p.x*p.x + p.y*p.y);
    if (r < 0.5) {
	return glm::vec3(2*p.x, 2*p.y,sqrt(1-4*r*r));
    } else {
	return glm::vec3(p.x/r, p.y/r, 0);
    }
  // unskip
}

// This function computes the quaternion corresponding to the great circle rotation between two vectors
glm::fquat SimpleViewer::getGreatCircleRotation(glm::vec3 a, glm::vec3 b) {
  glm::vec3 axis = glm::cross(a, b);
  float length = glm::length(axis);
  if(length > std::numeric_limits<float>::epsilon()) {
    axis = axis / length;

    float angle = glm::acos(glm::dot(a,b));
    float c = glm::cos(0.5*angle);
    float s = glm::sin(0.5*angle);
    return glm::fquat(c, s*axis[0], s*axis[1], s*axis[2]);
  } else {
    return glm::fquat(1.0f, 0.0f, 0.0f, 0.0f);
  }
}

// This function computes the quaternion corresponding to the rotation by a give angle around given axis
glm::fquat SimpleViewer::getAxisAngleRotation(glm::vec3& axis, float angle) {
  float c = glm::cos(0.5f * angle);
  float s = glm::sin(0.5f * angle);
  return glm::fquat(c, s*axis[0], s*axis[1], s*axis[2]);
}

} // namespace GfxUtil
