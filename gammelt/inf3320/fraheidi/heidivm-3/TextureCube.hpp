/* $Id: TextureCube.h,v 1.6 2006/10/02 13:08:49 dyken Exp $
 * (C) 2005 Christopher Dyken, <dyken@cma.uio.no>
 *
 * Distributed under the GNU GPL.
 */

#ifndef _TEXTURECUBE_H_
#define _TEXTURECUBE_H_

#include <GL/gl.h>
#include "Vec.hpp"
#include "NoiseSampler.hpp"

namespace GfxUtil {

    class TextureCube
    {
    public:
        TextureCube(float size=1.0):m_size(size){}
	// Initialize the texture cube, starting with texture number tex.
	void initGL(int tex);
	// Generate texture number tex, and set up so that future
	// render calls will use that texture.
	void generate(int tex);
	// Render the textured cube.
	void render();

    protected:
      float m_size;
	// Return the colour value of texture number tex at position p.
	GfxMath::Vec3f textureValue(int tex, const GfxMath::Vec3f& p);

	// All necessary geometry is stored in the following arrays.
	static GLfloat		m_vertices[8][3];
	static GLfloat		m_normals[6][3];
	static GLfloat		m_texcoords[4][2];
	static unsigned int	m_indices[6][4];
        static unsigned int     m_i[6][4];

	
    };

}
#endif //_TEXTURECUBE_H_
