//===========================================================================
//                                                                           
// File: GLUTWrapper.hpp                                                     
//                                                                           
// Created: Mon Sep 22 20:16:06 2008                                         
//                                                                           
// Author: Atgeirr F Rasmussen <atgeirr@sintef.no>
//                                                                           
// Revision: $Id$
//                                                                           
//===========================================================================

#ifndef SINTEF_GLUTWRAPPER_HEADER
#define SINTEF_GLUTWRAPPER_HEADER

#include "Vec.hpp"
#include <string>
#include <GL/glut.h>
#include <cstdlib>
#include "Singleton.hpp"

/// @brief A class that encapsulates GLUT functionality.
template <class Application>
class GLUTWrapper
{
public:
    /// The method that runs the application.
    static int run(int argc, char** argv)
    {
	glutInit(&argc, argv);
	bool init_ok = app().init(argc, argv);
	if (!init_ok) {
	    return EXIT_FAILURE;
	}
	glutInitDisplayMode(app().displayMode());
	glutInitWindowSize(app().initWindowSize()[0],
			   app().initWindowSize()[1]);
	glutCreateWindow(app().windowName().c_str());
	glutDisplayFunc(displayCallback);
	glutReshapeFunc(reshapeCallback);
	glutKeyboardFunc(keyboardCallback);
	glutMouseFunc(mouseCallback);
	glutMotionFunc(motionCallback);
	app().initGL();
	glutMainLoop();
	return EXIT_SUCCESS;
    }

private:
    /// Returns the singleton instance of the class.
    static Application& app()
    {
	// Using the 'Meyer singleton' technique
	// via the Singleton template.
	return Singleton<Application>::instance();
    }

    /// GLUT display callback.
    static void displayCallback()
    {
	app().display();
    }
    /// GLUT window reshape callback.
    static void reshapeCallback(int w, int h)
    {
	app().reshape(w, h);
    }
    /// GLUT keyboard callback.
    static void keyboardCallback(unsigned char key, int x, int y)
    {
	app().keyboard(key, x, y);
    }
    /// GLUT mouse button callback.
    static void mouseCallback(int button, int state, int x, int y)
    {
	app().mouse(button, state, x, y);
    }
    /// GLUT mouse move callback.
    static void motionCallback(int x, int y)
    {
	app().motion(x, y);
    }
};


#endif // SINTEF_GLUTWRAPPER_HEADER
