/* -*- mode: C++; tab-width:4; c-basic-offset: 4; indent-tabs-mode:nil -*-
 *
 * Time-stamp: <2005-07-13 12:01:05 dyken>
 *
 * (C) 2005 Christopher Dyken, <dyken@cma.uio.no>
 *
 * Distributed under the GNU GPL.
 */

#include <cmath>
#include <GL/glut.h>

#include "Mat.hpp"
#include "SimpleViewer.hpp"

using GfxMath::Vec2f;
using GfxMath::Vec3f;
using GfxMath::Quat4f;
using GfxMath::Mat4f;
namespace GfxUtil {

  SimpleViewer::SimpleViewer(){
    setViewVolume(GfxMath::Vec3f(0.0f, 0.0f, 0.0f), 1.74);
  }

  void SimpleViewer::setViewVolume(GfxMath::Vec3f origin, float radius){
    m_viewvolume_origin = origin;
    m_viewvolume_radius = radius;
    setWindowSize(250, 250);
    m_camera_distance = 4;
    m_rotation_mousepos_c = GfxMath::Vec3f(0,0,0);
    //Give identity quaternions
    m_rotation_orientation_c = GfxMath::Quat4f(1,0,0,0);
    m_rotation_orientation_i = GfxMath::Quat4f(1,0,0,0);
  }
  
  void SimpleViewer::rotationBegin(int x, int y){
    //Set the mouseposition when mouse is clicked
    m_rotation_mousepos_i = getPointOnUnitSphere(GfxMath::Vec2f(x,y));
    m_rotation_orientation_i = m_rotation_orientation_c;
    m_state = ROTATING;
  }
  
  void SimpleViewer::panBegin(int x, int y){ 
    // ... insert your code here ...
    //Have not done anything here yet.
    // ... end of your code ...
  }
  
  void SimpleViewer::zoomBegin(int x, int y){
    // ... insert your code here ...
    //Have not done anything here yet.
    // ... end of your code ...
  }
  

  void SimpleViewer::reshapeWindow(void) {
    glViewport(0,0,(GLsizei) m_window_width, (GLsizei) m_window_height);
    //fix the projection-matrix,new dimensions => new calculations
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60., (GLdouble) m_window_aspect, 2., 15.);

  }
  
  
  void SimpleViewer::motion(int x, int y)
  {
    m_rotation_mousepos_c = getPointOnUnitSphere(GfxMath::Vec2f(x,y));
    if (m_state == ROTATING) {//When we are rotating,do:
      GfxMath::Vec3f vec_2 = m_rotation_mousepos_c;
      GfxMath::Vec3f vec_1 = m_rotation_mousepos_i;
      GfxMath::Quat4f t = GfxMath::Quat4f::greatCircleRotation(vec_1, vec_2);
      m_rotation_orientation_c = t*m_rotation_orientation_i;
      m_rotation_axis = GfxMath::cross3d(vec_1,vec_2);
    }
  }
 
 
  void SimpleViewer::resetState(int /*x*/, int /*y*/){
    m_state = NONE;
    
  }
  
  void SimpleViewer::setWindowSize(int w, int h) {
    m_window_width = w;
    m_window_height = h;
    m_window_aspect = (float)w/(float)h;
  }
 
  
  void SimpleViewer::setGLMatrices(){
    //Work with the projection-matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.,m_window_aspect,2.,15.);//Set the view-perspective

    //Work with the modelview-matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    float d = m_camera_distance;
    glTranslatef(0,0,-d);//translate the objects a distance away, so that we can see it.
    
    //Rotate
    float M[16];
    m_rotation_orientation_c.fillGLMatrix(M);
    glMultMatrixf(M);
    
  }


  
  void SimpleViewer::renderDebugGraphics()
  {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-m_window_aspect, m_window_aspect, -1.0, 1.0, -1.0, 1.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    switch(m_state) {
    case ROTATING:
      glLineWidth(3.0);
      
      glColor3f(0.4, 0.4, 0.4);//set color of circle
      glBegin(GL_LINE_LOOP);
      glNormal3f(0.,0.,1.);
      for(size_t i=0; i<50; i++)
	glVertex2f(cos(2.0*M_PI*i/(float)50), sin(2.0*M_PI*i/(float)50));
      glEnd();
      
      //Draw vectors
      glBegin(GL_LINES);
      glColor3f(1.0, 0.5, 0.5);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3fv(m_rotation_axis.c_ptr());//draw rotation-axis
      
      glColor3f(0.5, 1.0, 0.5);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3fv(m_rotation_mousepos_i.c_ptr());//draw the click-position
      
      glColor3f(0.5, 0.5, 1.0);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3fv(m_rotation_mousepos_c.c_ptr()); //draw the current mouseposition
      
      glEnd();
      break;
    default:
      // Do nothing.
        break;
    }
  }
  

  Vec2f SimpleViewer::getNormalizedCoords(int x, int y){
    GfxMath::Vec2f p = GfxMath::Vec2f(x,y);
    p.normalize();
    return p;
  }
  
  Vec3f SimpleViewer::getPointOnUnitSphere(Vec2f p){
    float w = (float) m_window_width;
    float h = (float) m_window_height;
    Vec3f point = Vec3f(0,0,0);
    float x_ = p[0]/w - 0.5;
    float y_ = 0.5 - p[1]/h;
    float r  = sqrt(x_*x_ + y_*y_);
    
    if (r<0.5) {//on the sphere
      point[0] = 2*x_;
      point[1] = 2*y_;
      point[2] = sqrt(1-4*r*r);
    }else{//outside the sphere
      point[0] = x_/r;
      point[1] = y_/r;
      point[2] = 0;
    }
    return point;
  }

}// namespace GfxUtil
  
 
  
  
  
