/* $Id: TextureCube.cpp,v 1.7 2006/10/02 13:08:49 dyken Exp $
 * (C) 2005 Christopher Dyken, <dyken@cma.uio.no>
 *
 * Distributed under the GNU GPL.
 */

#include "TextureCube.hpp"
#include "Vec.hpp"
//#include "NoiseSampler.hpp"
#include <iostream>
#include <sstream>
using GfxMath::Vec3f;
using GfxMath::Vec2f;
using namespace std;

namespace GfxUtil {
    GLuint tex_name[6];
    const GLuint tex_size = 64;
    GLubyte m_tex[6][tex_size][tex_size][4];

    GLfloat
    TextureCube::m_vertices[8][3] =
	{
	    {-1.0, 1.0, 1.0},
	    {1.0, 1.0, 1.0},
	    {1.0, -1.0, 1.0},
	    {-1.0, -1.0, 1.0},
	    {-1.0, 1.0, -1.0},
	    {1.0, 1.0, -1.0},
	    {1.0, -1.0, -1.0},
	    {-1.0, -1.0, -1.0}
	};

    GLfloat  // 6 sides
    TextureCube::m_normals[6][3] =
	{
	    {0.0, 0.0, 1.0},
	    {1.0, 0.0, 0.0},
	    {0.0, 0.0,-1.0},
	    {-1.0, 0.0, 0.0},
	    {0.0, 1.0, 0.0},
	    {0.0,-1.0, 0.0}
	};

    GLfloat
    TextureCube::m_texcoords[4][2] =
	{
	    {0.0, 0.0},
	    {0.0, 1.0},
	    {1.0, 1.0},
	    {1.0, 0.0}
	};

    unsigned int //indices to place texturing right on the cube.
    TextureCube::m_i[6][4] = 
        {
	  {3,0,1,2},
	  {2,3,0,1},
	  {2,1,0,3},
	  {1,0,3,2},
	  {3,2,1,0},
	  {2,3,0,1}
        };

    unsigned int
    TextureCube::m_indices[6][4] =
	{
	    {0, 3, 2, 1},
	    {1, 2, 6, 5},
	    {5, 6, 7, 4},
	    {4, 7, 3, 0},
	    {0, 1, 5, 4},
	    {2, 3, 7, 6}
	};

    void TextureCube::initGL(int texture_function)
    { 
      // Create texture ids, set storage mode, etc...
      
      //set the lighting
      GLfloat mat_spec[] = {1.0, 1.0, 1.0, 1.0};
      GLfloat mat_shine[] = {50.0};
      GLfloat light_pos[] = {1.0, 1.0, 1.0, 0.0};
      GLfloat light[] = {1.0, 1.0, 1.0, 1.0};
      GLfloat lmod_ambient[] = {0.1, 0.1, 0.1, 0.1};
      
      
      glShadeModel(GL_SMOOTH);
      
      glMaterialfv(GL_FRONT, GL_SPECULAR, mat_spec);
      glMaterialfv(GL_FRONT, GL_SHININESS, mat_shine);
      glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
      glLightfv(GL_LIGHT0, GL_DIFFUSE, light);
      glLightfv(GL_LIGHT0, GL_SPECULAR, light);
      glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmod_ambient);
      glEnable(GL_LIGHTING);
      glEnable(GL_LIGHT0);
      
      glEnable(GL_DEPTH_TEST);

      glGenTextures(6,tex_name);
      glPixelStorei(GL_UNPACK_ALIGNMENT,1);
      generate(texture_function);
      
    }

    void TextureCube::generate(int texture_function)
    { 
      //Generate Texture:
      float d = tex_size/2.;
      //for each side, find the texture:
      for (int s = 0; s<6; s++) {
	//for each texel, find the color:
	for (uint i = 1; i<(tex_size+1);i++) {
	  for (uint j = 1;j<(tex_size+1); j++) {
	    Vec3f p;
	    if (s==0) {//front 
	      p = textureValue(texture_function,Vec3f((i-d)/d,(j-d)/d,1.));
	    }else if (s==1) {//right side
	      p = textureValue(texture_function,Vec3f(1.,(i-d)/d,(j-d)/d));
	    }else if (s==2) {//back
	      p = textureValue(texture_function,Vec3f((i-d)/d,(j-d)/d,-1.));
	    }else if (s==3) {//left side
	      p = textureValue(texture_function,Vec3f(-1.,(i-d)/d,(j-d)/d));
	    }else if (s==4) {//over
	      p = textureValue(texture_function,Vec3f((i-d)/d,1.,(j-d)/d));
	    }else if (s==5) {//under
	      p = textureValue(texture_function,Vec3f((i-d)/d,-1.,(j-d)/d));
	    }
	    m_tex[s][i-1][j-1][0] = static_cast<GLubyte>(p[0]* 255.f);
	    m_tex[s][i-1][j-1][1] = static_cast<GLubyte>(p[1]* 255.f);
	    m_tex[s][i-1][j-1][2] = static_cast<GLubyte>(p[2]* 255.f);
	    m_tex[s][i-1][j-1][3] = static_cast<GLubyte>(255.);
	    
	  }
	}
	//bind the 2D texture, and set wrapping, upload to GL
	glBindTexture(GL_TEXTURE_2D, tex_name[s]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,tex_size,tex_size,0,GL_RGBA,GL_UNSIGNED_BYTE,m_tex[s]);
	
      }
	
    }

    namespace
    {

	inline Vec3f colorCube(const Vec3f& p)
	{
	  return Vec3f((p[0]+1.)/2., (p[1]+1.)/2., (p[2]+1.)/2.);
	}

	inline Vec3f wood(const Vec3f& p)
	{
	    static NoiseSampler noise;
	    float x,y,z,r2,t;
	    x = p[0]; y = p[1]; z = p[2];
	    Vec3f p1 = Vec3f(0.6*x, 0.6*y, 0.2*z);
	    r2 = 35.0*(x*x + y*y) + 6*noise.fractalSum(p1);
	    t = (1.+sin(r2))/2.;
	    Vec3f ret = (1.- t)*Vec3f(0.5,0.5,0.1) + t*Vec3f(1.0,1.0,0.5);
	    return ret;
	}

	inline Vec3f marble(const Vec3f& p)
	{
	    static NoiseSampler noise;
	    float x,y,z,t,x1,y1,z1;
	    x = p[0]; y = p[1]; z = p[2];
	    Vec3f p1 = Vec3f((3./2.)*x,2.0*y,z);
	    t = noise.turbulence(p1);
	    x1 = sin(0.45*t); y1 = sin(1.0*t); z1 = sin(0.55*t);
	    return Vec3f(x1,y1,z1);
	}

    } // anon namespace


    Vec3f TextureCube::textureValue(int tex, const Vec3f& p)
    {
      if (tex==1) {//RGB
	return colorCube(p);
      }
      else if (tex==2) {//Wood
	return wood(p);
      }
      else if (tex==3) {//Marble
	return marble(p);
      }else {
	exit(1);
      }
    }

    void TextureCube::render()
    {
      //enable the texture
      glEnable(GL_TEXTURE_2D);
      glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
      glPushMatrix();
      glScalef(m_size,m_size,m_size);
      for (int i=0;i<6;i++) {
	//bind the right texture to the right side
	glBindTexture(GL_TEXTURE_2D,tex_name[i]);
	glBegin(GL_QUADS);
	glNormal3f(m_normals[i][0],m_normals[i][1],m_normals[i][2]);
	for (int j=0;j<4;j++){
	  glTexCoord2f(m_texcoords[m_i[i][j]][0],m_texcoords[m_i[i][j]][1]);
	  glVertex3fv(&m_vertices[m_indices[i][j]][0]);
	}
	glEnd();
      }
      glPopMatrix();
      glDisable(GL_TEXTURE_2D);
    }
}
