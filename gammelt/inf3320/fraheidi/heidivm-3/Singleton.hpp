//===========================================================================
//                                                                           
// File: Singleton.hpp                                                       
//                                                                           
// Created: Mon Sep 22 20:11:33 2008                                         
//                                                                           
// Author: Atgeirr F Rasmussen <atgeirr@sintef.no>
//                                                                           
// Revision: $Id$
//                                                                           
//===========================================================================

#ifndef SINTEF_SINGLETON_HEADER
#define SINTEF_SINGLETON_HEADER

/// Implements singleton behaviour.
/// A limit of this class is that it cannot be used to
/// implement singletons that depend on each other.
/// See for instance the discussion in Alexandrescu's
/// book "Modern C++ design."
template <class T>
class Singleton
{
public:
    /// Returns the single instance of the template argument
    /// class. Implemented using Scott Meyers' idiom.
    static T& instance()
    {
	static T instance;
	return instance;
    }
private:
    /// Hidden constructor.
    Singleton();
    /// Hidden destructor.
    ~Singleton();
    /// Hidden copy constructor.
    Singleton(Singleton const&);
    /// Hidden assignment operator.
    Singleton& operator=(Singleton const&);
};

#endif // SINTEF_SINGLETON_HEADER

