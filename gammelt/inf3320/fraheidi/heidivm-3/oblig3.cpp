//Third mandatory assignment, Heidi Vikki Munthe-Kaas
//Have copied some code from the OpenGL-book (the red one), and from the "code for excersizes" on the web-page.

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <cmath>
#include <GL/glut.h>
#include "Vec.hpp"
#include "Mat.hpp"

#include "GLApp.hpp"
#include "GLUTWrapper.hpp"
#include "OpenGLError.hpp"
#include "ColorSpace.hpp"
#include "SimpleViewer.hpp"
#include "TextureCube.hpp"
#include <boost/lexical_cast.hpp>
#include <stdexcept>

using GfxMath::Vec2f;
using GfxMath::Vec3f;
using GfxMath::Quat4f;
using GfxMath::Mat4f;

using namespace std;

int tex;
class Oblig2 : public GLApp
{
public:
  
  void initGL(){
    
    glClearColor(0.0f, 0.0f, 0.5f, 0.0f);
    glEnable(GL_CULL_FACE);
    cube_.initGL(tex);
  }
  
  void display(){
    //"clean" the buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //draw frame
    viewer_.setGLMatrices();
    //draw cube
    cube_.render();
    //draw circle and vectors
    viewer_.renderDebugGraphics();
    glutSwapBuffers();
  }
  
  //when window is reshaped, reshape everything
  void reshape(int w, int h) {
    viewer_.setWindowSize(w,h);
    viewer_.reshapeWindow();
  }
  
  //If someone presses the key q, the program should quit(exit) 
  //If 1,2,3 are pressed: RGB, wood, marble
  void keyboard(unsigned char key, int /*x*/, int /*y*/){
    
    if (key=='q'){
    std::exit(0);
    }else if (key == '1'){
      cube_.generate(1);
    }else if (key == '2'){
      cube_.generate(2);
    }else if (key == '3') {
      cube_.generate(3);
    }
  }
  
  //The mousefunction: if someone presse the right/left-button on the mouse, do:
  void mouse(int button, int state, int x, int y){
    if (state==GLUT_DOWN && button ==GLUT_LEFT_BUTTON) {
      viewer_.rotationBegin(x,y);//if left button is pressed, start rotation.
    }else {
      viewer_.resetState(x,y);//else reset state
    }
    glutPostRedisplay();
  }
  
  //When mouse is moved, do:
  void motion(int x, int y){
    viewer_.motion(x,y);
    glutPostRedisplay();
  }
  
  
  int displayMode(){
    return GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH;
  }
  
  std::string windowName(){
    return "Oblig3";
  }
  

  
  
private:
  GfxUtil::SimpleViewer viewer_;
  GfxUtil::TextureCube cube_;
};



int main(int argc, char** argv){
  if (argc!=2){
    cout<<"Wrong nr of arguments, one needed: 1, 2 or 3"<<endl;
    exit(1);
  }else {
  tex = boost::lexical_cast<int>(argv[1]);
  }
  return GLUTWrapper<Oblig2>::run(argc, argv);//run the application.
}



