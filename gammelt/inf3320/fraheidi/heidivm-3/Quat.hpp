/* $Id: Quat.h,v 1.2 2006/08/15 16:16:43 dyken Exp $
 *
 * (C) 2005 Christopher Dyken, <dyken@cma.uio.no>
 *
 * Distributed under the GNU GPL.
 */
#ifndef _QUAT_H_
#define _QUAT_H_

#include "Vec.hpp"

namespace GfxMath {

template<class type>
class Quat
{
public:

	/** Create a unit quaternion representing the rotation of angle around axis. */
	static Quat
	axisAngleRotation(Vec<type,3>& axis, type angle)
	{
		type c = cos(0.5*angle);
		type s = sin(0.5*angle);
		return Quat(c, s*axis[0], s*axis[1], s*axis[2]);
	}

	/** Create a unit quaternion representing the great-circle rotation from a to b. */
	static Quat
	greatCircleRotation(Vec<type,3> a, Vec<type,3> b)
	{
		Vec<type,3> axis = cross3d(a, b);
		try {
			axis.normalizeChecked();

			type angle = acos(dot(a,b));
			type c = cos(0.5*angle);
			type s = sin(0.5*angle);
			return Quat(c, s*axis[0], s*axis[1], s*axis[2]);
		} catch (std::runtime_error &e) {
			return Quat(1.0, 0.0, 0.0, 0.0);
		}
	}

	/** Default constructor. */
	Quat()
	{
		m_q[0] = 1.0;
		m_q[1] = 0.0;
		m_q[2] = 0.0;
		m_q[3] = 0.0;
	}		

	/** Constructor. */
	Quat(type r, type x, type y, type z)
	{
		m_q[0] = r;
		m_q[1] = x;
		m_q[2] = y;
		m_q[3] = z;
	}

	/** Fill a raw C array of 16 values with the OpenGL matrix corresponding to the rotation defined by the quaternion. */
	void
	fillGLMatrix(type *m)
	{
		type xx = m_q[1]*m_q[1];
	    type xy = m_q[1]*m_q[2];
	    type xz = m_q[1]*m_q[3];
	    type xw = m_q[1]*m_q[0];
	    type yy = m_q[2]*m_q[2];
	    type yz = m_q[2]*m_q[3];
	    type yw = m_q[2]*m_q[0];
	    type zz = m_q[3]*m_q[3];
	    type zw = m_q[3]*m_q[0];

	    m[ 0] = 1.0-2.0*(yy+zz);
	    m[ 1] =     2.0*(xy+zw);
	    m[ 2] =     2.0*(xz-yw);

	    m[ 4] =     2.0*(xy-zw);
	    m[ 5] = 1.0-2.0*(xx+zz);
	    m[ 6] =     2.0*(yz+xw);

	    m[ 8] =     2.0*(xz+yw);
	    m[ 9] =     2.0*(yz-xw);
	    m[10] = 1.0-2.0*(xx+yy);

	    m[ 3] = 0.0;
	    m[ 7] = 0.0;
	    m[11] = 0.0;
	    m[12] = 0.0;
	    m[13] = 0.0;
	    m[14] = 0.0;
	    m[15] = 1.0;
	}

	/** Access to elements. */
	type&
	operator[](unsigned int i)
    {
    	return m_q[i];
    }

	/** Const-access to elements. */
    const type&
    operator[](size_t i) const
    {
    	return m_q[i];
    }

protected:
	type m_q[4];		///< Internal representation: r, x, y, z.
	
};

/** Quaternion multiplicatrion. */
template<class type>
Quat<type>
operator*(const Quat<type>& a, const Quat<type>& b)
{
	return Quat<type>(a[0]*b[0] - (a[1]*b[1] + a[2]*b[2] + a[3]*b[3]),
					  a[0]*b[1] + b[0]*a[1] + a[2]*b[3] - b[2]*a[3],
					  a[0]*b[2] + b[0]*a[2] + b[1]*a[3] - a[1]*b[3],
					  a[0]*b[3] + b[0]*a[3] + a[1]*b[2] - b[1]*a[2]);
}

typedef Quat<float>  Quat4f;
typedef Quat<double> Quat4d;

} // namespace GfxMath
#endif //_QUAT_H_
