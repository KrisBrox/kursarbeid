//===========================================================================
//                                                                           
// File: GLApp.hpp                                                           
//                                                                           
// Created: Mon Sep 22 18:36:49 2008                                         
//                                                                           
// Author: Atgeirr F Rasmussen <atgeirr@sintef.no>
//                                                                           
// Revision: $Id$
//                                                                           
//===========================================================================

#ifndef SINTEF_GLAPP_HEADER
#define SINTEF_GLAPP_HEADER

#include "Vec.hpp"
#include <string>
#include <GL/glut.h>

/// @brief A base class suitable for use with GLUTWrapper.
class GLApp
{
public:

    /// A virtual destructor is necessary,
    /// so that any inherited class destructor
    /// will be called through the instance
    /// pointer. Override if you claim any
    /// non-automatic resources in the class.
    virtual ~GLApp()
    {
    }
    
    /// Override init to do your own setup.
    /// Do not call OpenGL functions here, since
    /// at this point the GL context does not exist.
    /// Put OpenGL init in initGL() instead.
    virtual bool init(int, char**)
    {
	return true;
    }
    /// Override init to do your own GL setup.
    /// This function will be called after
    /// GLUT has created an OpenGL context.
    virtual void initGL()
    {
    }

    /// Override to specify a different GL display mode.
    virtual int displayMode()
    {
	return GLUT_DOUBLE | GLUT_RGB;
    }
    /// Override to specify a different default window size.
    virtual GfxMath::Vec<int, 2> initWindowSize()
    {
	return GfxMath::Vec<int, 2>(512, 512);
    }
    /// Override to specify a different window name.
    virtual std::string windowName()
    {
	return "OpenGL application";
    }

    // ---- Callback overrides ----

    /// Override to customize display.
    virtual void display()
    {
    }
    /// Override to customize actions on window
    /// resizing.
    virtual void reshape(int, int)
    {
    }
    /// Override to do your own keyboard processing.
    virtual void keyboard(unsigned char, int, int)
    {
    }
    /// Override to do your own mouse button processing.
    virtual void mouse(int, int, int, int)
    {
    }
    /// Override to do your own mouse drag processing.
    virtual void motion(int, int)
    {
    }
};

#endif // SINTEF_GLAPP_HEADER
