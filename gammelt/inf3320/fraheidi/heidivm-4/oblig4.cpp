//Fourth mandatory assignment, Heidi Vikki Munthe-Kaas
//Have copied some code from the OpenGL-book (the red one), and from the "code for excersizes" on the web-page, and a little from the lecture notes.

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <iostream>
#include <stdlib.h>
#include "Mat.hpp"

#include "GLApp.hpp"
#include "GLUTWrapper.hpp"
#include "OpenGLError.hpp"
#include "ColorSpace.hpp"
#include "SimpleViewer.hpp"
#include "TriMesh.hpp"
#include <boost/lexical_cast.hpp>
#include <stdexcept>

using namespace std;
using namespace GfxUtil;

string filename;
int mode;
class Oblig2 : public GLApp
{
public:
  
  void initGL(){
    mode = 1;
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    viewer_ = GfxUtil::SimpleViewer();
    mesh_ = GfxUtil::TriMesh(filename);
    
    
  }
  
  void display(){
    //"clean" the buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //draw frame
    viewer_.setGLMatrices();
   
    //draw something(begin with wireframe, use keyboard to switch framemodes
    switch(mode) { 
    case 1:
      mesh_.render(mesh_.WIREFRAME);
      break;
    case 2:
      mesh_.render(mesh_.HIDDEN_LINE);
      break;
    case 3:
      mesh_.render(mesh_.FLAT);
      break;
    case 4:
      mesh_.render(mesh_.SMOOTH);
      break;
    }
      
    //draw circle and vectors
    viewer_.renderDebugGraphics();
    glutSwapBuffers();
    
  }
  
  //when window is reshaped, reshape everything
  void reshape(int w, int h) {
    viewer_.setWindowSize(w,h);
    viewer_.reshapeWindow();
  }
  
  //If someone presses the key q, the program should quit(exit).
  void keyboard(unsigned char key, int /*x*/, int /*y*/){
    switch(key) {
    case 'q':
      std::exit(0);
      break;
    case '1':
      mode = 1;
      break;
    case '2':
      mode = 2;
      break;
    case '3':
      mode = 3;
      break;
    case '4':
      mode = 4;
      break;
    }
  }
  
  //The mousefunction: if someone presse the right/left-button on the mouse, do:
  void mouse(int button, int state, int x, int y){
    if (state==GLUT_DOWN && button ==GLUT_LEFT_BUTTON) {
      viewer_.rotationBegin(x,y);//if left button is pressed, start rotation.
    }else {
      viewer_.resetState(x,y);//else reset state
    }
    glutPostRedisplay();
  }
  
  //When mouse is moved, do:
  void motion(int x, int y){
    viewer_.motion(x,y);
    glutPostRedisplay();
  }
  
  
  int displayMode(){
    return GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH;
  }
  
  std::string windowName(){
    return "Oblig4";
  }
  

  
  
private:
  GfxUtil::SimpleViewer viewer_;
  GfxUtil::TriMesh mesh_;
};



int main(int argc, char** argv){
  if (argc!=2) {
    cout<<"Wrong nr of arguments, one needed. Give a filename for a mesh-file"<<endl;
    exit(1);
  }else {
    filename = argv[1];
  }
  return GLUTWrapper<Oblig2>::run(argc, argv);//run the application.
}



