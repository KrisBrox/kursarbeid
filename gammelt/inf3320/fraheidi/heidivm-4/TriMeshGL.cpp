/* -*- mode: C++; tab-width:4; c-basic-offset: 4; indent-tabs-mode:nil -*-
 *
 * Time-stamp: <2005-07-13 12:01:05 dyken>
 *
 * (C) 2005 Christopher Dyken, <dyken@cma.uio.no>
 *
 * Distributed under the GNU GPL.
 */

#include "TriMesh.hpp"

#include <stdexcept>
#include <vector>
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glu.h>

using std::string;
using std::vector;
using std::cout;
using std::endl;
using GfxMath::Vec3f;


namespace GfxUtil {
    
    void TriMesh::computeNormals() {
        //Triangle normal vectors
        for(size_t i=0; i<m_triangles.size(); i++) {
            Triangle* t = m_triangles[i];
            Vec3f p0 = t->getNode(0)->m_pos;
            Vec3f p1 = t->getNode(1)->m_pos;
            Vec3f p2 = t->getNode(2)->m_pos;
            Vec3f A = p1-p0; Vec3f B = p2-p1;
            t->m_N = cross3d(A,B);
            t->m_N.normalize();
        }
        
        //Vertex normal vectors
        for(size_t i=0; i<m_nodes.size(); i++) {
            Node* n = m_nodes[i];
            HalfEdge* he = n->getLeadingHalfEdge();
            HalfEdge* leadhe = he;//leading half edge
            Triangle* tri;
            Vec3f N;
            int d = 0;
            do{//compute the average of the of the triangle normal vectors
                tri = he->getTriangle();
                N += tri->m_N;
                he = he->getVtxRingPrev();
                d++;
            } while(he !=NULL && he != leadhe);
            N = (1./d)*N;
            N.normalize();
            n->m_N = N;
        }
        
        
    }

    
    void TriMesh::renderEdges() {
        glLineWidth(1.);
        glBegin(GL_LINES);
        for (size_t i = 0; i<m_halfedges.size(); i++) {
            HalfEdge* he = m_halfedges[i];
            if(he->getTwin()==NULL||he< (he->getTwin())) {
                Vec3f s = he->getSourceNode()->m_pos;
                Vec3f d = he->getDestinationNode()->m_pos;
                glVertex3f(s[0],s[1],s[2]);
                glVertex3f(d[0],d[1],d[2]);
            }
        }
        glEnd();
    }
    
    void TriMesh::renderTrianglesFlat() {   
        glBegin(GL_TRIANGLES);
        for(size_t i = 0; i<m_triangles.size(); i ++) {
            Triangle* t = m_triangles[i];
            HalfEdge* he = t->m_he;
            Vec3f& N = t->m_N;//trianglenormal
            glNormal3f(N[0],N[1],N[2]);
            for (size_t j = 0; j<3; j++) {
                Vec3f& pos = he->getSourceNode()->m_pos;
                glVertex3f(pos[0],pos[1],pos[2]);
                he = he->getNext();
            }
        }
        glEnd();
        
    }
    
    void TriMesh::renderTrianglesSmooth() {
        glBegin(GL_TRIANGLES);
        for(size_t i = 0; i<m_triangles.size(); i++) {
            Triangle* t = m_triangles[i];
            HalfEdge* he = t->getLeadingHalfEdge();
            for(size_t j = 0; j<3; j++) {
                Node* n = he->getSourceNode();
                Vec3f& N = n->m_N;//vertexnormal
                Vec3f& pos = n->m_pos;
                glNormal3f(N[0],N[1],N[2]);
                glVertex3f(pos[0],pos[1],pos[2]);
                he = he->getNext();
            }
        }
        glEnd();
        
    }
    
    void TriMesh::render(RenderMode mode) {   
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        //Boundingbox
        //Find the max length of the sides:
        float x,y,z,max;
        x = m_bbox_max[0]-m_bbox_min[0];
        y = m_bbox_max[1]-m_bbox_min[1];
        z = m_bbox_max[2]-m_bbox_min[2];
        if (x>y) {
            if (x>z){
                max = x;
            }else{//z>=x, x>y
                max = z;
            }
        }else if (y>z) {
            //x<=y & z<y: 
            max = y;
        }else {
            //x<=y, y<=z;
            max = z;
        }
        glScalef(2./max,2./max,2./max);//scale the current matrix.
        glutWireCube(max);//make a cube around the object
        
        
        glPushMatrix();
        switch(mode) {//depending on which rendermode:
        case WIREFRAME:
            glColor3f(1.,1.,1.);
            renderEdges();
            break;
        case HIDDEN_LINE:
            glEnable(GL_POLYGON_OFFSET_FILL);
            glPolygonOffset(1.,1.);
            glColor3f(0.0, 0.0, 0.0);
            renderTrianglesFlat();
            glDisable(GL_POLYGON_OFFSET_FILL);
            glColor3f(1.,1.,1.);
            renderEdges();
            break;
        case FLAT:
            glShadeModel(GL_FLAT);
            glEnable(GL_LIGHTING);
            glEnable(GL_LIGHT0);
            glPolygonOffset(1.,1.);
            glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE );
            glEnable(GL_COLOR_MATERIAL);
            glColor3f(0.6, 0.6, 0.8);
            renderTrianglesFlat();
            glDisable(GL_COLOR_MATERIAL);
            glDisable(GL_LIGHTING);
            break;
        case SMOOTH:
            glShadeModel(GL_SMOOTH);
            glEnable(GL_LIGHTING);
            glEnable(GL_LIGHT0);
            glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
            glEnable(GL_COLOR_MATERIAL);
            glColor3f(0.6, 0.6, 0.8);
            renderTrianglesSmooth();
            glDisable(GL_COLOR_MATERIAL);
            glDisable(GL_LIGHTING);
            break;
        } 
        glPopMatrix();
        
    }
    
}
    
