/* -*- mode: C++; tab-width:4; c-basic-offset: 4; indent-tabs-mode:nil -*-
 *
 * Time-stamp: <2005-07-13 12:01:05 dyken>
 *
 * (C) 2005 Christopher Dyken, <dyken@cma.uio.no>
 *
 * Distributed under the GNU GPL.
 */

#include "Vec.hpp"
#include "TriMesh.hpp"

#include <stdexcept>
#include <fstream>
#include <vector>
#include <algorithm>
#include <list>
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glu.h>

using std::runtime_error;
using std::fstream;
using std::string;
using std::vector;
using GfxMath::Vec3f;


namespace GfxUtil {
    
    TriMesh::TriMesh()
    {
    }
    
    TriMesh::TriMesh(const string& filename) {
        
        readMesh(filename);
        //set lighting
        GLfloat mat_spec[] = {1.0, 1.0, 1.0, 1.0};
        GLfloat mat_shine[] = {50.0};
        GLfloat light_pos[] = {1.0, 1.0, 1.0, 0.0};
        GLfloat light[] = {1.0, 1.0, 1.0, 1.0};
        GLfloat lmod_ambient[] = {0.1, 0.1, 0.1, 0.1};
        
        glMaterialfv(GL_FRONT, GL_SPECULAR, mat_spec);
        glMaterialfv(GL_FRONT, GL_SHININESS, mat_shine);
        glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, light);
        glLightfv(GL_LIGHT0, GL_SPECULAR, light);
        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmod_ambient);
        
    }
    
    TriMesh::TriMesh(const vector<Vec3f>& points, const vector<int>& indices){
        buildTriangulation(points, indices);
    }
    
    void TriMesh::buildTriangulation(const vector<Vec3f>& points, const vector<int>& indices){
        // create nodes
        for(size_t i=0; i<points.size(); i++) {
            m_nodes.push_back(new Node(points[i]));
        }
        
        // create triangles and half-edges
        for(size_t j=0; j<indices.size()/3; j++) {
            HalfEdge* he[3];
            for(size_t i=0; i<3; i++) {
                he[i] = new HalfEdge(m_nodes[indices[3*j+i]], NULL, NULL, NULL);
                m_halfedges.push_back(he[i]);
            }
            Triangle* tri = new Triangle(he[0]);
            m_triangles.push_back(tri);
            
            for(size_t i=0; i<3; i++) {
                he[i]->m_next =	he[(i+1)%3];
                he[i]->m_face = tri;
                if(he[i]->m_from->m_he == NULL)
                    he[i]->m_from->m_he = he[i];
            }
        }
        
        // stitch together neighbouring half-edges and give leading half edge.
        buildConnectivity();
        
        calcBBox();
        computeNormals();
    }
    
    //Couldn`t find what was wrong here, but it created segfaults
    //TriMesh::~TriMesh()
    //{
	//for(size_t i=0; i<m_nodes.size(); i++) delete m_nodes[i];
	//for(size_t i=0; i<m_triangles.size(); i++) delete m_triangles[i];
	//for(size_t i=0; i<m_halfedges.size(); i++) delete m_halfedges[i];	
    //}
    
    void TriMesh::readMesh(const std::string& filename){
        fstream in(filename.c_str(), std::ios::in);
        if(!in.good())
            throw runtime_error("Error reading from " + filename);
        
        size_t Nv, Nt;
        in >> Nv >> Nt;
        
        vector<Vec3f> points;
        for(size_t i=0; i<Nv; i++) {
            float x, y, z;
            in >> x >> y >> z;
            points.push_back(Vec3f(x,y,z));
        }
        
        vector<int> indices;
        for(size_t i=0; i<3*Nt; i++) {
            int ix;
            in >> ix;
            indices.push_back(ix);
        }
        in.close();
        
        buildTriangulation(points, indices);
    }
    
    struct TriMesh::SortElement {
        SortElement(HalfEdge *he) {
            m_he = he;
            m_a = m_he->m_from;
            m_b = m_he->m_next->m_from;
            if(m_b < m_a) {
                Node *t = m_a; m_a = m_b; m_b=t;
            }
        }
        
        friend bool operator<(const SortElement &a, const SortElement &b) {
            if(a.m_a == b.m_a)
                return a.m_b < b.m_b;
            return a.m_a < b.m_a;
        }
        
        Node *m_a;
        Node *m_b;
        HalfEdge *m_he;	
    };
    
    void TriMesh::buildConnectivity() {
        //Finds the connectivity of the mesh, that is, finds twin half-edges.
        vector<SortElement> helper;//helper list
        for (size_t i=0; i<m_halfedges.size(); i++) {
            HalfEdge* he = m_halfedges[i];
            //give all the nodes a leading half edge
            he->getSourceNode()->m_he = he;
            helper.push_back(SortElement(he));//put all half edges into the helper list
        }
        //Sort helperlist using operator<, which is defined in the SortElement struct
        std::sort(helper.begin(), helper.end());
        
        for (size_t j=0; j<helper.size(); j++) {
            size_t i;
            for (i=j; i<helper.size()-1;i++) {
                if(helper[i].m_a!=helper[i+1].m_a || helper[i].m_b!= helper[i+1].m_b) {
                    break;
                }
            }
            if (i==j){//helper[j] points to boundary edge
                helper[j].m_he->m_twin = NULL;
            }else if (i==j+1) {//connect the triangles of helper[i] and helper[j]
                helper[i].m_he->m_twin = helper[j].m_he;
                helper[j].m_he->m_twin = helper[i].m_he;
            }else {//j..i-1 are non-manifold edges
                for(size_t k = j; k<i; k++) {
                    helper[k].m_he->m_twin = NULL;
                }
            }
            j=i;
        }
        
        
    }
    
    void TriMesh::calcBBox() {
        if(m_nodes.size() == 0)
            return;
        m_bbox_min = m_bbox_max = m_nodes[0]->m_pos;
        for(size_t j=1; j<m_nodes.size(); j++) {
            const Vec3f& p = m_nodes[j]->m_pos;
            
            for(size_t i=0; i<3; i++) {
                m_bbox_min[i] = p[i] < m_bbox_min[i] ? p[i] : m_bbox_min[i];
                m_bbox_max[i] = p[i] > m_bbox_max[i] ? p[i] : m_bbox_max[i];
            }
        }
    }
    
}
