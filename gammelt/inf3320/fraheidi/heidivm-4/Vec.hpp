/* $Id: Vec.h,v 1.8 2006/10/02 13:08:57 dyken Exp $
 *
 * (C) 2005 Christopher Dyken, <dyken@cma.uio.no>
 *
 * Distributed under the GNU GPL.
 */

#ifndef _VEC_HPP_
#define _VEC_HPP_

//#include <iosfwd>
#include <iostream>
#include <boost/static_assert.hpp>
#include <cmath>
#include <stdexcept>
#include <limits>

namespace GfxMath {

/** Vector template.
 * \author Christopher Dyken <dyken@cma.uio.no>
 * 
 * Template arguments are type T and dimension dim. Some typedefs, Vec2d,
 * Vec2f, etc. are included for convenience.
 *
 */

template<class type, unsigned int dim>
class Vec
{
public:
	typedef type	       value_type;
	typedef unsigned int   size_type;

	/** General constructor, initializes all elements to 0. */
	Vec()
	{
		// skip
		for(unsigned int i=0; i<dim; i++)
			m_data[i] = static_cast<type>(0);
		// unskip
	}

	/** 2D init constructor. */
	Vec(type x, type y)
	{
		BOOST_STATIC_ASSERT(dim == 2);
		// skip
		m_data[0]=x;
		m_data[1]=y;
		// unskip
	}

	/** 3D init constructor. */
	Vec(type x, type y, type z)
	{
    	BOOST_STATIC_ASSERT(dim == 3);
		// skip
		m_data[0]=x;
		m_data[1]=y;
		m_data[2]=z;
		// unskip
	}

	/** 4D init constructor */
	Vec(type x, type y, type z, type w)
	{
		BOOST_STATIC_ASSERT(dim == 4);
		// skip
		m_data[0]=x;
		m_data[1]=y;
		m_data[2]=z;
		m_data[3]=w;
		// unskip
	}

	/** Get raw c-pointer to internal data. */
	type*
	c_ptr()
	{
		return &m_data[0];
	}

	/** Get raw const c-pointer to internal data. */
	const type*
	c_ptr() const
	{
		return &m_data[0];
	}
        
	/** Get an element of the vector. */
	type&
	operator[](size_type i)
	{
#ifdef DEBUG
		if( dim <= i )
			throw std::out_of_range( __PRETTY_FUNCTION__ );
#endif
		return m_data[i];
	}

	/** Const-access operator to vector elements. */
	const type&
	operator[](size_type i) const
	{
#ifdef DEBUG
		if( dim <= i )
			throw std::out_of_range( __PRETTY_FUNCTION__ );
#endif
		return m_data[i];
	}

	/** The squared length of the vector */
	type
	lengthSquared() const
	{
		type ret = static_cast<type>(0);
		// skip
		for(size_type i=0; i<dim; ++i)
			ret	+= m_data[i]*m_data[i];
		// unskip
		return ret;
	}

	/** Normalizes vector, silently ignores if zero-vector */
	void
	normalize()
	{
		// skip
		type len = length();
		len = len < std::numeric_limits<type>::epsilon() ? 1.0 : len;
		scale(1.0/len);
		// unskip
	}

	/** Normalizes vector, throws an exception if zero-vector. */
	void
	normalizeChecked()
	{
		// skip
		type len = length();
		if(len < std::numeric_limits<type>::epsilon())
			throw std::runtime_error("Normalizing degenerate vector.");
		scale(1.0/len);
		// unskip
	}

	/** Scale by scalar. */
	void
	scale(type s)
	{
		// skip
		for(size_type i=0; i<dim; i++)
			m_data[i] *= s;
		// unskip
	}


	
	/** The length of the vector */
	type
	length() const
	{
		type ret;
		// skip
		ret = sqrt(lengthSquared());
		// unskip
		return ret;
	}
    
protected:
    type	m_data[dim];
};

/** Dot product */
template<class type, unsigned int dim>
type
dot(const Vec<type,dim>&a, const Vec<type,dim>& b)
{
	type ret = static_cast<type>(0);
	// skip
	for(unsigned int i=0; i<dim; i++)
		ret += a[i]*b[i];
	// unskip
	return ret;
}

/** Scale a vector by a scalar. */
template<class type, unsigned int dim>
Vec<type,dim>
operator*( float k, const Vec<type,dim>& v )
{
	Vec<type, dim> ret;
	// skip
	for(unsigned int i=0; i<dim; ++i)
		ret[i] = k * v[i];
	// unskip
	return ret;
}

/** Returns the Euclidean distance between two points. */
template<class type, unsigned int dim>
type
euclideanDistance( const Vec<type,dim>&a, const Vec<type,dim>& b )
{
	type ret;
	// skip
	type sum = 0;
		for(unsigned int i=0; i<dim; i++) {
			sum += (b[i]-a[i])*(b[i]-a[i]);
		}
	ret = sqrt(sum);
	// unskip
	return ret;
}

/** Linear interpolation between two points. */
template<class type, unsigned int dim>
Vec<type, dim>
lerp(const Vec<type,dim>& a, const Vec<type,dim>& b, type v)
{
	Vec<type, dim> ret;
	// skip
	for(unsigned int i=0; i<dim; ++i)
		ret[i] = (static_cast<type>(1)-v)*a[i] + v*b[i];
	// unskip
	return ret;
}

/** Subtracts two vectors. */
template<class type, unsigned int dim>
Vec<type,dim>
operator-( const Vec<type,dim>& a, const Vec<type,dim>& b )
{
	Vec<type, dim> ret;
	// skip
	for(unsigned int i=0; i<dim; ++i)
		ret[i] = a[i] - b[i];
	// unskip
	return ret;
}

/** Negates a vector. */
template<class type, unsigned int dim>
Vec<type,dim>
operator-( const Vec<type,dim>& v )
{
	Vec<type, dim> ret;
	// skip
	for(unsigned int i=0; i<dim; ++i)
		ret[i] = -v[i];
	// unskip
	return ret;
}


/** Adds two vectors. */
template<class type, unsigned int dim>
Vec<type,dim>
operator+( const Vec<type,dim>& a, const Vec<type,dim>& b )
{
	Vec<type, dim> ret;
	// skip
	for(unsigned int i=0; i<dim; ++i)
		ret[i] = a[i] + b[i];
	// unskip
	return ret;
}


/** Adds rhs vector to lhs vector. */
template<class type, unsigned int dim>
Vec<type,dim>&
operator+=( Vec<type,dim>& a, const Vec<type,dim>& b )
{
	// skip
	for(unsigned int i=0; i<dim; ++i)
		a[i] += b[i];
	// unskip
	return a;
}


/** Calculates the 2D cross product of two 2D vectors (i.e., the area of the parallelpiped spanned by the two vectors) */
template<class type>
type
cross2d(const Vec<type,2>& a, const Vec<type,2> &b)
{
	type ret;
	// skip
	ret = a[0]*b[1] - a[1]*b[0];
	// unskip
	return ret;
}

/** Calculates the cross product of two 3D vectors. */
template<class type>
Vec<type,3>
cross3d(const Vec<type,3>& a, const Vec<type,3> &b)
{
	Vec<type,3> ret;
	// skip
	ret[0] = a[1]*b[2] - a[2]*b[1];
	ret[1] = a[2]*b[0] - a[0]*b[2];
	ret[2] = a[0]*b[1] - a[1]*b[0];
	// unskip
	return ret;
}

/** Finds the orientation of a 2D triangle. */
template<class type>
type
orient2d(const Vec<type,2>& a, const Vec<type,2>& b, const Vec<type,2>& c)
{
	type ret;
	// skip
	ret = cross2d(a-c, b-c);
	// unskip
	return ret;
}

/** Dumps the contents of a vector to a stream. */
template<class type, unsigned int dim>
std::ostream&
operator<<(std::ostream& os, const Vec<type,dim> &v)
{
	// skip
    os << '[';
    for(unsigned int i=0; i<dim-1; i++)
        os << v[i] << ", ";
    os << v[dim-1] << ']';
	// unskip
	return os;
}

typedef Vec<float,2>  Vec2f; ///< Convenience typedef for Vec <float,2>.
typedef Vec<double,2> Vec2d; ///< Convenience typedef for Vec <double,2>.
typedef Vec<int,2>    Vec2i; ///< Convenience typedef for Vec <int, 2>
typedef Vec<float,3>  Vec3f; ///< Convenience typedef for Vec <float,3>.
typedef Vec<double,3> Vec3d; ///< Convenience typedef for Vec <double,3>.

} // namespace GfxMath

#endif //_VEC_HPP_
