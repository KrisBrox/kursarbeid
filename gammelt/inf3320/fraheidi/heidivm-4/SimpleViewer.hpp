/* -*- mode: C++; tab-width:4; c-basic-offset: 4; indent-tabs-mode:nil -*-
 *
 * Time-stamp: <2005-07-13 12:01:05 dyken>
 *
 * (C) 2005 Christopher Dyken, <dyken@cma.uio.no>
 *
 * Distributed under the GNU GPL.
 */

#ifndef GFXUTIL_SIMPLEVIEWER_H
#define GFXUTIL_SIMPLEVIEWER_H

#include <GL/gl.h>
#include "Vec.hpp"
#include "Quat.hpp"

namespace GfxUtil {

class SimpleViewer
{

public:
	SimpleViewer();
	void rotationBegin(int x, int y);
	void panBegin(int x, int y);
	void zoomBegin(int x, int y);
	void resetState(int x, int y);

	void motion(int x, int y);
	void setWindowSize(int w, int h);
	void setGLMatrices();
	void renderDebugGraphics();

	void setViewVolume(GfxMath::Vec3f origin, float radius);
    void reshapeWindow();

private:

	GfxMath::Vec2f getNormalizedCoords(int x, int y);
	GfxMath::Vec3f getPointOnUnitSphere(GfxMath::Vec2f p);

	enum { NONE, ROTATING, PANNING, ZOOMING } m_state;
	int   m_window_width;  //< Width of window
	int   m_window_height; //< Height of window
	float m_window_aspect; //< Aspect ratio of window.


	// View volume
	GfxMath::Vec3f m_viewvolume_origin; 
	float          m_viewvolume_radius;
	float          m_viewvolume_epsilon;  //< Small value used for epsilon tests.

	float           m_camera_distance;    //< Distance from pivot point along local -Z.

	GfxMath::Vec2f  m_unit_mousepos_i;
	GfxMath::Vec2f  m_unit_mousepos_c;


	// rotation state data:
    GfxMath::Quat4f m_rotation_orientation_c; ///< Current orientation.
	GfxMath::Quat4f m_rotation_orientation_i; ///< Orientation when mousebutton was pressed.
	GfxMath::Vec3f  m_rotation_axis;          ///< Currently defined rotation axis.
	float           m_rotation_angle;         ///< Currently defined rotation angle.
	GfxMath::Vec3f  m_rotation_mousepos_i;    ///< Mouseposition when mousebutton was pressed.
	GfxMath::Vec3f  m_rotation_mousepos_c;    ///< Current mouseposition.    

	// pan state data:
	GfxMath::Vec3f  m_pan_position_i;

	// zoom state data
	float           m_zoom_distance_i;

};

}
#endif
