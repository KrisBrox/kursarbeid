/* -*- mode: C++; tab-width:4; c-basic-offset: 4; indent-tabs-mode:nil -*-
 *
 * Time-stamp: <2005-07-13 12:01:05 dyken>
 *
 * (C) 2005 Christopher Dyken, <dyken@cma.uio.no>
 *
 * Distributed under the GNU GPL.
 */

#ifndef GFXUTIL_TRIMESH_H
#define GFXUTIL_TRIMESH_H

#include <vector>
#include <string>
#include "Vec.hpp"

namespace GfxUtil
{

/** Triangulation data structure using half-edges. */
class TriMesh
{
public:
	struct Node;
	struct Triangle;
	struct HalfEdge;

	/** Different render modes. */
	enum RenderMode { WIREFRAME, HIDDEN_LINE, FLAT, SMOOTH };

	/** Default constructor. */
	TriMesh();

	/** Constructor reading from a *.msh-file. */
	TriMesh(const std::string &filename);

	/** Constructor from a list of points and triangle indices. */
	TriMesh(const std::vector<GfxMath::Vec3f> &points, const std::vector<int> &triangles);

	/** Destructor. */
	//~TriMesh();

    /** Reads a mesh from a .msh-file. Call this if you used the default constructor. */
    void readMesh(const std::string& filename);

	/** Returns the minimum x,y,z-values of the vertices. */
	const GfxMath::Vec3f& getBBoxMin() const { return m_bbox_min; }

	/** Returns the maximum x,y,z-values of the vertices. */
	const GfxMath::Vec3f& getBBoxMax() const { return m_bbox_max; }

	/** Finds suitable normal vectors for triangles
     *  and vertices. */
	void computeNormals();

	/** Renders the mesh according to the given RenderMode. */
	void render(RenderMode mode);

    /** Renders the edges of the mesh. */
	void renderEdges();

    /** Renders the triangles of the mesh with flat shading. */
	void renderTrianglesFlat();

    /** Renders the triangles of the mesh with smooth shading. */
	void renderTrianglesSmooth();

	/** Refines the mesh one step using Loop-subdivision. */
	TriMesh* subdivideLoop();

	/** Refines the mesh one step using \sqrt{3}-subdivision. */
	TriMesh* subdivideSqrt3();

protected:
	struct SortElement; ///< Helper struct for building connectivity.

	/** Builds the data structure from an indexed point set. */
	void buildTriangulation(const std::vector<GfxMath::Vec3f>& points,
	                        const std::vector<int>& indices);

	/** Calculates bounding-box. */
	void calcBBox();

	/** Finds the connectivity of the mesh, that is,
     *  finds twin half-edges. */
	void buildConnectivity();

	GfxMath::Vec3f         m_bbox_min;    ///< Minimum values of bounding box.
	GfxMath::Vec3f         m_bbox_max;    ///< Maximum values of bounding box.

	std::vector<Node*>	   m_nodes;       ///< The nodes of this mesh.
	std::vector<Triangle*> m_triangles;   ///< The triangles of this mesh.
	std::vector<HalfEdge*> m_halfedges;   ///< The half-edges of this mesh.
};


/** Struct containing per half-edge data. */
struct TriMesh::HalfEdge
{
	/** Constructor.
	 * \param from The source node of the triangle.
	 * \param next The next half-edge in triangle loop.
	 * \param face The triangle of which this half-edge belongs.
	 * \param twin The other half-edge of this edge, or NULL if this half-edge
	 *             is on the boundary.
	 */
	HalfEdge(Node *from, Triangle *face, HalfEdge *next, HalfEdge *twin)
	: m_from(from), m_face(face), m_next(next), m_twin(twin) { }

	/** Returns true if half-edge is part of a boundary edge. */
	bool isBoundary()
	{
		return m_twin == NULL;
	}

	/** Get next half-edge in triangle loop. */
	HalfEdge* getNext() {
		return m_next;
	}

	/** Get twin half-edge in neighbouring triangle. */
	HalfEdge* getTwin() {
		return m_twin;
	}

	/** Returns the previous half-edge in the triangle loop.
	 * \note Assumes that triangles are used. */
	HalfEdge* getPrev()
	{
		return m_next->m_next;
	}

	/** Returns the next half-edge from the same node in a counter-clockwise order.
	 * 
	 * This function  is used to iterate over all the connecting edges or
	 * neighbouring vertices of a vertex.
	 */
	HalfEdge* getVtxRingNext()
	{
		return getPrev()->m_twin;
	}

	/** Returns the next half-edge from the same node in a clockwise order.
	 * 
	 * This function  is used to iterate over all the connecting edges or
	 * neighbouring vertices of a vertex.
	 */
	HalfEdge* getVtxRingPrev()
	{
		if(m_twin == NULL)
			return NULL;
		return m_twin->m_next;
	}

	/** Returns the triangle of the half-edge. */
	Triangle* getTriangle()
	{
		return m_face;
	}

	/** Returns the source node of this half-edge. */
	Node* getSourceNode()
	{
		return m_from;
	}

	/** Returns the destination node of this half-edge. */
	Node* getDestinationNode()
	{
		return m_next->m_from;
	}
			
	Node*     m_from; ///< The source node of this half-edge.
	Triangle* m_face; ///< The triangle of this half-edge.
	HalfEdge* m_next; ///< The next half-edge of this triangle.
	HalfEdge* m_twin; ///< The other half-edge of this edge.

	size_t    m_ix;
};

/** Struct containing per node data. */
struct TriMesh::Node
{
	/** Constructor.
	 * \param p The geometric positin of the node. */
	Node(const GfxMath::Vec3f &p)
	: m_pos(p), m_he(NULL)
	{}

	/** Returns one half-edge from the node, if node is on boundary,
	 *  it returns the first half-edge in anti-clockwise order. */
	HalfEdge *getLeadingHalfEdge() { return m_he; }

	/** Returns true if node is on boundary. */
	bool isBoundary()
	{
		return m_he->getVtxRingPrev() == NULL;
	}

	GfxMath::Vec3f  m_pos;   ///< Position of vertex.
	GfxMath::Vec3f  m_N;     ///< Vertex shading normal.
	HalfEdge*       m_he;    ///< Leading half-edge.

	size_t          m_ix;
};

/** Struct containing per triangle data. */
struct TriMesh::Triangle
{
	/** Constructor
	 * \param he One of the half-edges of this triangle.
	 */
	Triangle(HalfEdge *he)
	: m_he(he)
	{ }

	/** Return node no ix from triangle.
	 * \param ix is either 0, 1 or 2.
	 */
	Node* getNode(size_t ix)
	{
		HalfEdge *he = m_he;
		for(size_t i=0; i<ix; i++)
			he = he->getNext();
		return he->getSourceNode();
	}

	/** Returns one of the half-edges of this triangle. */
	HalfEdge* getLeadingHalfEdge()
	{
		return m_he;
	}

	GfxMath::Vec3f m_N;    ///< Shading normal of triangle
	HalfEdge*      m_he;   ///< One of the half-edges of this triangle.
	size_t         m_ix;
};

}
#endif
