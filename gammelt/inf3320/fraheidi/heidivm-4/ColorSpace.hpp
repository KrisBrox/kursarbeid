/* $Id: ColorSpace.h,v 1.3 2006/08/15 16:16:43 dyken Exp $
 *
 * (C) 2005 Christopher Dyken, <dyken@cma.uio.no>
 *
 * Distributed under the GNU GPL.
 */

#ifndef _COLORSPACE_HPP_
#define _COLORSPACE_HPP_

#include <limits>
#include "Vec.hpp"

namespace GfxUtil {

/** Change represention of a color from RGB space to HSV space. */
template<class type>
GfxMath::Vec<type,3>
rgbToHsv(const GfxMath::Vec<type,3>& rgb)
{
	int i;
	GfxMath::Vec<type,3> hsv;
	int w = 0;
	type min = rgb[0];
	type max = rgb[0];
	for(i=0; i<3; i++) {
		if (rgb[i]<min) min = rgb[i];
		if (rgb[i]>max) { max = rgb[i]; w = i; }
	}
	hsv[2] = max;
	hsv[1] = max>std::numeric_limits<type>::epsilon() ? ((max-min)/max) : 0.0;
	if (hsv[1]<=std::numeric_limits<type>::epsilon()) {
	} else {
    	type delta = max-min;
 		switch(w) {
 		case 0: hsv[0] = 60.0*(0.0 + (rgb[1]-rgb[2])/delta); break;
 		case 1: hsv[0] = 60.0*(2.0 + (rgb[2]-rgb[0])/delta); break;
 		case 2: hsv[0] = 60.0*(4.0 + (rgb[0]-rgb[1])/delta); break;
        }
		while (hsv[0] < 0.0) hsv[0] += 360.0;
	}
	return hsv;
}

/** Change represention of a color from HSV space to RGB space. */
template<class type>
GfxMath::Vec<type,3>
hsvToRgb(const GfxMath::Vec<type,3>& hsv)
{
	GfxMath::Vec<type,3> rgb;
	type h = hsv[0];
	type s = hsv[1];
	type v = hsv[2];
	if(s <= std::numeric_limits<type>::epsilon()) {
		for(size_t i=0; i<3; i++)
			rgb[i] = v;
	} else {
		while(h<0.0) h+=360.0;
		while(h>=360.0) h-=360.0;
		h/=60.0;
		int i = (int)floorf(h);
		type f = h-(type)i;
		type p = v*(1.0-s);
		type q = v*(1.0-(s*f));
		type t = v*(1.0-(s*(1.0-f)));
		switch(i) {
		case 0: rgb[0] = v; rgb[1] = t; rgb[2] = p; break;
		case 1: rgb[0] = q; rgb[1] = v; rgb[2] = p; break;
		case 2: rgb[0] = p; rgb[1] = v; rgb[2] = t; break;
		case 3: rgb[0] = p; rgb[1] = q; rgb[2] = v; break;
		case 4: rgb[0] = t; rgb[1] = p; rgb[2] = v; break;
		case 5: rgb[0] = v; rgb[1] = p; rgb[2] = q; break;
		}
	}
	return rgb;
}

} // namespace GfxUtil
#endif //_COLORSPACE_HPP_
