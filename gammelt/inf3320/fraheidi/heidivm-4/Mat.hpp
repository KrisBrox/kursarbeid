// $Id: Mat.h,v 1.4 2006/09/12 18:28:45 dyken Exp $
#ifndef _GFXMATH_MAT_H_
#define _GFXMATH_MAT_H_

#include "Vec.hpp"
#include "Quat.hpp"

namespace GfxMath {

// Homogeneous 4x4 matrix
//    m[0]  m[4]  m[ 8]  m[12]
//    m[1]  m[5]  m[ 9]  m[13]
//    m[2]  m[6]  m[10]  m[14]
//    m[3]  m[7]  m[11]  m[15]
   
  
    template<class T>
    class Mat4 {
    public:
	static Mat4 rotationMatrix(Quat<T> q)
	{
	    Mat4 M;
	    // tip: look at the quaternion class.
	    // skip
	    T xx = q[1]*q[1];
	    T xy = q[1]*q[2];
	    T xz = q[1]*q[3];
	    T xw = q[1]*q[0];
	    T yy = q[2]*q[2];
	    T yz = q[2]*q[3];
	    T yw = q[2]*q[0];
	    T zz = q[3]*q[3];
	    T zw = q[3]*q[0];

	    M.m_M[ 0] = 1.0-2.0*(yy+zz);
	    M.m_M[ 1] =     2.0*(xy+zw);
	    M.m_M[ 2] =     2.0*(xz-yw);

	    M.m_M[ 4] =     2.0*(xy-zw);
	    M.m_M[ 5] = 1.0-2.0*(xx+zz);
	    M.m_M[ 6] =     2.0*(yz+xw);

	    M.m_M[ 8] =     2.0*(xz+yw);
	    M.m_M[ 9] =     2.0*(yz-xw);
	    M.m_M[10] = 1.0-2.0*(xx+yy);

	    M.m_M[ 3] = 0.0;
	    M.m_M[ 7] = 0.0;
	    M.m_M[11] = 0.0;
	    M.m_M[12] = 0.0;
	    M.m_M[13] = 0.0;
	    M.m_M[14] = 0.0;
	    M.m_M[15] = 1.0;
	    // unskip
	    return M;
	}

	T& m(size_t row, size_t col)
	{
	    // skip
	    return m_M[row + 4*col];
	    // unskip
	}

	T* c_ptr()
	{
	    // skip
	    return &m_M[0];
	    // unskip
	}

    protected:
	// skip
	T m_M[16];
	// unskip
    };

    typedef Mat4<float>  Mat4f;
    typedef Mat4<double> Mat4d;

} // namespace GfxMath
#endif //_GFXMATH_MAT_H_
