/* $Id: oblig1_gasket.cpp, v1.0 2011/09/07$
 *
 * Author: Bartlomiej Siwek, <bartloms@ifi.uio.no>
 * 
 * Distributed under the GNU GPL.
 */

#include <cstdlib>
#include <cmath>
#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/gl.h>

#include <glm/glm.hpp>

#include "OpenGLError.hpp"

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

// Vertex structure
struct Vertex { 
  Vertex(float x, float y)
      : position(x, y) {
  }
  
  Vertex(const glm::vec2 &p) 
      : position(p) {
  }
  
  glm::vec2 position;
};

// Global variables
static std::vector<Vertex> vertices;
static std::vector<unsigned int> indices;
static int level = 0;

// Vertex buffer object IDs
//-------------------------------------------------
GLuint verticesVertexBufferObjectId; 
GLuint indicesIndexBufferObjectId;
//-------------------------------------------------
void sierpinskiGasket(unsigned int a, unsigned int b, unsigned int c, unsigned int level) {
  if(level == 0) {
    // Insert trinagle indices into indices array
    //---------------------------------------------
    indices.push_back(a);
    indices.push_back(b);
    indices.push_back(c);
    //---------------------------------------------


  } else {
    // Generate new vertices and call this function recursively
    //--------------------------------------------------
    --level;
    Vertex ta = vertices[a]; Vertex tb = vertices[b]; Vertex tc = vertices[c];
    Vertex ab = Vertex((ta.position.x+tb.position.x)/2.0f, (ta.position.y+tb.position.y)/2.0f);
    Vertex ac = Vertex((ta.position.x+tc.position.x)/2.0f, (ta.position.y+tc.position.y)/2.0f);
    Vertex bc = Vertex((tb.position.x+tc.position.x)/2.0f, (tb.position.y+tc.position.y)/2.0f);
    unsigned int pos = vertices.size();
    vertices.push_back(ab);
    vertices.push_back(bc); 
    vertices.push_back(ac);
    sierpinskiGasket(a, pos, pos+2, level);
    sierpinskiGasket(pos, b, pos+1, level);
    sierpinskiGasket(pos+2, pos+1, c, level);
    //-------------------------------------------------
  }
}

void rebuildGasket() {
  // Clear the data arrays
  vertices.clear();
  indices.clear();
  
  // Insert basic vertices
  vertices.push_back(Vertex(-1.0f, -1.0f));
  vertices.push_back(Vertex( 1.0f, -1.0f));
  vertices.push_back(Vertex( 0.0f,  1.0f));
  
  // Start the recursion
  sierpinskiGasket(0, 1, 2, level);
}

void bindDataToBuffers() {
  // Bind VBOs and provide data to them
    //------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, verticesVertexBufferObjectId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0])*vertices.size(), &vertices[0], GL_STREAM_DRAW);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesIndexBufferObjectId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0])*indices.size(), &indices[0], GL_STREAM_DRAW); 
    glVertexPointer(2, GL_FLOAT,0,0);

    //------------------------------------------------
  // Unbind the VBO's after we are done
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  
  CHECK_OPENGL;
}

void myInit()
{
  // Initialize the gasket
  rebuildGasket();
  
  // Generate VBO ids
    //----------------------------------------------------
    glGenBuffers(1, &verticesVertexBufferObjectId);
    glGenBuffers(1, &indicesIndexBufferObjectId);
    //----------------------------------------------------
     
  // Bind data to buffers
  bindDataToBuffers();
    
  // Initialize OpenGL specific part
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  
  CHECK_OPENGL;
}

void myKeyboard(unsigned char key, int /* x */, int /* y */)
{
  switch(key) {
    case '+':
      level++;
      // Rebuild gasket and bind data to buffers
	myInit();
      break;
    case '-':
      level = level > 0 ? level-1 : 0;
      // Rebuild gasket and bind data to buffers
	myInit();
      break;
  }
  
  glutPostRedisplay();
}

void myDisplay()
{
  // Clear the backgound
  glClear(GL_COLOR_BUFFER_BIT);
 
  // Bind VBO's and call a drawing function
    //----------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER,verticesVertexBufferObjectId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,indicesIndexBufferObjectId);
    glColor3f(1.0f,1.0f,1.0f); 

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_INDEX_ARRAY);

    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, NULL); // Endre for � endre nivaa
    
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_INDEX_ARRAY);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    //---------------------------------------
  // Switch the buffer
  glFlush();
  glutSwapBuffers();
  
  CHECK_OPENGL;
}

void myShutdown() {
  // Delete VBOs
    glDeleteBuffers(1, &verticesVertexBufferObjectId);
    glDeleteBuffers(1, &indicesIndexBufferObjectId);
}

int main(int argc, char **argv)
{
  // Initialization of GLUT
  glutInit(&argc, argv);
  glutInitContextVersion(3, 1);
  glutInitContextFlags(GLUT_DEBUG);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutInitWindowSize(512, 512);
  glutCreateWindow( __FILE__ );

  // Init GLEW
  GLenum err = glewInit();
  if (GLEW_OK != err) {
    std::cerr << "Error: " << glewGetErrorString(err) << std::endl;
    return 1;
  } 

  if(!GLEW_VERSION_3_1) {
    std::cerr << "Driver does not support OpenGL 3.1" << std::endl;  
    return 1;
  }
  
  // Attach handlers
  glutDisplayFunc(myDisplay);
  glutKeyboardFunc(myKeyboard);
  
  // A nasty trick to get a shutdown handler
  atexit(myShutdown);
  
  // Application specific initialization
  myInit();
  
  // Run the GLUT main loop
  glutMainLoop();
  return 0;
}
