#!/usr/bin/env python
from __future__ import division
from scitools.std import *
from numpy import sin, pi

def S(t,n, T=2*pi):
	s = 0
	for j in range(0,n):
		s += 1/(2*j-1)*sin(2*(2*j-1)*pi*t/T)
	return s*4/pi
t = linspace(0,2*pi, 1001)
for n in (1,3,20,200):
	#print type (S(t, n))
	plot(t, S(t, n), hold='on')


#x = linspace(0,4*pi, 1001)


