#!/usr/bin/env python
import sys
v0 = 3; g = 9.81; t = 0.6
if len(sys.argv) >= 3:
   v0 = int(sys.argv[1])
   t = int(sys.argv[2])
else:
   print "need two arguments, v0 and t (in that order)"
   sys.exit(1)
y = v0*t - 0.5*g*t**2
print y
#---------------------------------------------------------
