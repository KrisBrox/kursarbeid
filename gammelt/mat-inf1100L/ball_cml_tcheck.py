#!/usr/bin/env python
import sys
g=9.81
if len(sys.argv) >= 3:
   v0 = float(sys.argv[1])
   t = float(sys.argv[2])
else:
   print "need two arguments, v0 and t (in that order)"
   sys.exit(1)
if t < 0 or t >= 2*v0/g:
   sys.exit('only interested in positive t giving positive y')
y = v0*t - 0.5*g*t**2
print y
# -----------------------------------------------------------------------------
