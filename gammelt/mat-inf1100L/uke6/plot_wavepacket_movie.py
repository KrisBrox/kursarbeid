#!/usr/bin/env python
from scitools.std import *

def f(x, t=0):
   return exp(-(x-3*t)**2)*sin(3*pi*(x-t))

t_values = linspace(-1,1, 61)
x = linspace(-6, 6, 1001) 

import time
for t in t_values:
   y = f(x, t=t)
   plot(x, y, axis = [x[0], x[-1], -1, 1], legend = 't=%4.2f'%t)
   time.sleep(0.2) 


