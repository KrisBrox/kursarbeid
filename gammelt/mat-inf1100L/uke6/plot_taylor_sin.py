#!/usr/bin/env python
from __future__ import division
from numpy import sin, pi, linspace 
from math import factorial
from scitools.std import plot

def S(x, n):
   s = 0
   for i in range(n+1):
      s += (((-1)**i)*x**(2*i+1))/factorial(2*i+1)
   return s      
   

x = linspace(0, 2*pi, 200)
s1, s2, s3, s6, s12 = S(x,1), S(x,2), S(x,3), S(x,6), S(x, 12)
ss = sin(x)

for s in [s1, s2, s3, s6,s12, ss]:
   plot(x, s, hold='on')
