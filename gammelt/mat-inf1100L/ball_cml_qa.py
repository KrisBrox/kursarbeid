#!/usr/bin/env python
import sys
v0 = 3; g = 9.81; t = 0.6
try:
   v0 = int(sys.argv[1])
   t = int(sys.argv[2])
except IndexError:
   print "need two arguments, v0 and t (in that order)"
   v0 = int(raw_input('v0=?'))
   t = int(raw_input('t=?'))
y = v0*t - 0.5*g*t**2
print y
#-----------------------------------------------------
