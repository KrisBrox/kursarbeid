% Script for testing sdtest.m with different input
M = [50 ,100,200,1000,2000];
a = 1./9;
d = 5./18;
tol = 10^-8;
for m=M
    n = m*m;
    itmax = n*10;
    K = sdtest(m,a,d,tol,itmax);
    str = sprintf('n: %d, K: %d',n,K);
    disp(str)
end

M = [10,20,40,50];
a = -1;
d = 2;
tol = 10^-8;
for m=M
    n = m*m;
    itmax = n*10;
    K = sdtest(m,a,d,tol,itmax);
    str = sprintf('n: %d, K: %d',n,K);
    disp(str)
end
