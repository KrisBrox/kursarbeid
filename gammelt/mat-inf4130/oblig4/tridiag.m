function T = tridiag( a,b,c,n )

a = a*ones(n-1,1); b = b*ones(n,1); c = c*ones(n-1,1);

T = gallery('tridiag', a, b, c);
end

