function [X,K] = sdtest(m,a,d,tol,itmax)
    R = ones(m)/(m+1)^2;
    rho = sum(sum(R.*R));
    rho0 = rho;
    %disp(rho0)
    X = zeros(m,m);
    %I = eye(m);
    T1 = sparse(diag(ones(m-1,1)*a,-1)+diag(ones(m,1)*d,0) ... 
            +diag(ones(m-1,1)*a,1));
    %T2 = kron(T1,I)+kron(I,T1);
    for k=1:itmax
        %disp(rho/rho0)
       if sqrt(rho/rho0)<=tol
           K = k;
           return
       end
       T = T1*R+R*T1;
       %T = T2*R;
       alpha = rho/sum(sum(R.*T));
       X = X + alpha*R;
       R = R - alpha*T;
       rho = sum(sum(R.*R));
       %disp(rho)
    end
    %disp(rho)
    %disp(rho/rho0)
    K = itmax + 1;
end

