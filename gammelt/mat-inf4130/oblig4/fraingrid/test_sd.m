N = [2500,10000,40000,1000000,4000000];
a = 1./9;
d = 5./18;
tol = 1e-8;
for n=N
    m = sqrt(n);
    itmax = n*10;
    [X,K] = sdtest(m,a,d,tol,itmax);
    str = sprintf('n: %d, K: %d',n,K)
end

N = [2500,10000,40000,160000];
a = -1;
d = 2;
tol = 1e-8;
for n=N
    m = sqrt(n);
    itmax = n*10;
    [X,K] = sdtest(m,a,d,tol,itmax);
    str = sprintf('n: %d, K: %d',n,K)
end
