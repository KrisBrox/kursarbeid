function K = sdtest(m,a,d,tol,itmax)
    R = ones(m)/(m+1)^2;
    rho = sum(sum(R.*R));
    rho0 = rho;
    X = zeros(m,m);
    T1 = tridiag(a, d, a, m);
    for k=1:itmax
       if sqrt(rho/rho0)<=tol
           K = k;
           return
       end
       T = T1*R+R*T1;
       a = rho/sum(sum(R.*T));
       X = X + a*R;
       R = R - a*T;
       rho = sum(sum(R.*R));
    end
    K = itmax + 1;
end

