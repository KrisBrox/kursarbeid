function U=simplefastbiharmonic(F)
m = length(F); h = 1/(m+1);
hv = pi*h*(1:m)';
S = sin(hv*(1:m));
G = S*F*S;
sigma = sin((hv/2)).^2;

X = h^6 * G./(( sigma*ones(1,m) + ones(1,m)'*sigma').^2);
U = 0.25*S*X*S;

end