function V = standard(F)
I = eye(size(F));
T = zeros(size(F));
r = length(F);
h = 1/(r+1);
for n = 1 : size(F),
    for m = 1:size(F),
        if n==m,
           T(n,m) = 2; 
        end
        if ((n == m-1) || (m == n-1)),
            T(n,m) = -1;
        end
    end
end

A = (kron(T,I) + kron(I,T)) ^ 2;

F = h^4 * F(:);
x = linsolve(A,F);
V = reshape(x,size(I));

end