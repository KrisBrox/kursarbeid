% Script to find when error crosses tolerance 10^-8
n = 6;
k = n-1;
maxk = 0;
crossedthreshold = false;
while crossedthreshold == false
    t = f(k, n); 
    if t > maxk
        maxk = t;
    end
    if ((t < 10^-8) && ( crossedthreshold == false))
        crossedthreshold = true;
        when = k;
        disp(when)
    end
    k = k+1;
end
