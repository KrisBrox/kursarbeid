function y = f( k, n )
lambda = 0.9;
a = 10;
y = nchoosek(k,n-1)*a^(n-1) * lambda^(k-n+1);
end

