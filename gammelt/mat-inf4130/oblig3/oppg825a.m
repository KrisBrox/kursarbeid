% Script to plot f(k) for k up to 200
n = 6;
v = zeros(200-n +1);
maxk = 0;
crossedthreshold = false;
for k = n-1 : 200
    t = f(k, n);
    v(k-n+2) = t; 
    if t > maxk
        maxk = t;
    end
    if ((t < 10^-8) && ( crossedthreshold == false))
        crossedthreshold = true;
        when = k;
        disp(when)
    end
end
plot(v)
