#!/usr/bin/env python
import sys, re

try:
    word = sys.argv[-1]
    text = open(sys.argv[-2], 'r')
except:
    sys.exit("when searching for a word in a text it is customary to tell your program what you are searching for, and where to search for it.")
r = "%s" % word
t = '';case = ''
for line in text:
    t += line
if '-i' in sys.argv:
    t = t.lower()
    word = word.lower()
    case = ' (case insensitive)'
if '-b' in sys.argv:
    r = '\s'+r+'[\s\.,\?!]'

print 'Number of occurances of word %s%s: %d' % (word,case, len(re.findall(r, t)))
"""
krisbrox@iskrystall ~/Inf3331/uke4krisbrox $ ./count_words.py -b -i football.txt goal
Number of occurances of word goal (case insensitive): 2
krisbrox@iskrystall ~/Inf3331/uke4krisbrox $ ./count_words.py football.txt ball
Number of occurances of word ball: 3
krisbrox@iskrystall ~/Inf3331/uke4krisbrox $ ./count_words.py -i football.txt ball
Number of occurances of word ball (case insensitive): 3
krisbrox@iskrystall ~/Inf3331/uke4krisbrox $ ./count_words.py -b football.txt goal
Number of occurances of word goal: 1
krisbrox@iskrystall ~/Inf3331/uke4krisbrox $ 
"""
