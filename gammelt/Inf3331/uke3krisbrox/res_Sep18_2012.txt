krisbrox@urbankan ~/Inf3331/uke3krisbrox $ ./inversconvert.py t1.dat t2.dat t3.dat
## result: 
some comment line
1.000000
	t1	t2	t3
	2	1	0
	3	1	1
	5	2	1
	6	3	2

##-------------------------------------------
krisbrox@aeminium ~/Inf3331/uke3krisbrox $ ls
add_date.py  inversconvert.py  outfile  res.txt  t1.dat  t2.dat  t3.dat
krisbrox@aeminium ~/Inf3331/uke3krisbrox $ ./add_date.py res.txt 
krisbrox@aeminium ~/Inf3331/uke3krisbrox $ ls
add_date.py       outfile             t1.dat  t3.dat
inversconvert.py  res_Sep18_2012.txt  t2.dat
##-------------------------------------------
krisbrox@aeminium ~/Inf3331/uke3krisbrox $ ./ranking.py efficiency.test 
CPU-time:   5.41   g77 -O3 -ffast-math -funroll-loops original (optimal?) cod
CPU-time:   5.55   g77 -O3 original (optimal?) cod
CPU-time:   5.62   g77 -O2 original (optimal?) cod
CPU-time:   6.02   f95 -O2 original (optimal?) cod
CPU-time:   6.02   f95 -O3 original (optimal?) cod
CPU-time:   6.04   f95 -O1 original (optimal?) cod
CPU-time:   7.22   g77 -O3 -ffast-math -funroll-loops traversing the array rowwis
CPU-time:   7.27   f95 -O1 traversing the array rowwis
CPU-time:   7.27   f95 -O2 traversing the array rowwis
CPU-time:   7.30   g77 -O3 traversing the array rowwis
CPU-time:   7.31   f95 -O3 traversing the array rowwis
CPU-time:   7.37   g77 -O2 traversing the array rowwis
CPU-time:   7.67   g77 -O1 original (optimal?) cod
CPU-time:   8.26   g77 -O1 traversing the array rowwis
CPU-time:   9.45   f95 -O3 if-test inside the main loo
CPU-time:   9.47   f95 -O1 if-test inside the main loo
CPU-time:   9.52   f95 -O2 if-test inside the main loo
CPU-time:   9.67   g77 -O3 -ffast-math -funroll-loops if-test inside the main loo
CPU-time:   9.87   g77 -O3 if-test inside the main loo
CPU-time:   9.92   g77 -O2 if-test inside the main loo
CPU-time:  11.66   g77 -O1 if-test inside the main loo
CPU-time:  12.30   f95 -O2 lambda array replaced by h(x,y) call
CPU-time:  12.31   f95 -O1 lambda array replaced by h(x,y) call
CPU-time:  12.32   f95 -O3 lambda array replaced by h(0,0) call
CPU-time:  12.32   f95 -O3 lambda array replaced by h(x,y) call
CPU-time:  12.33   f95 -O1 lambda array replaced by h(0,0) call
CPU-time:  12.33   f95 -O2 lambda array replaced by h(0,0) call
CPU-time:  15.27   f95 -O0 original (optimal?) cod
CPU-time:  15.84   f95 -O0 if-test inside the main loo
CPU-time:  15.97   f95 -O0 traversing the array rowwis
CPU-time:  17.64   f95 -O3 -Kloop -KPENTIUM_PRO -x - lambda array replaced by h(x,y) call
CPU-time:  19.45   f95 -O3 -Kloop -KPENTIUM_PRO -x - original (optimal?) cod
CPU-time:  19.58   f95 -O3 -Kloop -KPENTIUM_PRO -x - traversing the array rowwis
CPU-time:  22.18   f95 -O3 -Kloop -KPENTIUM_PRO -x - lambda array replaced by h(0,0) call
CPU-time:  29.58   f95 -O3 -Kloop -KPENTIUM_PRO -x - if-test inside the main loo
CPU-time:  30.55   f95 -O0 lambda array replaced by h(0,0) call
CPU-time:  32.39   f95 -O0 lambda array replaced by h(x,y) call
CPU-time:  37.56   g77 -O1 lambda array replaced by h(0,0) call
CPU-time:  37.59   g77 -O3 -ffast-math -funroll-loops lambda array replaced by h(0,0) call
CPU-time:  37.85   g77 -O3 lambda array replaced by h(0,0) call
CPU-time:  37.91   g77 -O2 lambda array replaced by h(0,0) call
CPU-time:  41.33   g77 -O1 lambda array replaced by h(x,y) call
CPU-time:  41.97   g77 -O3 -ffast-math -funroll-loops lambda array replaced by h(x,y) call
CPU-time:  42.78   g77 -O3 lambda array replaced by h(x,y) call
CPU-time:  42.88   g77 -O2 lambda array replaced by h(x,y) call
CPU-time:  42.94   g77 -O0 original (optimal?) cod
CPU-time:  43.50   g77 -O0 traversing the array rowwis
CPU-time:  45.37   g77 -O0 if-test inside the main loo
CPU-time:  49.10   g77 -O0 lambda array replaced by h(0,0) call
CPU-time:  56.54   g77 -O0 lambda array replaced by h(x,y) call
CPU-time: 229.95   g77 -O1 formatted I/
CPU-time: 234.90   g77 -O3 formatted I/
CPU-time: 236.01   g77 -O3 -ffast-math -funroll-loops formatted I/
CPU-time: 238.32   g77 -O2 formatted I/
CPU-time: 243.93   f95 -O3 -Kloop -KPENTIUM_PRO -x - formatted I/
CPU-time: 252.40   f95 -O2 formatted I/
CPU-time: 252.47   f95 -O1 formatted I/
CPU-time: 252.51   f95 -O3 formatted I/
CPU-time: 255.97   f95 -O0 formatted I/
CPU-time: 272.90   g77 -O0 formatted I/

