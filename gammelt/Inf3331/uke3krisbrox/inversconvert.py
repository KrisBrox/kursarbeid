#!/usr/bin/env python
import math, sys, string

#open the infiles
filenames = sys.argv[1:]
infiles = []
for name in filenames:
    try:
	infiles.append(open(name, 'r'))
    except:
	print 't is read from the files, not the command line'
	sys.exit(1)	
# write the first line
outfile = open('outfile', 'w')
outfile.write('some comment line\n')

#find dt and write it on line 2
tvals = []
for line in infiles[0]:
    tvals.append(float(line.split()[0]))
dt = tvals[1]-tvals[0]
outfile.write('%f\n' % (dt) )

numlines = len(tvals)

#write the columnnames on line 3 
names = ""
for name in filenames:
    names += '\t' + name[:-4]
outfile.write('%s\n' % names)

#reopen the file used to find dt
infiles[0].close()
infiles[0] = open(sys.argv[1], 'r')

#copy the data
lines = []
for i in range(numlines):
    lines.append('')
    for file in infiles:
	lines[i] += '\t' + file.readline().split()[1]
    outfile.write(lines[i]+'\n')

for file in infiles:
    file.close()
outfile.close()
