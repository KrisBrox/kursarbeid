#!/usr/bin/env python
import sys, random
try:
    n = int(sys.argv[1])
except:
    sys.exit('usage: %s <number of iterations>' % sys.argv[0])

k = 0; sum = 0
while k < n:
    k += 1 
    if random.randint(1,6) == 6 or random.randint(1,6) == 6:
	sum += 1
p = sum/float(n)
print p, 11/36.

# Running it produces:

#krisbrox@kaldfront ~/Inf3331/uke6krisbrox $ ./dice2.py 500000
#0.305564 0.305555555556
#krisbrox@kaldfront ~/Inf3331/uke6krisbrox $ ./dice2.py 100000
#0.30766 0.305555555556
#krisbrox@kaldfront ~/Inf3331/uke6krisbrox $ ./dice2.py 100000
#0.3052 0.305555555556
#krisbrox@kaldfront ~/Inf3331/uke6krisbrox $ ./dice2.py 100000
#0.30558 0.305555555556
#krisbrox@kaldfront ~/Inf3331/uke6krisbrox $ ./dice2.py 500000
#0.305754 0.305555555556

