#!/usr/bin/env python
import shutil, os, time

def sizeandtime(arg, dirname, files):
    for file in files:
	filepath = os.path.join(dirname, file)
	if os.path.isfile(filepath):
	    size = os.path.getsize(filepath)/1000000.
	    when = os.path.getatime(filepath)
	    when = (time.time()-when)/(3600*24)
	    if size > arg[1] and when > arg[2]:
		arg[0].append(filepath)

def oldandlarge(dir, Mb, days):
    delete = []
    os.path.walk(dir, sizeandtime, [delete, Mb, days])
    for file in delete:
	if not os.path.isdir('tmp'):
	    os.mkdir('tmp')
	shutil.copy(file, './tmp/')
	os.remove(file)

if __name__ == '__main__':
    oldandlarge('./tmptree', 1, 100)

# Runtime example:

"""
krisbrox@kaldfront ~/Inf3331/uke6krisbrox/tmp $ ls
tmpf-165459  tmpf-188037  tmpf-19120  tmpf-544825  tmpf-93296
krisbrox@kaldfront ~/Inf3331/uke6krisbrox/tmp $ rm * 
krisbrox@kaldfront ~/Inf3331/uke6krisbrox/tmp $ ls
krisbrox@kaldfront ~/Inf3331/uke6krisbrox/tmp $ cd ..
krisbrox@kaldfront ~/Inf3331/uke6krisbrox $ ls
fakefiletree.py  findprograms.py  old_and_large.py  tmp  tmptree
krisbrox@kaldfront ~/Inf3331/uke6krisbrox $ cd tmptree/
./tmptree/
krisbrox@kaldfront ~/Inf3331/uke6krisbrox/tmptree $ ls -l
total 35572
drwx------ 2 krisbrox 1100    4096 2012-10-08 18:24 tmpf-160372
-rw------- 1 krisbrox 1100 9836000 2012-02-28 17:24 tmpf-165459
-rw------- 1 krisbrox 1100 5652000 2012-04-25 18:24 tmpf-19120
drwx------ 2 krisbrox 1100    4096 2012-10-08 18:24 tmpf-391976
-rw------- 1 krisbrox 1100 9779000 2012-02-14 17:24 tmpf-544825
-rw------- 1 krisbrox 1100 1078000 2012-07-13 18:24 tmpf-776307
-rw------- 1 krisbrox 1100 9986000 2012-09-06 18:24 tmpf-834646
krisbrox@kaldfront ~/Inf3331/uke6krisbrox/tmptree $ cd .. 
krisbrox@kaldfront ~/Inf3331/uke6krisbrox $ ./old_and_large.py 
krisbrox@kaldfront ~/Inf3331/uke6krisbrox $ cd tmptree/
./tmptree/
krisbrox@kaldfront ~/Inf3331/uke6krisbrox/tmptree $ ls -l
total 10840
drwx------ 2 krisbrox 1100    4096 2012-10-08 18:31 tmpf-160372
drwx------ 2 krisbrox 1100    4096 2012-10-08 18:31 tmpf-391976
-rw------- 1 krisbrox 1100 1078000 2012-07-13 18:24 tmpf-776307
-rw------- 1 krisbrox 1100 9986000 2012-09-06 18:24 tmpf-834646
krisbrox@kaldfront ~/Inf3331/uke6krisbrox/tmptree $ cd ../tmp
krisbrox@kaldfront ~/Inf3331/uke6krisbrox/tmp $ ls
tmpf-165459  tmpf-188037  tmpf-19120  tmpf-544825  tmpf-93296
krisbrox@kaldfront ~/Inf3331/uke6krisbrox/tmp $ 
"""
