#!/usr/bin/env python
import sys, random

try:
    n = int(sys.argv[1])
except:
    sys.exit('usage: %s <number of iterations>' % sys.argv[0])

k = 0; sum = 0; win = 0
while k < n:
    k += 1 
    for i in range(0,4):
	sum += random.randint(1,6)
    if sum < 9:
	win += 1
    sum = 0
p = win/float(n)
print 'fraction of simulations won:', p
if p > 1/11.:
    print 'you should play'
else:
    print 'you should not play'

# result:
#   (note: i assumed I would lose if the total was exactly nine)


#krisbrox@kaldfront ~/Inf3331/uke6krisbrox $ ./dice4.py 100000
#fraction of simulations won: 0.05336
#you should play
#krisbrox@kaldfront ~/Inf3331/uke6krisbrox $ ./dice4.py 100000
#fraction of simulations won: 0.0546
#you should play

