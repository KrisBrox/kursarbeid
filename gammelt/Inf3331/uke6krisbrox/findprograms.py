#!/usr/bin/env python
import sys, os, glob

def findprograms(names, dirs):
    paths = os.environ['PATH'].split(':') + dirs
    dict = {}
    found = 0
    for name in names:
	for path in paths:
	    fullpath = os.path.join(path, name)
	    if os.path.isfile(fullpath):
		dict[name] = fullpath
		found = 1
	if not found:
	    dict[name] = None
	else: 
	    found = 0
    return dict

if __name__ == '__main__':
    programs = {
	'gnuplot'  : 'plotting program',
	'gs'       : 'ghostscript, ps/pdf interpreter and previewer',
	'f2py'     : 'generator for Python interfaces to F77',
	'swig'     : 'generator for Python interfaces to C/C++',
	'convert'  : 'image conversion, part of the ImageMagick package',
	'findprograms.py' : 'this program',
	'lolcats'    : 'program for easy access to funny cats'
    }

    installed = findprograms(programs.keys(), ['e', 'r'])
    for program in installed.keys():
	if installed[program]:
	    print "You have %s (%s)" % (program, programs[program])
	else:
	    print "*** Program %s was not found on the system" % (program,)

# Running the code produces this output:

#krisbrox@kaldfront ~/Inf3331/uke6krisbrox $ ./findprograms.py 
#You have convert (image conversion, part of the ImageMagick package)
#You have gs (ghostscript, ps/pdf interpreter and previewer)
#You have gnuplot (plotting program)
#You have swig (generator for Python interfaces to C/C++)
#You have findprograms.py (this program)
#You have f2py (generator for Python interfaces to F77)
#*** Program lolcats was not found on the system

