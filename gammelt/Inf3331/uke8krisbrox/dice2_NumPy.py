#!/usr/bin/env python
import sys,random, time
try:
    n = int(sys.argv[1])
except:
    sys.exit('usage: %s <number of iterations>' % sys.argv[0])

k = 0; sum = 0
start = time.time()
while k < n:
    k += 1 
    if random.randint(1,6) == 6 or random.randint(1,6) == 6:
	sum += 1
p = sum/float(n)
stop = time.time()
print p, 11/36.
print 'probability calculated in %g seconds using the standard libraries \
(%d iterations)' % (stop-start, n)

import numpy as np
k = 0; sum = 0
start = time.time()
throws = np.random.randint(1, 7, n*2)
yes = np.where(throws == 6, 1, 0)
yes = np.reshape(yes,(n, 2))
t = np.zeros(n)
sum = np.any(yes, 1, t)
sum = np.sum(t)
stop = time.time()
print sum/float(n), 11/36.
print 'probability calculated in %g seconds utilizing vectorization \
(%d iterations)' % (stop-start, n)

# Result:
#krisbrox@aeminium ~/Inf3331/uke8krisbrox $ ./dice2_NumPy.py 5000000
#0.3056518 0.305555555556
#probability calculated in 18.4178 seconds using the standard libraries (5000000 iterations)
#0.3053162 0.305555555556
#probability calculated in 1.07701 seconds utilizing vectorization (5000000 iterations)



