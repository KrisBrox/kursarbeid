#!/usr/bin/env python
import numpy as np
import sys
try:
    f = open(sys.argv[1], 'r')
except:
    sys.exit('give a file to read')
titles = []; nums = []
for line in f:
    t = line[:-1].split(',')
    titles.append(t[0])
    nums.append(t[1:])
mat = np.matrix(nums).astype(float)
t = np.sum(mat, axis = 1)
for i in range(len(titles)):
    print '%s : %g'%(titles[i], t[i])
f.close()

"""
When running this script on a file containing: 

"activity 1", 2376, 256, 87
"activity 2", 27, 89, 12
"activity 3", 199, 166.50, 0

The result is:
krisbrox@aeminium ~/Inf3331/uke8krisbrox $ ./process_spreadsheet.py data.dat
"activity 1" : 2719
"activity 2" : 128
"activity 3" : 365.5
"""
