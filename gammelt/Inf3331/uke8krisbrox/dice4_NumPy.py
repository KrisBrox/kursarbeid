#!/usr/bin/env python
import numpy as np
import sys, random, time
try:
    n = int(sys.argv[1])
except:
    sys.exit('usage: %s <number of iterations>' % sys.argv[0])

# Standard libs
k = 0; sum = 0; win = 0
start = time.time()
while k < n:
    k += 1 
    for i in range(0,4):
	sum += random.randint(1,6)
    if sum < 9:
	win += 1
    sum = 0
p = win/float(n)
stop = time.time()
print 'fraction of simulations won:', p
print 'calculated in %g seconds (%d iterations)' %(stop-start, n) 

# with numpy:
start = time.time()
throws = np.random.randint(1, 7, n*4)
throws = throws.reshape(n, 4)
sum = np.sum(throws, 1)
wins = np.where(sum < 9, 1, 0)
stop = time.time()
print 'fraction of simulations won:',np.sum(wins)/float(n)
print 'calculated in %g seconds (%d iterations)'%(stop-start, n)

# result
# krisbrox@aeminium ~/Inf3331/uke8krisbrox $ ./dice4_NumPy.py 1000000
# fraction of simulations won: 0.053975
# calculated in 10.5586 seconds (1000000 iterations)
# fraction of simulations won: 0.053723
# calculated in 0.191285 seconds (1000000 iterations)

