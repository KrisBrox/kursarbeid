#!/usr/bin/env python

class SparseVec:
    def __init__(self, n):
	self.n = n
	self.nonz = {}
	
    def __setitem__(self, i, f):
	self.nonz[(i)] = float(f) 

    def __getitem__(self, i):
	return self.nonz[(i)]

    def __str__(self):
	string = ''
	for i in range(self.n):
	    if (i) in self.nonz:
		string += '[%d]=%g ' % (i, self.nonz[(i)])
	    else:
		string += '[%d]=0 ' % i
	return string

    def nonzeros(self):
	string = '{'
	for i in range(self.n):
	    if (i) in self.nonz:
		string += '%d: %g, ' % (i, self.nonz[(i)])
	string = string[:-2] + '}'
	return string

    def __add__(s, o):
	if s.n > o.n:
	    n = s.n
	else:
	    n = o.n
	ret = SparseVec(n)
	for i in range(n):
	    if (i) in s.nonz:
		ret.nonz[(i)] = s.nonz[(i)]
	    if (i) in o.nonz:
		if (i) in ret.nonz:
		    ret.nonz[(i)] += o.nonz[(i)]
		else:
		    ret.nonz[(i)] = o.nonz[(i)]
	return ret

    def __iter__(self):
	""" Note that I here use the iterator of the dict holding the 
	nonzero elements of the vector, thus the class will not work 
	exactly as illustrated by the example. I believe this is somewhat 
	reasonable given that this is a sparse vector, so the other elements
	can be assumed to equal 0.
	"""
	return self.nonz.iteritems()

a = SparseVec(4); b = SparseVec(6)
a[2] = -5; a[0] = 3.8; b[1] = 1; b[2] = 30
print a; print a.nonzeros(); print b
c = a + b
print c
for i,ai in c:
    print 'a[%d]=%d ' % (i, ai),

""" Result:
krisbrox@aeminium ~/Inf3331/uke8krisbrox $ ./SparseVec.py
[0]=3.8 [1]=0 [2]=-5 [3]=0 
{0: 3.8, 2: -5}
[0]=0 [1]=1 [2]=30 [3]=0 [4]=0 [5]=0 
[0]=3.8 [1]=1 [2]=25 [3]=0 [4]=0 [5]=0 
a[0]=3  a[1]=1  a[2]=25 

"""
