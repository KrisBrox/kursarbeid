#!/usr/bin/env python
from scitools.numpytools import *

x = sequence(0, 1, 0.5)
# y = 2*x + 1:
y = x;  y *= 2;  y += 1
# z = 4*x - 4:
z = x;  z *= 4;  z -= 4
print x, y, z

"""
The reason x, y and z get the same values seems to be that x is a pointer,
and so, when you set y and z to equal x they actually just point to the same
place in memory. The result is that anything done to either of the three is
also done to the other two.

To circumvent this, we need to make y and z copies of x: this is done by
changing the two assignement statements from y|z = x to y|z = x.copy(), 
seen below:
"""

x = sequence(0, 1, 0.5)
# y = 2*x + 1:
y = x.copy();  y *= 2;  y += 1
# z = 4*x - 4:
z = x.copy();  z *= 4;  z -= 4
print x, y, z

# output from running this script:
# krisbrox@aeminium ~/Inf3331/uke8krisbrox $ ./NumPy_assignement.py 
# [ 0.  4.  8.] [ 0.  4.  8.] [ 0.  4.  8.] 
# [ 0.   0.5  1. ] [ 1.  2.  3.] [-4. -2.  0.]

