#!/usr/bin/env python
import random as r
import sys

print 'Welcome to Rock, Paper, Scissors!'

s = raw_input('How many points are required for a win? ')
try:
    n = int(s)
except ValueError:
    sys.exit('invalid input')
choices = ['r', 'p', 's']
human = 0; comp = 0;

def displayresult(h, c):
    """
    in this function the result of the round is calculated.
    a more efficient and elegant solution would use a matrix containing the
    possible outcomes. this solution is more primitive, but writing it
    required less thinking.
    """
    dict = {
    'r': 'rock',\
    'p': 'paper',\
    's': 'stone'
    }
    
    if h == c:
	ans = 'A draw'
	r = 0
    elif \
    (h == 'r' and c == 's') \
    or (h == 's' and c == 'p') \
    or (h == 'p' and c == 'r'):
	ans = 'Human wins!'
	r = 1
    else:
	ans = 'Computer wins!'
	r = -1

    print 'Human: %s\t Computer: %s\t %s\n' % (dict[h],dict[c], ans)
    return r

while 1:
    h = raw_input('Choose (R)ock, (P)aper, or (S)cissors? ').lower()
    c = choices[r.randint(0,2)]
    if h not in choices:
	print "the only valid inputs are 'R', 'P' or 'S'"
	continue
    
    who = displayresult(h, c)
    if who < 0:
	comp += 1
    elif who > 0:
	human += 1
    print 'Score: Human %d\t Computer %d' % (human, comp)
    if human == n or comp == n:
	sys.exit('\nFinal score: Human %d\t Computer %d\n' % (human, comp) )

# result:
"""
krisbrox@aeminium ~/Inf3331/uke7krisbrox $ ./roshambo.py 
Welcome to Rock, Paper, Scissors!
How many points are required for a win? 2
Choose (R)ock, (P)aper, or (S)cissors? r
Human: rock      Computer: stone         Human wins!

Score: Human 1   Computer 0
Choose (R)ock, (P)aper, or (S)cissors? p
Human: paper     Computer: stone         Computer wins!

Score: Human 1   Computer 1
Choose (R)ock, (P)aper, or (S)cissors? s
Human: stone     Computer: paper         Human wins!

Score: Human 2   Computer 1

Final score: Human 2     Computer 1
"""

