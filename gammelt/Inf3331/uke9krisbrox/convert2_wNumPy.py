#!/usr/bin/env python
import sys, math, string
import numpy as np
usage = 'Usage: %s infile' % sys.argv[0]

try:
    infilename = sys.argv[1]
except:
    print usage; sys.exit(1)

# load file into a list of lines
f = open(infilename, 'r'); lines = f.readlines(); f.close()

# the second line contains the increment in t:
dt = float(lines[1])

# the third line contains the name of the time series:
ynames = lines[2].split()

# store y data in a dictionary of lists of floats:
y = {}           # declare empty dictionary
for name in ynames:
    y[name] = [] # empty list (of y values of a time series)
    
# load data from the rest of the lines:
for line in lines[3:]:
    #############################
    # the next line was added by me, it replaces the old list-based 
    # implementation (yvalues was a list previously)
    #############################
    yvalues = np.array(line.split(), float)
    
    # alternative:  yvalues = map(float, line.split())
    if len(yvalues) == 0: continue  # skip blank lines
    i = 0  # counter for yvalues
    for name in ynames:
        y[name].append(yvalues[i]); i += 1
    
for name in ynames:    
    y[name] = np.array(y[name], float)

print 'y dictionary:\n', y

# write out 2-column files with t and y[name] for each name:
for name in y.keys():
    ofile = open(name+'.dat', 'w')
    for k in range(len(y[name])):
        ofile.write('%12g %12.5e\n' % (k*dt, y[name][k]))
    ofile.close()

# result :

# krisbrox@aeminium ~/Inf3331/uke7krisbrox $ ./convert2.py t.dat 
# y dictionary:
# {'tmp-model2': [1.0, 0.188, 0.25], 'tmp-model1': [0.10000000000000001, 0.10000000000000001, 0.20000000000000001], 'tmp-measurements': [0.0, 0.10000000000000001, 0.20000000000000001]}

# which is the same output as previously.
# note: I used the input file from chapter 2.5
