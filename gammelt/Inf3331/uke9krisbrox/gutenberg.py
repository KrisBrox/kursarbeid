#!/usr/bin/env python
import sys, re

try:
    f = open(sys.argv[1])
except:
    sys.exit('usage: %s <text file>' % sys.argv[0])

words = {}
r = r"([a-z]+)"
for line in f:
    # split the line on whitespace
    tmp = line.split()
    for word in tmp:
	word = word.lower()
	# remove any non-alphabetical characters
	match = re.search(r, word)
	if match:
	    word = match.group(1)

	    if not word in words:
		words[word] = 1
	    else:
		words[word] += 1

for word in sorted(words):
    # print the sorted dictionary
    print word, words[word]

# ran the script on 'beowulf' from project gutenberg,
# part of the result:
"""
beings 1
believe 2
believed 3
belong 1
belonged 7
belongings 1
belongs 1
belov 16
beloved 4
below 4
benc 2
bench 10
benches 2
benchward 1
bend 1
bended 1
benefactor 1
benne 1
bent 5
beowulf 251
beowulfs 1
bequeath 1
bequest 2
bereav 5
bereave 1
bereft 1
"""
