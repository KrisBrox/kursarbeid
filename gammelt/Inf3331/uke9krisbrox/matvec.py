#!/usr/bin/env python
import numpy as np

# make a matrix and a vector
A = np.array([[1, 2, 3], [4, 5, 7], [6, 8, 10]], float)
b = np.array([-3, -2, -1], float)

# print the product
print np.dot(A, b) 

# Result: 

# krisbrox@aeminium ~/Inf3331/uke7krisbrox $ ./matvec.py 
# [-10. -29. -44.]

