#!/usr/bin/env python
import sys, re
from math import *
import numpy as np
try:
    t = sys.argv[1]
    f = sys.argv[2]
except:
    sys.exit('usage: %s start:stop,step func' % sys.argv[0])
rt = r'(\d*):(\d*),(\d*\.?\d*)'
match = re.search(rt, t)
if match:
    x = float(match.group(1))
    stop = int(match.group(2))
    step = float(match.group(3))
out = []
while x <= stop:
    out.append((x, eval(f)))
    x += step
for tuple in out:
    print tuple

# Kjoreeksempel
""" 
krisbrox@ood ~/Inf3331/uke5krisbrox $ ./xygenerator.py 0:50,2.5 x**2
(0.0, 0.0)
(2.5, 6.25)
(5.0, 25.0)
(7.5, 56.25)
(10.0, 100.0)
(12.5, 156.25)
(15.0, 225.0)
(17.5, 306.25)
(20.0, 400.0)
(22.5, 506.25)
(25.0, 625.0)
(27.5, 756.25)
(30.0, 900.0)
(32.5, 1056.25)
(35.0, 1225.0)
(37.5, 1406.25)
(40.0, 1600.0)
(42.5, 1806.25)
(45.0, 2025.0)
(47.5, 2256.25)
(50.0, 2500.0)
"""
