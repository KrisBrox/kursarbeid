#!/usr/bin/env python
import sys, os, re
def gettime(f):
    """
    takes a jpeg header file and returns two tuples containing the date and the time. the line in the header file containing this information must contain the string 'Date/Time' for the function to do anything. I make the assumption that it does because i was not explicitly told by the assignement text not to make it.
    """
    for line in f:
	if 'Date/Time' in line:
	    s = line.split(' ')
	    d = tuple(s[-2].split(':'))
	    t = tuple(s[-1][:-1].split(':'))
	    return d, t

def prefix(d, t, img):
    """
    Takes a date tuple (YYYY,MM,DD) and a time tuple (HH,MM,SS) and a filename and adds the date and time to the filename according to the specifications in the assignement text.
    """
    d = '_'.join(d)
    t = '_'.join(t)
    s = d+'__'+t+'__'
    if not len(img) > len(s):
	os.rename(img, s+img) 

try:
    f = open(sys.argv[1], 'r')
    img = sys.argv[2]
except:
    sys.exit('usage: %s <JPEG header filename> <File to be renamed>' % sys.argv[0])

d, t = gettime(f)
prefix(d, t, img)

# Results:
#krisbrox@aeminium ~/Inf3331/uke5krisbrox $ ls
#html  jhead.sample  join.py  join.pyc  jpegrename.py  s.jpg  xygenerator.py
#krisbrox@aeminium ~/Inf3331/uke5krisbrox $ ./jpegrename.py jhead.sample s.jpg 
#krisbrox@aeminium ~/Inf3331/uke5krisbrox $ ls
#2002_05_19__18_10_03__s.jpg  jhead.sample  join.pyc       xygenerator.py
#html                         join.py       jpegrename.py
#krisbrox@aeminium ~/Inf3331/uke5krisbrox $ ./jpegrename.py jhead.sample *.jpg 
#krisbrox@aeminium ~/Inf3331/uke5krisbrox $ ls
#2002_05_19__18_10_03__s.jpg  jhead.sample  join.pyc       xygenerator.py
#html                         join.py       jpegrename.py

