#!/usr/bin/env python
import sys, re

def join(*args):
    """
    Takes a variable number of arguments, where the first arg
    is a string and the rest are either strings, lists of 
    strings or tuples of strings. joins them all with the 
    first argument inserted between each string.
    """
    ret = ''
    delim = args[0] 
    for arg in args[1:]:
	if isinstance(arg, tuple) or isinstance(arg, list):
	    for s in arg:
		ret += s + delim
	else:
	    ret += arg + delim
    return ret[0:-len(delim)]

## for the sake of demonstration:
list1 = ['s1','s2','s3']
tuple1 = ('s4', 's5')
ex1 = join(' ', 't1', 't2', list1, tuple1, 't3', 't4')
ex2 = join('  #  ', list1, 't0')

print ex1
print ex2
## end demonstration

# result:
#   krisbrox@ood ~/Inf3331/uke5krisbrox $ ./join.py
#   t1 t2 s1 s2 s3 s4 s5 t3 t4
#   s1  #  s2  #  s3  #  t0

