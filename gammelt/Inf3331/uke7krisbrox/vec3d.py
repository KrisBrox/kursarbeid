#!/usr/bin/env python
from math import sqrt

class Vec3D:
    def __init__(self, x, y, z):
	self.x = float(x)
	self.y = float(y)
	self.z = float(z)

    def __str__(self):
	return '('+str(self.x)+', '+str(self.y)+', '+str(self.z)+')' 

    def __repr__(self):
	return 'Vec3D' + str(self)

    def len(self):
	return sqrt(self.x**2+self.y**2+self.z**2)

    def __getitem__(self, i):
	if i not in range(0,3):
	    raise IndexError
	if i == 0:
	    return self.x
	elif i == 1:
	    return self.y
	else:
	    return self.z

    def __setitem__(self, i, val):
	if i not in range(0,3):
	    raise IndexError
	if i == 0:
	    self.x = float(val)
	elif i == 1:
	    self.y = float(val)
	elif i == 2:
	    self.z = float(val)

    def __pow__(self, other):
	return Vec3D(\
	self.y*other.z-self.z*other.y,\
	self.z*other.x-self.x*other.z,\
	self.x*other.y-self.y*other.x
	)

    def __add__(a, b):
	return Vec3D(a.x+b.x, a.y+b.y, a.z+b.z)

    def __sub__(a, b):
	return Vec3D(a.x-b.x, a.y-b.y, a.z-b.z)

    def __mul__(a, b):
	return a.x*b.x + a.y*b.y + a.z*b.z

	


if __name__ == '__main__':
    u = Vec3D(1, 0, 0)  # (1,0,0) vector
    v = Vec3D(0, 1, 0)
    print str(u)
    print repr(v)
    print u.len()
    print u[1]
    v[2] = 2.5
    print u, v
    print u**v
    print repr(u+v)
    print repr(u-v)
    print repr(u*v)

# Result:
"""
krisbrox@aeminium ~/Inf3331/uke7krisbrox $ ./vec3d.py 
(1.0, 0.0, 0.0)
Vec3D(0.0, 1.0, 0.0)
1.0
0.0
2 2.5
(1.0, 0.0, 0.0) (0.0, 1.0, 2.5)
(0.0, -2.5, 1.0)
Vec3D(1.0, 1.0, 2.5)
Vec3D(1.0, -1.0, -2.5)
0.0
"""




