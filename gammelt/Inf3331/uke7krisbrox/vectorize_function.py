#!/usr/bin/env python
from numpy import ndarray, ones, zeros

def initial_condition(x):
    if type(x) == ndarray:
	return 3*ones(x.shape, x.dtype)
    return 3.0

# Testing block
if __name__ == '__main__':
    x = zeros((4,5))
    print initial_condition(x)

# Result:
# krisbrox@aeminium ~/Inf3331/uke7krisbrox $ ./vectorize_function.py 
# [[ 3.  3.  3.  3.  3.]
#  [ 3.  3.  3.  3.  3.]
#  [ 3.  3.  3.  3.  3.]
#  [ 3.  3.  3.  3.  3.]]



