#!/usr/bin/env python
from math import sqrt
import sys
class Vec3D:
    """ Primitive class for three-dimensional vectors
    """
    def __init__(self, x, y, z):
	self.x = float(x)
	self.y = float(y)
	self.z = float(z)

    def __str__(self):
	return '('+str(self.x)+', '+str(self.y)+', '+str(self.z)+')' 

    def __repr__(self):
	return 'Vec3D' + str(self)

    def len(self):
	return sqrt(self.x**2+self.y**2+self.z**2)

    def __getitem__(self, i):
	if i not in range(0,3):
	    raise IndexError
	if i == 0:
	    return self.x
	elif i == 1:
	    return self.y
	else:
	    return self.z

    def __setitem__(self, i, val):
	if i not in range(0,3):
	    raise IndexError
	if i == 0:
	    self.x = float(val)
	elif i == 1:
	    self.y = float(val)
	elif i == 2:
	    self.z = float(val)

    def __pow__(self, other):
	return Vec3D(\
	self.y*other.z-self.z*other.y,\
	self.z*other.x-self.x*other.z,\
	self.x*other.y-self.y*other.x
	)

    def __add__(a, b):
	if type(b) == float or type(b) == int:
	    return b+a
	return Vec3D(a.x+b.x, a.y+b.y, a.z+b.z)

    def __radd__(self, other):
	other = Vec3D(other, other, other)
	return self+other

    def __rsub__(self, other):
	other = Vec3D(other, other, other)
	return other-self

    def __sub__(a, b):
	if type(b) == float or type(b) == int:
	    return b-a
	return Vec3D(a.x-b.x, a.y-b.y, a.z-b.z)

    def __rmul__(self, other):
	return self*other

    def __div__(a, b):
	try:
	    return Vec3D(a.x/b, a.y/b, a.z/b)
	except:
	    sys.exit('Division by zero!')

    def __mul__(a, b):
	if type(b) == float or type(b) == int:
	    return Vec3D(a.x*b, a.y*b, a.z*b)
	return a.x*b.x + a.y*b.y + a.z*b.z

# Testing block:
if __name__ == '__main__':
    u = Vec3D(1, 0, 0)  
    v = Vec3D(0, -0.2, 8)
    a = 1.2 
    print repr(u+v)
    print repr(a+v)
    print repr(v+a)
    print a-v
    print v-a
    print a*v
    print v*a
    print v/a

# Result:
"""
krisbrox@aeminium ~/Inf3331/uke7krisbrox $ ./vec3d_ext.py 
Vec3D(1.0, -0.2, 8.0)
Vec3D(1.2, 1.0, 9.2)
Vec3D(1.2, 1.0, 9.2)
(1.2, 1.4, -6.8)
(1.2, 1.4, -6.8)
(0.0, -0.24, 9.6)
(0.0, -0.24, 9.6)
(0.0, -0.166666666667, 6.66666666667)
"""
