#!/usr/bin/python
from math import log
import sys
print 'Hello, world!'
for i in range(1, len(sys.argv)):
    f = float(sys.argv[i])
    if f<0:
	print 'ln(%s) is illegal' % (sys.argv[i])
	continue
    print 'ln(%s)=%f' % (sys.argv[i], log(f))

