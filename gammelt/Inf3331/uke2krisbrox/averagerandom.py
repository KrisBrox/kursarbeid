#!/usr/bin/python
from random import uniform
import sys
n = int(sys.argv[1])
sum=0
count=0
for i in range(0,n):
    sum += uniform(-1, 1)
    count += 1

print 'average of %d random nrs: %.4f' % (count, sum/count)
