#!/usr/bin/env python
import numpy as np
import time

def num_solv_2o(f, n):
    """
    solver for 2nd-order linear PDE's. (or at least the one in ex.3)
    """
    h = (1/float(n+1))
    L = np.zeros((n,n), float)
    for i in xrange(1,n-1):
	L[i-1,i] = -1
	L[i, i]	= 2 + h*h
	L[i+1, i] = -1
    L[0,0] = 2+h*h
    L[1,0] = -1
    L[n-1, n-1] = 2+h*h
    L[n-2, n-1] = -1

    f_vec = h*h*f(np.linspace(0, 1, n))

    return np.linalg.solve(L,f_vec)


k = 1
f = lambda x : np.sin(np.pi*k*x) 
print num_solv_2o(f, 30)

for k in (1, 10, 100):
    for n in (100, 1000, 10000):
	f = lambda x : np.sin(np.pi*k*x) 
	t = time.time()
	num_solv_2o(f, n)
	t = time.time() - t
	print "seconds spent solving with k=%d,n=%d: %g" % (k,n,t) 

""" Result:
krisbrox@proamon ~/infmat3360/oblig1 $ ./num_sol.py 
[ 0.00868711  0.01738325  0.02598498  0.03439005  0.04249865  0.05021454
  0.05744621  0.06410792  0.07012073  0.07541342  0.0799233   0.08359699
  0.08639098  0.08827222  0.08921843  0.08921843  0.08827222  0.08639098
  0.08359699  0.0799233   0.07541342  0.07012073  0.06410792  0.05744621
  0.05021454  0.04249865  0.03439005  0.02598498  0.01738325  0.00868711]
seconds spent solving with k=1,n=100: 0.000465155
seconds spent solving with k=1,n=1000: 0.0948651
seconds spent solving with k=1,n=10000: 59.1894
seconds spent solving with k=10,n=100: 0.000495911
seconds spent solving with k=10,n=1000: 0.080822
seconds spent solving with k=10,n=10000: 59.6141
seconds spent solving with k=100,n=100: 0.000591993
seconds spent solving with k=100,n=1000: 0.081316
seconds spent solving with k=100,n=10000: 59.1015
"""
