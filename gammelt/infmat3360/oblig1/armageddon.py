#!/usr/bin/env python
import numpy as np
def armageddon(n, alpha, beta, xi=0):
    h = 1/float(1+n) 
    
    s_prev = 1000
    i_prev = 0
    z_prev = 2
    r_prev = 0
    sigma_l = 0
    delta = 0.001
    rho = 0.1
    sigma = 0.001
    s=i=r=z=0
    
    for i in np.arange(0, n, h):
	s = s_prev + h*( sigma_l -beta*s_prev*z_prev - delta*s_prev)
	i = i_prev + h*( beta*s_prev*z_prev -rho*i_prev - delta*i_prev)
	z = z_prev + h*( rho*i_prev-alpha*s_prev*z_prev + xi*r_prev - sigma*z_prev)
	r = r_prev + h*( delta*s_prev + delta*i_prev - xi*r_prev + alpha*s_prev*z_prev)
	s_prev = s
	i_prev = i
	z_prev = z
	r_prev = r
    return (s, i, z, r)

#a)
print armageddon(100, 0.06, 0.03, 0.001),"\n", armageddon(100, 0.06, 0.03) 
""" Results in:
(898.73630855254407, 0.82171935716925915, 0.0034227249648074824, 102.43832387103379) 
(903.03342259647548, 0.0061151463563176066, 1.1296511479044465e-05, 98.960384461084317)
"""
#b)
print armageddon(100, 0.03, 0.06)
""" Results in:
(3.9525251667299724e-323, 2.7524876098208013, 451.47567137726838, 528.62976494489965)
"""


    


