#!/usr/bin/env python
import numpy as np

def b_euler(a, n, init):
    h = 1/float(n+1)
    y_prev = init

    for i in range(1, n):
	y_next = y_prev/float(1-h*a) 
	y_prev = y_next

    return abs(y_next-np.exp(a))

for a in (-1000, -10, -1, 1, 10, 1000):
    print b_euler(a, 30, 1)

"""
krisbrox@osiran ~/infmat3360/oblig2 $ ./b_euler_comp.py 
7.32803994458e-45
0.000255732325231
0.0303558553393
0.130231400197
58350.1236004
inf
"""
