#!/usr/bin/env python

def b_euler(a, n, init):
    h = 1/float(n+1)
    y_prev = init
    t = h

    for i in range(1, n):
	y_next = y_prev/float(1-h*a)  # + h * f(y_prev)
	y_prev = y_next

    return y_next

for a in (-1000, -10, -1, 1, 10, 1000):
    print b_euler(a, 30, 1)

"""
krisbrox@pel ~/infmat3360/oblig2 $ ./b_euler.py 
7.32803994458e-45
0.000301132254994
0.398235296511
2.58805042826
80376.5893952
-4.42689872568e-44 # her har jeg aapenbart faatt overflow (e^1000 er et 
    ganske stort tall...).
"""
