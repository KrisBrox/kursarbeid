#!/usr/bin/env python
import numpy as np

def num_solv_2o(f,a_f, n, c):
    h = (1/float(n+1))
    L = np.zeros((n,n), float)
    for i in xrange(1,n-1):
	L[i,i-1] = -1
	L[i, i]	= c*h + 2
	L[i, i+1] = -c*h -1
    L[0,0] = c*h + 2
    L[1,0] = -1
    L[0,1] = -c*h -1
    L[n-1, n-1] = c*h + 2
    L[n-2, n-1] = -1 - c*h
    L[n-1, n-2] = -1

    f_vec = h*h*f(np.linspace(0, 1, n))
    f_a_vec = a_f(np.linspace(0, 1, n))

    return abs(np.linalg.solve(L,f_vec) - f_a_vec)


k = 1
c = 0.5
n = 1000

f = lambda x : k*k*np.pi**2*np.sin(np.pi*k*x) - c*k*np.pi*np.cos(k*np.pi*x) 
a_f = lambda x : np.sin(np.pi*k*x) 
res = num_solv_2o(f,a_f, 30, c)

print res

"""
krisbrox@proamon ~/infmat3360/oblig1 $ ./num_sol2_comp.py 
[ 0.09325624  0.0785218   0.06407906  0.0500881   0.03670387  0.02407433
  0.01233873  0.00162601  0.00794677  0.01627577  0.02327161  0.0288604
  0.03298461  0.03560377  0.03669492  0.03625289  0.03429037  0.03083776
  0.02594281  0.01967005  0.01210007  0.00332856  0.00653485  0.01736778
  0.02903664  0.04139812  0.05430089  0.06758739  0.08109566  0.09466121]
"""

