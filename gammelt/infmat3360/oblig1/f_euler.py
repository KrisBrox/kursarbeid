#!/usr/bin/env python
def forward_euler(f, n, init):

    h = 1/float(n+1)
    y_prev = init

    for i in range(1, n):
        y = y_prev + h*f(y_prev)
        y_prev = y

    return y

for a in (-1000, -10, -1, 1, 10, 1000):
    f = lambda u : a*u
    print forward_euler(f, 30, 1)

"""
krisbrox@pel ~/infmat3360/oblig2 $ ./f_euler.py 
-2.2589177254e+43 # her har det �penbart skjedd noe rart. 
1.24414336006e-05
0.386391234529
2.51107827147
3320.80002529
1.36462138247e+44
"""
