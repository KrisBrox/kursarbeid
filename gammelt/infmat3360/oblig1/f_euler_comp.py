#!/usr/bin/env python
import numpy as np

def forward_euler(f, n, init):
    h = 1/float(n+1)
    y_prev = init

    for i in range(1, n):
        y = y_prev + h*f(y_prev)
        y_prev = y

    return y

for a in (-1000, -10, -1, 1, 10, 1000):
    f = lambda u : a*u
    print abs(forward_euler(f, 30, 1) - np.exp(a))

"""
krisbrox@osiran ~/infmat3360/oblig2 $ ./f_euler_comp.py 
2.2589177254e+43
3.29584961619e-05
0.0185117933578
0.20720355699
18705.6657695
inf
"""
