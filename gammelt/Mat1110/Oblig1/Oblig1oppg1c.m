function [u,v,w,z] = Oblig1oppg1c(K)
% Parameteren a styrer hvilken vannrett og loddrett linje man
% tegner bildet av. Variablene u og v beskriver bildet av de
% loddrette linjene, mens variablene w og z beskriver bildet
% av de vannrette linjene. For-l�kken itereres 1000 ganger for � f� mange
% nok punkter til � gj�re grafen jevn.
for m = 1:K+1
    a = (m-1)/K;
    for n = 1:1001
        t(n) = (n-1)*0.001;
        u(n) = a^3-2*t(n);
        v(n) = a+t(n)^3;
        w(n) = t(n)^3-2*a;
        z(n) = t(n)+a^3;
    end
    plot(u,v)
    axis equal
    hold on
    plot(w,z,'r')
end
hold off
end
