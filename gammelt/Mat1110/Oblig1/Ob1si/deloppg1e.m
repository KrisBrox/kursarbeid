function [u,v,w,z] = deloppg1e(K)
K=100
for m = 1:K+1
    a = (m-1)/K;
    for n = 1:1001
            t(n) = (n-1)*0.001;
            u(n) = (3/4)*a-2*t(n)-(1/4);
            v(n) = a+(3/4)*t(n)-(1/4);
            w(n) = (3/4)*t(n)-2*a-(1/4);
            z(n) = t(n)+(3/4)*a-(1/4);
    end
    plot(u,v,'k')
    axis equal
    hold on
    plot(w,z,'k')
end
hold off
end