function [X, Y] = Oblig2(N)
    h = 0.001;
    X = zeros(1000*N+1, 1)';
    Y = zeros(1000*N+1, 1)';
    X(1) = 1;
    X(2) = 1+2*h;
    Y(1) = 2;
    Y(2) = 2-h;
    for m=2:1000*N
        X(m+1) = 2*X(m)-X(m-1)-18*X(m)*h*h+9*Y(m)*h*h;
        Y(m+1) = 2*Y(m)-Y(m-1)+9*X(m)*h*h-18*Y(m)*h*h+27*h*h;
    end
end

% Implementerer den numeriske tilnærmingen beskrevet i oppgave1.