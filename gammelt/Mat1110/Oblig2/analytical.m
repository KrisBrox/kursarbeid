function [x, y] = analytical(N)
    t = linspace(1,30, 1000*N+1);
    
    x = 1.+(1/6).*sin(3*t)+(1./(2.*sqrt(3))).*sin(sqrt(27).*t);
    y = 2.+(1/6).*sin(3*t)-(1./(2.*sqrt(3))).*sin(sqrt(27).*t);
end
