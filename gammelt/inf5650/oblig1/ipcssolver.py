#!/usr/bin/env python
from dolfin import *
from coupledsolver import CoupledFluidFlowSolver
class IpcsFluidFlowSolver(CoupledFluidFlowSolver):

    def define_problem(self, mesh, bc_func, degree = 1):
	self.set_mesh(mesh)
	self.set_degree(degree)
	self.V = VectorFunctionSpace(self.mesh, 'CG', self.degree+1)
	self.Q = FunctionSpace(self.mesh, 'CG', self.degree)
	self.bcs = bc_func(self.V)

    def solve_it(self, nu=1e-3,dt = 0.1, T = 5, bc_update=None, \
		f = Constant((0,0)), beta = 0, Re=None): 
	V = self.V; Q = self.Q; bcs = self.bcs
	u = TrialFunction(V); v = TestFunction(V)
	p = TrialFunction(Q); q = TestFunction(Q)
	
	u1 = Function(V); p1 = Function(Q)
	u_ = Function(V); 
	u0 = interpolate(Constant((1,1)), V)
	p0 = interpolate(Constant(1), Q)
	f = interpolate(f, V)
	nu = Constant(nu)

	n = FacetNormal(self.mesh)

	epsilon = lambda u : 0.5*(nabla_grad(u)+nabla_grad(u).T)
	sigma = lambda u, p, nu : 2*nu*epsilon(u)-p*Identity(u.cell().d)

	U = 0.5*(u0+u)
	F1 = (1./dt)*inner(u-u0,v)*dx \
	    + inner(grad(u0)*u, v)*dx \
	    + inner(sigma(U, p0, nu),epsilon(v))*dx \
	    + inner(p0*n, v)*ds \
	    -beta*nu*inner(grad(U).T*n, v)*ds \
	    -inner(f, v)*dx

	a1 = lhs(F1); L1 = rhs(F1)

	a2 = inner(grad(p), grad(q))*dx
	L2 = inner(grad(p0),grad(q))*dx \
	    - (1.0/dt)*div(u1)*q*dx

	a3 = inner(u,v)*dx
	L3 = inner(u1, v)*dx - dt*inner(grad(p1-p0), v)*dx

	t = 0; n = 0
	plot_dummy = Function(V)
	while t < T:
	    if bc_update != None: bc_update(t)
	    solve(a1==L1, u1, bcs)
	    solve(a2==L2, p1, None)
	    solve(a3==L3, u_, bcs)
	    t += dt
	    u0.assign(u_)
	    plot_dummy.assign(u_)
	    plot(plot_dummy)



