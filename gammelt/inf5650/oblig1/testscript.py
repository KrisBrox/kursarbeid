#!/usr/bin/env python
from dolfin import *
from ipcssolver import IpcsFluidFlowSolver
from coupledsolver import CoupledFluidFlowSolver
if True:
    x0, y0 = (0.,-1.)
    x1, y1 = (8., 1.)
    xs, ys, r = (1., 0., 0.25)
    rotatingcylindermesh = Mesh('cylinder.xml')

    def cyl(x, on_boundary):
	return sqrt((x[0]-xs)*(x[0]-xs)+x[1]*x[1]) < r+DOLFIN_EPS \
		    and on_boundary
    def left(x, on_boundary):
	return x[0] < DOLFIN_EPS and on_boundary
    def sides(x, on_boundary):
	y = False
	if x[1] >= 1-DOLFIN_EPS:    y = True
	elif x[1] <= -1+DOLFIN_EPS: y = True
	return y and on_boundary

    cylspeed = Expression(('-a*x[1]','a*(x[0]-xs)'), a = 0, xs=xs)

    def bc_funcCylinderCoupled(VQ):
	leftspeed = Expression(('1-x[1]*x[1]','0'))
	bcC = DirichletBC(VQ.sub(0), cylspeed, cyl)
	bcL = DirichletBC(VQ.sub(0), leftspeed, left)
	bcS = DirichletBC(VQ.sub(0), Constant((0,0)), sides)
	return [bcC, bcL, bcS]
    def bc_funcCylinderIpcs(V):
	leftspeed = Expression(('1-x[1]*x[1]','0'))
	bcC = DirichletBC(V, cylspeed, cyl)
	bcL = DirichletBC(V, leftspeed, left)
	bcS = DirichletBC(V, Constant((0,0)), sides)
	return [bcC, bcL, bcS]

    def bc_update(t):
	cylspeed.a = 2*sin(t*pi)

   
    ### Running the program
    def go(problem = 'rotatingcylinder', coupled = True, \
		dt=0.1, Re=1000, nu=1e-2, beta=0, T = 5, viz=None):

	if problem == 'rotatingcylinder':
	    mesh = rotatingcylindermesh
	    if coupled:
		s = CoupledFluidFlowSolver()
		bc_func = bc_funcCylinderCoupled
	    else:
		s = IpcsFluidFlowSolver()
		bc_func = bc_funcCylinderIpcs

	    s.define_problem(mesh, bc_func)
	    s.solve_it(dt = dt, nu=nu, Re=Re, beta = beta, \
		    bc_update=bc_update, T=T, viz = viz)

def viz(u, n):
    f= File('u%d.pvd'%n)
    f << u



go(dt=0.05, Re=500, nu=2e-2, beta = 0, T = 2, coupled = True, viz = None)

#go(dt=0.1, Re=1000, nu=2e-2, beta = 0, T = 2, coupled = True, \
#	viz = None, problem = 'poiseuille')
