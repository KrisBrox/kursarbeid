#!/usr/bin/env python
from dolfin import *
set_log_active(False)

def cylinder(x, on_bound):
    y = (x[0]-0.2)*(x[0]-0.2)+(x[1]-0.2)*(x[1]-0.2) \
	    <= 0.05*0.05 + DOLFIN_EPS
    return on_bound and y    
def sides(x, on_bound):
    y = near(x[1],0) or near(x[1], 0.41)
    return y and on_bound
def inlet(x, on_bound):
    return near(x[0],0) and on_bound
def outlet(x, on_bound):
    return near(x[0], 2.2) and on_bound

cylinder_speed = Expression(('-a*x[1]','a*(x[0]-0.2)'),a=0 )
Um = 1.5; H = 0.41; H2 = H**2
xs, ys, r = (0.2, 0.2, 0.05); x0, y0 = (0, 0)
x1, y1 = (2.2, 0.41); mesh = Mesh('cylinder.xml')
inletspeed = Expression(('4*Um*x[1]*(H-x[1])*sin(pi*t/8.)/H2','0'), 
		Um=Um,H=H,H2=H2,t=0)

def ipcs_cylinder(degree=1, nu=1e-03, dt = 0.01, T = 10, beta = 1, n=50, save_mov=False):
    """ Simulates 2D fluid flow around a cylinder, as per 21.4.5 in the fenics book. 
    """
    mesh = Mesh(Rectangle(0,0,2.2,0.41) - Circle(0.2,0.2,0.05,5*n), n)
    V = VectorFunctionSpace(mesh, 'CG', 1+degree)
    Q = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V); v = TestFunction(V)
    p = TrialFunction(Q); q = TestFunction(Q)
    #tentative
    u1 = Function(V); p1 = Function(Q)
    #prev
    u0 = interpolate(Constant((0,0)), V)
    p0 = interpolate(Constant(0), Q)
    #prevprev
    u00 = interpolate(Constant((0,0)), V)


    bcI = DirichletBC(V, inletspeed, inlet)
    bcS = DirichletBC(V, Constant((0,0)), sides)
    bcC = DirichletBC(V, Constant((0,0)), cylinder)
    bcu = [bcI, bcS, bcC]
    bcp = DirichletBC(Q, Constant(0), outlet)


    epsilon = lambda u : 0.5*(nabla_grad(u)+nabla_grad(u).T)
    sigma = lambda u, p, nu : 2*nu*epsilon(u)-p*Identity(u.cell().d)
    n = FacetNormal(mesh)

    U = 0.5*(u0+u)
    u_ab = 1.5*u0 - 0.5*u00

    F1 = (1./dt)*inner(u-u0,v)*dx \
        + inner(grad(U)*u_ab, v)*dx \
        + inner(sigma(U, p0, nu),epsilon(v))*dx \
        + inner(p0*n, v)*ds \
        - beta*nu*inner(grad(U).T*n, v)*ds \

    a1 = lhs(F1); L1 = rhs(F1)
    
    a2 = inner(grad(p), grad(q))*dx
    L2 = inner(grad(p0),grad(q))*dx - (1./dt)*div(u1)*q*dx
    
    a3 = inner(u,v)*dx
    L3 = inner(u1, v)*dx - dt*inner(grad(p1-p0), v)*dx

    t = 0; k=0
    plot_dummy = Function(V)
    u_ = Function(V)
    while t <= T:
        solve(a1==L1, u1, bcu)
        solve(a2==L2, p1, bcp)
        solve(a3==L3, u_, bcu)
        t += dt
	inletspeed.t=t
        u0.assign(u_)
        plot_dummy.assign(u_)
        plot(plot_dummy, title='t=%.3f'%(t-dt))
    
    return p1(0.15,0.2)-p1(0.25,0.2)

def coupled_cylinder(degree=1, Re=1e+03, dt = 0.025, T = 10, n=50):
    mesh = Mesh(Rectangle(0,0,2.2,0.41) - Circle(0.2,0.2,0.05,5*n), n)
    V = VectorFunctionSpace(mesh, 'CG', 1+degree)
    Q = FunctionSpace(mesh, 'CG', 1)
    VQ = V*Q
    u, p = TrialFunctions(VQ)
    v, q = TestFunctions(VQ)
    up_1 = Function(VQ)
    u_1, p_1 = split(up_1)

    bcI = DirichletBC(VQ.sub(0), inletspeed, inlet)
    bcS = DirichletBC(VQ.sub(0), Constant((0,0)), sides)
    bcC = DirichletBC(VQ.sub(0), cylinder_speed, cylinder)
    bcu = [bcI, bcS, bcC]

    up_ = Function(VQ)
    up_1 = interpolate(Constant((0,0,0)), VQ)

    #mf = FacetFunction(mesh, 'size_t')
    

    F = Re**-1*inner(nabla_grad(u),nabla_grad(v))*dx \
        - inner(p, div(v))*dx \
        - inner(q, div(u))*dx \
	+ inner(dot(u_1,nabla_grad(u)),v)*dx \
	+ dt**-1*inner((u-u_1),v)*dx
#	+ -1*v*n*ds

    u_, p_ = up_.split()
    plotter = Function(V)
    t=0
    while t <= T:
	solve(lhs(F)==rhs(F), up_, bcu)
	t += dt
	inletspeed.t = 3*t
	up_1.assign(up_)
	plotter.assign(u_)
	plot(plotter, title='t=%.3f'%t)
    return p_(0.15,0.2)-p_(0.25,0.2)

if __name__=='__main__':
    #print ipcs_cylinder(T = 8, degree = 1, dt = 0.01, nu=1e-03, save_mov=False)
    print coupled_cylinder(T = 8, degree = 1, dt = 0.01, Re = 1e+3, n=55)
