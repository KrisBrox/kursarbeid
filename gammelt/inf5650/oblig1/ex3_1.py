#!/usr/bin/env python
from dolfin import *

def boundary(x, on_boundary): return on_boundary
n = 1000
mesh = UnitIntervalMesh(n)

V = FunctionSpace(mesh, 'CG', 1)
u = TrialFunction(V)
v = TestFunction(V)

bc = DirichletBC(V, Constant(0), boundary)
A, _ = assemble_system(inner(grad(u),grad(v))*dx, Constant(0)*v*dx, bc)
M, _ = assemble_system(u*v*dx, Constant(0)*v*dx, bc)

import pytave
from numpy import matrix, diagflat, sqrt
from numpy import linalg

e, l = pytave.feval(2, "eig", A.array(), M.array())
e = matrix(e)
l = matrix(l)

k = 9
for expr in ['sin(k*pi*x[0])','cos(k*pi*x[0])']:
    print '\n',expr, ', k=%d'%k
    print 'Unit interval mesh n = %d'%n
    u_ex = Expression(expr,k=k)
    u = interpolate(u_ex, V)
    x = matrix(u.vector().array())
    ex = e.T*x.T

    L2_norm = sqrt(assemble(u*u*dx))
    print 'L2 norm = %.4f'%L2_norm

    H1_seminorm = pi*k*sqrt(2)/2.
    #print 'H1 seminorm = %.4f'%H1_seminorm
    H1_seminorm = sqrt(assemble(inner(grad(u),grad(u))*dx))
    print 'H1 seminorm = %.4f'%H1_seminorm
    H1_seminorm = sqrt(ex.T*l*ex)/float(len(ex))
    #print 'H1 seminorm = %.4f'%H1_seminorm

    Hm1_norm = sqrt(ex.T*l**-1*ex)/float(len(ex))
    print 'Hm1 seminorm: %.4f' % Hm1_norm[0][0]


    
