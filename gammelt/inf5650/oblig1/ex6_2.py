#!/usr/bin/env python
from dolfin import *
from numpy import linalg
set_log_active(False)
def ceatest(alpha = 1, t = 2):
    degree = t-1
    mesh = UnitIntervalMesh(100)
    V = FunctionSpace(mesh, 'CG', degree)

    u = TrialFunction(V)
    v = TestFunction(V)
    f = Expression('a*pi*pi*sin(pi*x[0])-pi*cos(pi*x[0])',a=alpha)

    a = alpha*inner(grad(u),grad(v))*dx - dot(u.dx(0),v)*dx
    b = f*v*dx
    
    def boundary(x,on_boundary): return on_boundary
    bc = DirichletBC(V, Constant(0), boundary)
    u_ = Function(V)
    solve(a==b, u_, bc)

    h2_norm = (pi**4/2.)**0.5

    u_e = Expression('sin(pi*x[0])')
    h = mesh.hmin()**(t-1)
    diff = errornorm(u_e, u_, norm_type='h10',mesh=mesh)
    Ch = diff/(h**(t-1)*h2_norm)
    print Ch

for alpha in (10.,5.,1.,0.5,0.1,0.01):
    ceatest(alpha = alpha)
