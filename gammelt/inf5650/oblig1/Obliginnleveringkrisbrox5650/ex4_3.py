#!/usr/bin/env python
from dolfin import *

mesh = UnitIntervalMesh(100)
V = FunctionSpace(mesh, 'CG', 1)

u = TrialFunction(V)
v = TestFunction(V)
for f in ('sin(10*pi*x[0])','cos(10*pi*x[0])'):
    f = Expression(f)

    a = inner(grad(u),grad(v))*dx
    b = v*f*dx

    u_ = Function(V)
    solve(a==b, u_)

    plot(u_, interactive = True)
