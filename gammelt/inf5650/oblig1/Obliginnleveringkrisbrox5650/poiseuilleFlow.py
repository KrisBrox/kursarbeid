#!/usr/bin/env python
from dolfin import *
set_log_active(False)

def sides(x, on_boundary):
    y = False
    if x[1]<DOLFIN_EPS or x[1] > 1-DOLFIN_EPS: y = True
    return y and on_boundary

def inlet(x, on_boundary):
    return x[0]<DOLFIN_EPS and on_boundary

def outlet(x, on_boundary):
    return x[0]>=1-DOLFIN_EPS and on_boundary

Left = AutoSubDomain(inlet)
Right = AutoSubDomain(outlet)

def poiseuilleCoupled(degree=1, Re=8, T = 0.5, dt = 0.05, n=50):
    mesh = UnitSquareMesh(n,n)
    V = VectorFunctionSpace(mesh, 'CG', 1+degree)
    Q = FunctionSpace(mesh, 'CG', 1)
    VQ = V*Q

    bcs = DirichletBC(VQ.sub(0), Constant((0,0)), sides)
    u, p = TrialFunctions(VQ)
    v, q = TestFunctions(VQ)
    up_ = Function(VQ)
    up_1 = Function(VQ)
    u_1, p_1 = split(up_1)
    up_1.assign(Constant((0,0,0)))

    mf = FacetFunction('size_t',mesh)
    mf.set_all(0)
    Left.mark(mf,1)
    Right.mark(mf,2)
    ds = Measure("ds")[mf]

    n = FacetNormal(mesh)
    F = Re**-1*inner(nabla_grad(u),nabla_grad(v))*dx \
	- inner(p, div(v))*dx \
	- inner(q, div(u))*dx \
	+ inner(dot(u_1,nabla_grad(u)),v)*dx \
	+ dt**-1*inner((u-u_1),v)*dx \
	+ dot(v, n)*ds(1)	

    t = 0
    u_, p_ = up_.split()
    plot_dummy = Function(V)
    while t < T:
	solve(lhs(F)==rhs(F), up_, bcs)
	up_1.assign(up_)
	
	plot_dummy.assign(u_)
	plot(plot_dummy)
	t += dt
    return u_(1,0.5)[0]

def poiseuilleIpcs(degree=1, T=0.5,dt=0.025, nu = 0.125):
    beta = 1
    mesh = UnitSquareMesh(54,54)
    V = VectorFunctionSpace(mesh, 'CG',1+degree)
    Q = FunctionSpace(mesh, 'CG', 1)

    u = TrialFunction(V); v = TestFunction(V)
    p = TrialFunction(Q); q = TestFunction(Q)
    u0 = interpolate(Constant((0,0)), V)
    p0 = interpolate(Constant(0), Q)
    u1 = interpolate(Constant((0,0)), V)
    p1 = interpolate(Constant(0), Q)
    
    n = FacetNormal(mesh)
    epsilon = lambda u : 0.5*(nabla_grad(u)+nabla_grad(u).T)
    sigma = lambda u, p, nu : 2*nu*epsilon(u)-p*Identity(u.cell().d)

    bcU= DirichletBC(V, Constant((0,0)), sides)
    bcI = DirichletBC(Q, Constant(1), inlet)
    bcO = DirichletBC(Q, Constant(0), outlet)
    bcP = [ bcI, bcO]

    U = 0.5*(u0+u)
    F1 = 1/dt*inner(u-u0,v)*dx \
	+ inner(grad(u0)*u,v)*dx \
	+ inner(sigma(U, p0, nu), epsilon(v))*dx \
	+ inner(p0*n, v)*ds \
	- beta*nu*inner(grad(U).T*n, v)*ds

    a1 = lhs(F1); L1 = rhs(F1)

    a2 = inner(grad(p), grad(q))*dx
    L2 = inner(grad(p0), grad(q))*dx \
	- 1/dt*div(u1)*q*dx

    a3 = inner(u, v)*dx
    L3 = inner(u1,v)*dx-dt*inner(grad(p1-p0),v)*dx

    t = 0
    plotter = Function(V)
    u_ = Function(V)
    while t < T:
	solve(a1 == L1, u1, bcU)
	solve(a2 == L2, p1, bcP)
	solve(a3 == L3, u_, bcU)
	t += dt
	u0.assign(u_)
	plotter.assign(u_)
	plot(plotter, title='t=%.3f'%t)
    return u_(1,0.5)[0]

if __name__=='__main__':
    #print poiseuilleCoupled( degree = 0)
    print poiseuilleIpcs( degree = 0)
