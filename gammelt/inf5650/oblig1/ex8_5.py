#!/usr/bin/env python
from dolfin import *
from time import time

def test_order_optimal(u, v, p, q, f, bcs, W):
    # COPIED CODE
    a = inner(grad(u), grad(v))*dx + div(v)*p*dx + q*div(u)*dx
    L = inner(f, v)*dx

    # Form for use in constructing precondtioner matrix
    b = inner(grad(u), grad(v))*dx + p*q*dx

    # Assemble the system
    A, bb = assemble_system(a, L, bcs)

    # Assemble preconditioner system
    P, btmp = assemble_system(b, L, bcs)

    # Create Krylov solver and AMG preconditioner and set params
    solver = KrylovSolver("tfqmr", "amg")
    solver.parameters["relative_tolerance"] = 1.0e-8
    solver.parameters["absolute_tolerance"] = 1.0e-8
    solver.parameters["monitor_convergence"] = False
    solver.parameters["report"] = False
    solver.parameters["maximum_iterations"] = 50000

    # Associate operator (A) and preconditioner matrix (P)
    solver.set_operators(A, P)

    # Solve
    U = Function(W)
    return solver.solve(U.vector(), bb)
    #u_, p_ = split(U)
    #plot(u_, interactive = True)
    # /END COPIED CODE

times = []
for n in [25, 50, 100]:
    mesh = UnitSquareMesh(n, n)
    V = VectorFunctionSpace(mesh, 'CG', 2)
    Q = FunctionSpace(mesh, 'CG', 1)
    W = V*Q
    u, p = TrialFunctions(W)
    v, q = TestFunctions(W)
    f = Expression(('0','0'))
    def top(x, on_boundary):
	return x[1] > 1-DOLFIN_EPS and on_boundary
    def other(x, on_boundary):
	return not top(x, on_boundary) and on_boundary
    bcT = DirichletBC(W.sub(0), Constant((1,0)), top)
    bcO = DirichletBC(W.sub(0), Constant((0,0)), other)
    bcs = [bcT, bcO]
    print test_order_optimal(u, v, p, q, f, bcs, W)




