#!/usr/bin/env python
from dolfin import *
set_log_active(False)

class CoupledFluidFlowSolver():
    def __init__(self, mesh = None, bc_func = None, degree = 1):
	if mesh != None and bc_func != None:
	    self.define_poblem(mesh, bc_func, degree)
	elif degree != 1: 
	    self.set_degree(degree)

    def set_mesh(self, mesh): self.mesh = mesh

    def set_bcs(self, bcs): self.bcs = bcs

    def set_degree(self, degree): self.degree = degree

    def define_problem(self, mesh, bc_func, degree=1):
	self.set_mesh(mesh)
	self.set_degree(degree)
	self.V = VectorFunctionSpace(self.mesh, 'CG', self.degree+1)
	self.Q = FunctionSpace(self.mesh, 'CG', self.degree)
	self.VQ = self.V*self.Q
	self.bcs = bc_func(self.VQ)

    def solve_it(self, Re=1e+3, dt = 0.05, T = 5, creeping = False, \
		    bc_update = None, viz = None,nu=0, beta=None):
	VQ = self.VQ
	u, p = TrialFunctions(VQ)
	v, q = TestFunctions(VQ)
	up_ = Function(VQ)
	up_1 = Function(VQ)
	u_1, p_1 = split(up_1)

	F = Re**-1*inner(nabla_grad(u),nabla_grad(v))*dx \
	    - inner(p, div(v))*dx \
	    - inner(q, div(u))*dx

	solve(lhs(F)==rhs(F), up_, self.bcs)
	self.creepingsolution = up_
	if creeping: return
    	
	up_1.assign(up_)
#	F = Re**-1*inner(nabla_grad(u),nabla_grad(v))*dx \
#	    - inner(p, div(v))*dx \
#	    - inner(q, div(u))*dx \
	F =    + inner(dot(u_1,nabla_grad(u)),v)*dx \
	    + dt**-1*inner((u-u_1),v)*dx

	a1 = assemble(Re**-1*inner(nabla_grad(u),nabla_grad(v))*dx \
	    - inner(p, div(v))*dx - inner(q,div(u))*dx )

	n = 0; t = 0.
	u_, p_ = up_.split()
	plot_dummy = Function(self.V)
	while t < T:
	    t += dt
	    if bc_update != None: bc_update(t)
	    a2 = assemble(lhs(F))
	    a2.axpy(1.0,a1,True)
	    b = assemble(rhs(F))
	    for bc in self.bcs:
		bc.apply(a2,b)
	    solve(a2,up_.vector(), b)
	    #solve(lhs(F)==rhs(F), up_, self.bcs)
	    up_1.assign(up_)
	    
	    plot_dummy.assign(u_)
	    if n%1 == 0:
		plot(plot_dummy)
		if viz != None: viz(plot_dummy, n)
	    n += 1

if __name__ == '__main__':
    x0, y0 = (0.,-1.)
    x1, y1 = (6., 1.)
    xs, ys, r = (1., 0., 0.25)
    mesh = Mesh(Rectangle(x0, y0, x1, y1) - Circle(xs, ys, r, 200), 40)

    def cyl(x, on_boundary):
	return sqrt((x[0]-xs)*(x[0]-xs)+x[1]*x[1]) < r+DOLFIN_EPS \
		    and on_boundary
    def left(x, on_boundary):
	return x[0] < DOLFIN_EPS and on_boundary
    def sides(x, on_boundary):
	y = False
	if x[1] >= 1-DOLFIN_EPS: 
	    y = True
	elif x[1] <= -1+DOLFIN_EPS: 
	    y = True
	return y and on_boundary

    a = 4
    s = CoupledFluidFlowSolver()
    dt = 0.05
    cylspeed = Expression(('-a*x[1]','a*(x[0]-xs)'), a = 0, xs=xs)

    def bc_func(VQ):
	a = 4
	leftspeed = Expression(('1-x[0]*x[0]','0'))
	bcC = DirichletBC(VQ.sub(0), cylspeed, cyl)
	bcL = DirichletBC(VQ.sub(0), leftspeed, left)
	bcS = DirichletBC(VQ.sub(0), Constant((0,0)), sides)
	return [bcC, bcL, bcS]

    def bc_update(t):
	cylspeed.a = sin(t*pi)

    s.define_problem(mesh, bc_func)
    s.solve_it(1000, creeping = False, bc_update = bc_update, dt=0.1,T=20)
