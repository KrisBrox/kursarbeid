#!/usr/bin/env python
from dolfin import *

def boundary(x, on_boundary): return on_boundary

mesh = UnitIntervalMesh(1000)

V = FunctionSpace(mesh,'Lagrange', 1)
u = TrialFunction(V)
v = TestFunction(V)

bc = DirichletBC(V, Constant(0), boundary)
A, _ = assemble_system(inner(grad(u),grad(v))*dx, Constant(0)*v*dx, bc)
M, _ = assemble_system(u*v*dx, Constant(0)*v*dx, bc)

import pytave
from numpy import matrix, diagflat, sqrt
from numpy import linalg

e, l = pytave.feval(2, "eig", A.array(), M.array())
e = matrix(v)
l = matrix(l)
for k in [1, 10, 100]:
    u_ex = Expression("sin(k*pi*x[0])",k=k)
    u = interpolate(u_ex, V)
    x = matrix(u.vector().array())
    ex = x.T*e.T
    H1_norm = pi*k*sqrt(2)/2
    print "H1_seminorm of sin(%d pi x) %e " % (k, H1_norm)
    H1_norm = sqrt(assemble(inner(grad(u),grad(u))*dx))
    print "H1_seminorm of sin(%d pi x) %e " % (k, H1_norm)
    H1_norm = sqrt(ex.T*l*ex) / len(ex)
    print "H1_seminorm of sin(%d pi x) %e " % (k, H1_norm)
   


