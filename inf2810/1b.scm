;; 1)
(cons 10 11)
;; (10 . 11)

(cons 47 '())
;; (47)

(list 47 11)
;; (47 11)

'(47 (11 12))
;; (47 (11 12))

(define foo '(1 2 3))
(cons foo foo)
;; ((1 2 3) 1 2 3)


; 2)
;; a)
(define (length2 l) 
  (define (length-iter l c) 
    (if (null? l)
      c
  (length-iter (cdr l) (+ c 1))))
  (length-iter l 0))
;;(length2 '(1 2 3 4 5 6))

;; b)
(define (rev-list items) 
  (if (null? items)
      '()
  (append (rev-list (cdr items))  (list (car items))))) ;; sett sammen revers av de siste elementene  
;;(rev-list (list 1 2 3 4 5))                             ;; med det første elementet bakerst.
;;(rev-list '("s" "erte" "terte" "smerte"))               ;; prosedyren er ikke halerekursiv. det er fordi   
;;(rev-list '())                                          ;; den første løsningen jeg kom på var denne.

;; c)
(define (ditch num nums) 
  (define (rev-list items) 
    (if (null? items)
      '()
      (append (rev-list (cdr items))  (list (car items)))))  
  (define (ditch-iter newnums oldnums)
    (cond ((null? oldnums) 
           (rev-list newnums))
          ((eq? num (car oldnums)) 
           (ditch-iter newnums (cdr oldnums)))
          (else  
           (ditch-iter (cons (car oldnums) newnums) (cdr oldnums)))))
  (ditch-iter '() nums))
;; Her ble løsningen min skikkelig teit, fordi omvendt (rett) rekkefølge i.e.
;; (cons newnums (car oldnums)) gir resultatet ((((((() . 1) . 2) . 4) . 5) . 6) . 7)
(ditch 3 (list 1 2 3 3 3 4 3 5 6 7))
(ditch 3 (list 3 3 3 3 1 2 3 3 3 4 3 5 6 7))

;; d)
(define (nth pos l)
  (define (nth-iter rel-pos rest)
    (if (zero? rel-pos)
        (car rest)
    (nth-iter (- rel-pos 1) (cdr rest))))
    (nth-iter pos l))
(nth 4 '(3 4 5 666 69 2))

;; e)
(define (where num l)
  (define (where-iter pos rest)
    (cond ((null? rest) 
           #f)
          ((eq? num (car rest))
           pos)
           (else
            (where-iter (+ pos 1) (cdr rest)))))
        (where-iter 0 l))
(where 4 '(3 49 5 666 69 2 4))
(where 666 '(1 2 3 4 5))

;; f) 
(define (map2 op l1 l2)
  (define (map e1 e2)
    (op e1 e2))
  (define (map2-iter l1 l2 res)
    (cond ((null? l1) (rev-list res)) 
          ((null? l2) (rev-list res))
          (else 
           (map2-iter (cdr l1) (cdr l2) (cons (map (car l1) (car l2)) res)))))
  (map2-iter l1 l2 '()))
(map2 + '(1 2 3 5 6) '(1 2 3 4))  

;; g)
(map2 (lambda (a b) (/ (+ a b) 2)) '(1 2 3 4) '(3 4 5))
;; (2 3 4)


(map2 (lambda (a b) (and (= (remainder a 2) 0) (= (remainder b 2) 0))) '(1 2 3 4) '(4 4 5 7 "f")) 
;; (#f #t #f #f)

;; h)
(define (both? pred)
  (lambda (a b) 
    (and (pred a)
         (pred b))))

(map2 (both? zero?) '(454 332 0 0) '(90 2 3 0)) ;; (#f #f #f #t)
(map2 (both? even?) '(2 2 2) '(1 2 1))          ;; (#f #t #f)

;; i
(define (self proc)
  (lambda (x) (proc x x)))

((self +) 5)
((self list) 3)
(self +)
