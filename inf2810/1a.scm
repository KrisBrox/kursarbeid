;; Task 1

(* (+ 4 2) 5)
; 30

;; (* (+ 4 2) (5))
;; "expected a procedure"
;; five is not a procedure, and so it cannot stand alone in a parenthesis.

;; (* (4 + 2) 5)
;; "not a procedure"
;; 4 is here written as the first symbol in a parenthesis, but it is not a procedure. 
;; Infix notation is not understood in r5rs 

(define bar (/ 42 2))
bar
;; 21

(- bar 11)
;; 10

(/ (* bar 3 4 1) bar)
;; 12

;; Task 2a

(or (= 1 2)
    "piff!"
    "paff!"
    (zero? (1 - 1)))
;; "piff!", because
;; or returns the first non-false operand

(and (= 1 2)
     "piff!"
     "paff!"
     (zero? (1 - 1)))
;; #f, because
;; and returns #f if there is at least one operand that is #f

(if (positive? 42)
    "poff!"
    (i-am-undefined))
;; "poff!", because
;; 42 is positive so the first operand is ran 
;; (and a string evaluates to itself)

;; Task 2b

(define (sign x) 
  (if (negative? x) 
      -1
      (if (= 0 x) 0 1)))
(sign 1)   ;; 1
(sign -10) ;; -1
(sign 0)   ;; 0
;; selvforklarende

(define (sign2 x) 
  (cond 
    ((< x 0) -1) 
    ((> x 0) 1) 
    (else 0)))
(sign2 1e23) ;; 1
(sign2 -34)  ;; -1
(sign2 0)    ;; 0

;; Task 2c

(define (sign3 x)
  (or 
   (and (< x 0) -1)
   (and (> x 0) 1)
   0))

(sign2 -1e23) ;; -1
(sign2 34)    ;; 1
(sign2 0)     ;; 0
      
;; Task 3a

(define (addl x)
  (+ 1 x))
(define (subl x)
  (- x 1))

(addl 34)
(subl 34)

;; Task 3b

(define (plus a b)
  (if (zero? b)
   a
   (plus (addl a) (subl b))))

(plus 44 1e2) ;; virker (144)

;; Task 3c

;; (plus a b) er en rekursiv prosedyre som gir opphav til en iterativ prosess.
;; Prosessen itererer over alle tallene mellom a og a+b, 
;; og for hvert av dem sjekker om det er svaret. Siden den ikke venter på svar fra kall på seg selv,
;; men kun jobber videre med resultatet fra forrige kall hver gang, er prosessen iterativ. (det er ingen "chain of deferred operations"). 

;; Task 3d

(define (pct b n)
  (define (pi b n e) ;; internal definition of help procedure
    (if (> (expt b e) n)
        e
        (pi b n (+ e 1))))
  (pi b n 1))

(pct 2 255) ;; 8
(pct 2 256) ;; 9
  

;; I don't see how to make the helper procedure simpler,
;; but below is an alternative implementation.

(define (pct2 b n)
  (define (pi2 b_ e) 
    (if (> b_ n)
        e
        (pi2 (* b b_) (+ 1 e))))
  (pi2 b 1))
(pct2 2 7)  ;; 3
(pct2 3 26) ;; 3

;; Task 3e

(define (fib n)
  (define (fibiter a b c) 
    (if (= c 0)
        b
        (fibiter (+ a b) a (- c 1))))
  (fibiter 1 0 n)) 

(fib 2)   ;; 1
(fib 6)   ;; 8
(fib 256) ;; 141693817714056513234709965875411919657707794958199867

;; It seems to me, that to calculate the n'th fibonacci number you need to iterate over the sequence,
;; and in each step you need to calculate the sum of two numbers, update where in the sequence you are,
;; and check if you have arrived at the desired place in the sequence. As far as I can see, this is 
;; exactly what the procedure does. 

;; Nevertheless, below is a version of the algorithm which dispenses with the counting variable.
;; I am not if fib2 may count as a simplification of fib.

(define (fib2 n)
  (define (fibiter2 a b) 
    (if (begin (set! n (- n 1)) (= n -1))
        b
        (fibiter2 (+ a b) a)))
  (fibiter2 1 0)) 

(fib2 4) ;; 3
(fib2 5) ;; 5
(fib2 6) ;; 8
